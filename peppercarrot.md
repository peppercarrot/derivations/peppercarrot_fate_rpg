<!--Pepper&Carrot Fate Accelerated RPG by Craig Maloney et al.-->

\tableofcontents

# Overview: What is This?

The Pepper&Carrot Fate Accelerated RPG is a role playing game set in the universe of Hereva where Pepper and Carrot live. This game will allow you to explore the world of Pepper&Carrot and play a variety of characters from the comics written and illustrated by David Revoy.

The Pepper&Carrot Fate Accelerated RPG is designed for 3--5 players, one of whom will play as the Game Moderator (or GM). The GM is responsible for describing the world and events of Hereva to the players. They also take the role of any characters that the players might encounter in this world. We'll have more details about what it means to be a GM later on in this book. 

You'll also need four **Fate** or **Fudge** dice. These dice are six-sided dice that have the two each of the following faces:  plus (\dPlus), minus (\dMinus), and blank (\dBlank). You can find these online or at finer hobby game stores for purchase. You can also make them yourself via several methods (either with a blank die, or with some clever marking of a six sided die). Check the appendix for more details about Fate or Fudge Dice. You may also use **The Deck of Fate** in place of, or in addition to, dice. Each player should preferably have their own set of dice or cards.

Each player should have a **character sheet** (located in the back of the book), one for each player. They should also have a supply of **Index cards** or **sticky notes**.

The Pepper&Carrot Fate Accelerated RPG requires the use of tokens, or **Fate Points**. These can be anything from **Fate Points** available from Evil Hat, poker chips, tokens from other games, glass beads (non-rolling glass beads preferred) or anything you can find about 30--40 of for a game. (Note: **The Deck of Fate** can also be used for Fate Points.)


\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth,height=7cm]{images/2019-02-27_Flight-of-Spring_by-David-Revoy.jpg} 
\end{figure}

# Hereva: The 50Ko Tour of Komona

*Gather around friends for this exciting and brief tour of the City of Komona. Usually such tours go for 10 times the price, but for the price of a few Star Fruit you won't be tourists for long. I'll just tell my son to mind the shop and we'll be on our way.*

Where we are is the **Komona City Market Square**. This is where folks from all over Hereva come to buy and sell their wares. It's a large bazaar where one can find most anything that you desire (as long as it's legal. **Komonan Guards** keep a tight watch on this market).

We'll walk over here just outside the market to the town square. That building there? That's the office of **Miss  Saffron**, the most famous witch in all of Komona. She caused quite a stir when she won the second magic contest (the first potion contest nearly destroyed Komona. I mean, who's big idea was it to pour a growth potion on a zombified canary?). Miss Saffron's a celebrity and now all of the witches want to get her advice on anything from how to join the **Magmah school**, to relationship advice, to pretty much anything else. They say her appointment calendar is booked at least 13 months in advance. She must be something to have that kind of success.

The office of **Mayor Bramble** is over there. I think the only time we ever see him is when he's announcing another magic or potion contest. Granted these contests do bring some much-needed tourism into Komona, but I'd like him to show a little more focus on the rebuilding efforts from the first potion contest. 

At the second magic contest the mayor brought in representatives from all of the magical schools. We had **Queen Coriander** from **Zombiah**, **Shichimi** from **Ah**, **Pepper** from **Chaosah**, **Spirulina** from **Aqua**, **Camomile** from **Hippiah**, and of course Miss Saffron representing Magmah. I think this was the first time since the war that all six schools were in one location without fighting each other. Naturally Saffron won the contest and her fame in Komona grew. As you can see there are several ad campaigns using her to pitch their products. 

The other big event here was the coronation of Queen Coriander in her castle in **Qualicity**. Oh, it was a lovely spectacle. We watched the coverage in the arena. Everyone who is anyone was there, including those witches from Chaosah that caused quite a spectacle. **Wasabi**, the head of the school of Ah said they were a complete disgrace with the gluttony of **Cumin**, the carrying on and cavorting of that old witch **Thyme**, and worst of all the *intimidating stare* of **Cayenne**. I don't think she even blinked once during the whole coronation. And **Pepper** and her cat **Carrot** seemed to be right in the thick of things. The tabloids had a field day with that lot.

Between the potion contests and the coronation Saffron and Queen Coriander are the most famous witches in all of Hereva right now. The most infamous witches are Pepper, Thyme, Cumin, and Cayenne. Even with just the photo in the paper Cayenne's stare was just, just... Yeah, I'd rather not think about it.

If you look over that wall there you'll see the three moons peeking over the horizon. Over that way is where they say that the temples of Ah are located. They're one of the few constants in Hereva. I'm not sure where you're traveling next but you'll want to visit the Hereva Cartography Company to get their maps. Accept no substitutes! The landscape changes have been more unpredictable than usual, which means the Hereva Cartography Company's business is booming selling their proprietary maps. Believe the hype --- they're the best way to get around Hereva. Some folks still take their chances navigating around Hereva by looking for landmarks and using the stars, but for me there's no substitute. Of course there are a few competitors making alternate maps, but for my Ko the Hereva Cartography Company maps are the best.

Komona and Kerberos are both floating cities, floating by the magic power of **The Great Tree of Komona** in the center of each city. Kerberos is the more recent of the cities, formed when the great tree fractured during the war. That's where the really wealthy Komonans live, though I've yet to see any of it.

The great war really changed the landscape of Hereva, It certainly changed the magical schools that lost. Kerberos didn't even exist until The Great Tree of Komona nearly died during the conflict. Folks say the city was falling to the ground when suddenly a loud **boom** happened. Suddenly there was a smaller tree next to The Great Tree of Komona. Some took it as a sign of a new era of Hereva while others wondered if the war had irreparably changed Hereva forever. What I know is the real-estate developers saw an opportunity for a once-in-a-lifetime real-estate development and suddenly there was a whole gated community there on Kerberos. If you get a chance to go over there I'd love to hear your tale. Heck, you might get me to part with some Ko for your trouble.

Speaking of parting with some Ko it seems our tour has concluded. I hope you enjoy the rest of your stay in Komona and please feel free to browse around the shop and let me know if I can be of any more service to you. Good Pink Moon to you all.

\includegraphics[width=\textwidth,height=7cm]{images/2015-11-07_wiki_place_komona_by-David-Revoy.jpg}

# A Brief History of Hereva (Excerpt)

*This is an excerpt from the five volume series "The Story of Hereva" written by W. A. Currant. In this vast series of books is the complete history of Hereva through it's different periods. It talks about what is known about early Herevans (not much), the discovery of magic in Hereva and the The Great Tree of Komona, the rise of the various magical schools, and what lead up to the great war. The final volume ends just after The Great War. It is a fascinating look at what Hereva was before the devastation. It also is the most complete document of the formation of the schools of Hereva assembled in one work. Some historians scoff at accuracy of the material, and new evidence continues to be unearthed about the early Herevans, but W. A. Currant's work still remains the most readable account of the history of Hereva.*

## The pre-history of Hereva

The Dragons have lived in the lands of Hereva long before human recollection. It wasn't until The Great Tree of Komona sprouted forth that the dragons took notice of the humans in Hereva. Why The Great Tree's appearance in Hereva coincides with the arrival of humans in draconic recollection remains a mystery. Several Komonan scholars have dedicated substantial research to look into humanity's past before The Great Tree and continue to be baffled at the lack of conclusive evidence to resolve this question. Many conversations with the dragons about the origin of humans in Hereva prove inconsistent at best and obfuscated at worst. One theory is that the dragons simply did not pay any attention to the humans until they wielded magic (much in the same way that humans don't pay attention to the ants until they start building superstructures in their back yard). Another theory is the dragons purposefully obfuscate human history, answering in riddles and conflicting details in the hopes of keeping humans away from some hidden lore about their existence prior to arriving in Hereva.

Another prevailing theory is that the Dragons were involved in a war which brought about the formation of The Great Tree of Komona and the presence of humans. The theory posits that because most of recorded history starts with the discovery of The Great Tree of Komona then there is a direct connection between the Great Tree of Komona and the awakening of humans in Hereva. The theory splinters into two separate sub-theories. The first is that the humans were somehow magically uplifted from their primitive beginnings. Unfortunately there is little archaeological evidence to support this theory. The second is that the humans were brought from another universe and quickly adapted to the magic of Hereva. This is the more widely-accepted theory, but the question of who or what brought the humans and where they came from is still a mystery. Some believe the dragons of Hereva played a key role, but the dragons have proved unreliable and unwilling to provide any answers.

The only thing that all Komonan scholars know for certain is that recorded history of humans in Hereva is relatively short and that it somehow coincides with The Great Tree of Komona and the dragons taking notice of humanity. 

## The Formation of Magic

Speculation abounds over what happened before magic was discovered in Hereva, but what is clear is Hereva matured quickly once magic entered the realm. The Great Tree of Komona figures prominently in the folklore of Hereva and its arrival signaled the birth of the magical age. None of the written records predate the blooming of The Great Tree, and some speculate that The Great Tree is really when Hereva came into existence. So significant is the sprouting of the great tree that it is marked as year 0 in the Hereva calendar. Events before the sprouting of The Great Tree are referred to as "Before The Great Tree" (usually shortened to "Before Times"). Events after the sprouting of The Great Tree are not given any special distinction and are referred to by their year. The thought is that anything before the sprouting of The Great Tree is ancient history that is only of use to nosy scholars who like to talk to meddlesome and misdirecting dragons.

## The Blossoming of Hereva

With the discovery of magic Hereva transformed overnight. Grunts and gestures gave way to the written word and spoken language. Incantations were discovered and recorded, and before long schools formed to collect the various spells into books. So many spells were discovered that it was impossible to keep up and the schools began to specialize into the disciplines known to Hereva today. Some were open and welcoming to new members, but others remained more secretive and aloof. Schools like Hippiah, Magmah and Zombiah found ways to take the drudgery out of day-to-day existence, whereas Chaosah preferred spells to control and direct the universe itself in minute detail.

For centuries these schools lived in harmony. Occasionally there were squabbles and bickering but everything remained civil between the schools. By most accounts the only school that caused any trouble before the war was Chaosah, and that was because of their tendency to play pranks on the other schools. One such prank by Chaosah would involve the leaving around spell books entitled "Chaosah Secrets" full of nonsense spells, or potions that rarely did what they said on the label. One such potion accidentally made its way back to the shelves of Chaosah where an unfortunate student made the discovery. The student realized (too late) that the potion labeled "Potion of Invisibility" was actually A "Potion of Divisibility", and the hapless student disappeared in a puff of mathematical improbabilities.

## The Great War

In 252 there was a brutal and all-encompassing war between the Magmah and Aquah schools. What started as a simple disagreement between Magmah and Aquah escalated into assaults on each other. Both schools enlisted aid from any schools they could and dragged whomever they could into the conflict.

During the years leading up to the war Chaosah grew in strength. They played Magmah and Aquah against each other and were responsible for escalating a few skirmishes into an all-out war. The fighting spread, and the other schools were dragged into the conflict. Zombiah quickly learned they could not supply both Magmah and Aquah with materials for their weapons without the other school finding out. Once Magmah learned that Zombiah was supplying materials to Aquah and Aquah learned that Zombiah was supplying materials to Magmah then both Magmah and Aquah declared war on Zombiah. Zombiah took heavy losses during the attacks from both schools and used their magic to animate and re-animate both mechanical constructs and fallen warriors. Hippiah tried to stay neutral but in the wake of starvation they caved and used their magic to help supply any who came to them with food and shelter.

Later in the war the schools devised spells of unspeakable power that summoned the nastiest and most brutal forms of destruction. The Elemental Battles saw Magmah and Aquah summoning up elementals to do battle. Fire elementals clashed with water elementals in combat. Whole areas were wiped out either by fire or flood. Zombiah learned how to make earth elementals to try to defend themselves from the ensuing onslaught. Hippiah summoned trees and plants to fortify their borders. But Chaosah would not be outdone and summoned the Chaosah Demons to battle against these elemental forces.

Nobody is quite sure why The High Council of Ah remained silent for most of the war, but historians point to two significant events that lead to their entry in 254. The first was Ah noticing that Zombiah was re-animating their fallen warriors in large numbers. The predominant theory explains that such a large reanimation caused a disturbance in the spirit world, and Ah could not help but interfere swiftly to prevent such a disturbance. The second event was Ah realizing that Chaosah were stoking the fires of this conflict into an all-out war. Worse, Chaosah were using their advanced inter-dimensional knowledge to poke holes into Hereva itself. The Chaosah Demons were brought to Hereva as part of Chaosah's inter-dimensional poking. The dimensional spells and black-holes of Chaosah are believed to have caused one of the final events in the great war. The theory holds that The Great Tree of Komona was the last defense against the eventual annihilation of the reality of Hereva, and if left unchecked Chaosah would have poked enough holes in Hereva to cause irreparable harm. Regardless of the true reason, Ah joined the fighting and immediately went after the witches of Chaosah. Ah systematically banished the Chaosah Demons and destroyed each witch of Chaosah in turn until only Cayenne, Thyme and Cumin remained.

Defiant, Cayenne and Thyme fought the forces of Ah. Cumin tried to escape but was picked off. Cayenne held an offensive position while Thyme attempted to summon up reinforcements. Unfortunately Thyme had not planned on the ferocity of Ah's assault, and her spell began to use up the available Rea from the surrounding area. Cayenne was unable to block the power drain and both her and Thyme were ill-prepared for the ensuing rupture that killed them. It took several adepts of Ah to try to contain flow of Rea. The Great Tree, unbalanced by the destruction of Chaosah spewed Rea throughout Hereva with such force that it blew off one of the limbs of the tree (which later became Kerberos). The resulting shock-wave of Rea from The Great Tree explosion disrupted the rupture long enough for it to seal.

But the damage was done. With Chaosah destroyed, the magic of Hereva was deteriorating rapidly. Ah enlisted Zombiah to revive what it could of Chaosah. Cumin was revived but that was not enough to restore balance. Reluctantly Thyme and Cayenne were revived and charged with finding a true heir to carry on the Chaosah school and train them. Chaosah was put under careful watch to ensure they were indeed training their school's heir.

The remaining members from the magic schools were assembled and a treaty was brokered, the results of which are stored in a magic lock-box visible only to select members of the schools. With the treaty came an end to the fighting and bloodshed that almost destroyed Hereva.

The High Council of Ah determined that both Magmah and Aquah were to blame for the conflict. Because Magmah and Aquah began this conflict The High Council of Ah ruled that both Magmah and Aquah should suffer the ultimate shame of being re-named by the other school for 10 years. Magmah renamed Aquah as Wharrgarblah, and Aquah renamed Magmah as Kielbasah. The records for both schools changed immediately, and all references to the schools were magically altered to use the new names. Having served their sentences the names Wharrgarblah and Kielbasah have since reverted back to their original names (Aquah and Magmah respectively).

Soon after the treaty Zombiah focused on animating constructs and creating stronger materials. It's unclear if Ah was responsible for Zombiah's change in focus, or if it became more economical to animate constructs than re-animate the dead. Only those with access to the lock-box know the truth.

As additional penance each school also surrendered their spell books that were used in the war to Ah. Each school sent their book to Ah for safekeeping. Wasabi keeps them all on display as a reminder of their victory in The Great War and to prevent further hostilities. (Though there has been some talk that the book that Chaosah submitted doesn't look much like a spell books, and they still possess the ability to summon demons at will).

# Character Creation

*Players in Pepper&Carrot Fate Accelerated RPG portray witches in the magical schools of Hereva. They can be students in their first year, highly gifted students, or even the leaders of the magical schools. Other characters are possible but for this chapter we're going to focus on how to make a witch in the world of Hereva.*

In this section we'll describe how to make a Witch to play in the world of Hereva. Witches are some of the most powerful humans in Hereva. Each step of this process will guide you in creating your character.

## How Do I Make a Character?

Now it's time to start writing stuff down. Grab a pencil and a copy of the character sheet. Some people like to use form-fillable PDFs on a laptop or tablet computer. Any of that's fine, but you definitely want something that lets you erase and change.

## Aspects in a Nutshell

An aspect is a word, phrase, or sentence that describes something centrally important to your character. It can be a motto your character lives by, a personality quirk, a description of a relationship you have with another character, an important possession or bit of equipment your character has, or any other part of your character that is vitally important.

Aspects allow you to change the story in ways that tie in with your character's tendencies, skills, or problems. You can also use them to **[establish facts about the setting](#establishing-facts)**, such as the presence of a certain type of magic, or the existence of a useful ally, dangerous enemy, or secret organization.

Your character will have a handful of aspects (between three and five), including a high concept and a trouble. We discuss aspects in detail in **[Aspects and Fate Points](#aspects-and-fate-points)**---but for now, this should help you get the idea.

### High Concept

First, decide on your character's high concept. This is a single phrase or sentence that neatly sums up your character, saying who you are, what you do, what your "deal" is. When you think about your high concept, try to think of two things: how this aspect could help you, and how it might make things harder for you. Good high concept aspects do both.

$\hrulefill$

Examples: New student of Magmah; Former apprentice to the head teacher of Aquah; Wandering priestess of Ah.

$\hrulefill$

#### Trouble

Next, decide on the thing that always gets you into trouble. It could be a personal weakness, or a recurring enemy, or an important obligation---anything that makes your life complicated. 

$\hrulefill$

Examples: I need to prove myself; Never enough Ko; Tempted by shiny things

$\hrulefill$

#### Another Aspect

Now compose another aspect. Think of something really important or interesting about your character. Are they the strongest person in their hometown? Do they have a reputation known across different schools? Do they talk too much? Are they filthy rich?

#### Optional: One or Two Additional Aspects

If you wish, you may create one or two more aspects. These aspects might describe your character's relationship with other player characters or with an NPC\@. Or, like the third aspect you composed above, it might describe something especially interesting about your character.

If you prefer, you can leave one or both of these aspects blank right now and fill them in later, after the game has started.

#### Name and Appearance

Describe your character's appearance and give them a name.

**CREATING CHARACTERS: THE 30-SECOND VERSION**

* [Write two aspects](#aspects-and-fate-points): a high concept and a trouble.
* Write another aspect.
* Give your character a name and describe their appearance.
* [Choose approaches](#approaches).
*  Set your refresh to 3.
*  You may write up to two more aspects and [choose a stunt](#stunts) if you wish, or you may do that during play.

#### Approaches

Choose your approaches.

Approaches are descriptions of how you accomplish tasks. Everyone has the same six approaches:

* Careful
* Clever
* Flashy
* Forceful
* Quick
* Sneaky

Each approach is rated with a bonus. Choose one at Good (+3), two at Fair (+2), two at Average (+1), and one at Mediocre (+0). You can improve these later. We talk about [what each approach means](#approaches) and how you use them in [How to Do Stuff: Outcomes, Approaches, and Actions](#how-to-do-stuff-outcomes-approaches-and-actions).

$\hrulefill$

**THE LADDER**

In Fate, we use a ladder of adjectives and numbers to rate a character's approaches, the result of a roll, difficulty ratings for simple checks, etc.

Here's the ladder:

| Skill Level | + / -      |
|-------------|------------|
| Legendary   |      +8    |
| Epic        |      +7    |
| Fantastic   |      +6    |
| Superb      |      +5    |
| Great       |      +4    |
| Good        |      +3    |
| Fair        |      +2    |
| Average     |      +1    |
| Mediocre    |       0    |
| Poor        |      -1    |
| Terrible    |      -2    |

$\hrulefill$

Your approaches can say a lot about who you are. Here are some examples:

* The Brute: Forceful +3, Careful and Flashy +2, Sneaky and Quick +1, Clever +0
* The All-Star: Quick +3, Forceful and Flashy +2, Clever and Careful +1, Sneaky +0
* The Trickster: Clever +3, Sneaky and Flashy +2, Forceful and Quick +1, Careful +0
* The Guardian: Careful +3, Forceful and Clever +2, Sneaky and Quick +1, Flashy +0
* The Thief: Sneaky +3, Careful and Quick +2, Clever and Flashy +1, Forceful +0

#### Stunts and Refresh

A stunt is a special trait that changes the way an approach works for your character. Generally, stunts give you a bonus (almost always +2) to a certain approach when used with a particular action under specific circumstances. We'll talk more about stunts in **[Stunts](#stunts)**. Choose one stunt to start, or you can wait and add a stunt during the game. Later, when your character advances, you can choose more.

Your refresh is the number of fate points you begin each game session with---unless you ended the previous session with more unspent fate points than your refresh, in which case you start with the number you had left last time. By default, your refresh starts at three and is reduced by one for each stunt after the first four you choose---essentially, your first four stunts are free! As your character advances, you'll get opportunities to add to your refresh. Your refresh may never go below one.


$\hrulefill$

**HOW MANY STUNTS?**

By default, Fate Accelerated Edition suggests choosing one stunt to start with.

However, if this is your first time playing a Fate game, you might find it easier to pick your first stunt after you've had a chance to play a bit, to give you an idea of what a good stunt might be. Just add your stunt during or after your first game session.

On the other hand, if you're an experienced Fate gamer, you might look ahead and discover that, just like in Fate Core, your character is entitled to several free stunts before it starts costing you refresh. In that case, let the least experienced member of your game group be your guide; if someone is new to the game and only takes one to start with, that's what everyone should do. If you're all experienced, and you want to start with more powerful characters, just take all four to start and off you go.

We allow your character up to **four stunts** before requiring you to spend refresh. This means your character may start with two **school stunts** and two stunts of their own. (This allows you one more stunt than the standard three stunts of Fate Core.)

School stunts are stunts that are provided for being a member of a school. These are related to what the school teaches or the reputation of the school. Certain characters (like Pepper) may not fit with one of the school stunts. Chaosah's ability to intimidate others simply by being a Chaosah witch does not fit Pepper's character, so she selects a different stunt instead.

You must have at least one of the school stunts in order to be a member of that school. You may use this as your initial stunt for your character. Each school's stunts are listed in the "Magic School" chapter for each school.

Remember that your character can change or add additional stunts at a milestone. Perhaps later in the game Pepper becomes a powerful Chaosah witch. She can later choose to trade one of her current stunts for the Chaosah Intimidation stunt. Or Shichimi might decide to become less like an Ah student and remove one of the Ah stunts for something she prefers. 

If your character decides to change schools (e.g.:\ your character was part of Hippiah school but gets recruited by Chaosah Witches) you may do this at a major milestone. You may remove one or both of your current school's stunts. As your character learns the ways of their new school you may substitute or add the new school's stunts at each minor milestone. Remember that if you no longer have any of your previous school's stunts you are no longer a member of that school.

$\hrulefill$

### How To Do Stuff: Outcomes, Actions, and Approaches

Now it's time to start doing something. You need to leap from one flying broom to another. You need to search the entire library for that spell you really need. You need to distract the guard so you can sneak into the fortress. How do you figure out what happens?

First you narrate what your character is trying to do. Your character's own aspects provide a good guide for what you can do. If you have an aspect that suggests you can perform magic, then cast that spell. If your aspects describe you as a swordsman, draw that blade and have at it. These story details don't have additional mechanical impact. You don't get a bonus from your magic or your sword, unless you choose to spend a fate point to invoke an appropriate [aspect](#aspects). Often, the ability to use an aspect to make something true in the story is bonus enough!

How do you know if you're successful? Often, you just succeed, because the action isn't hard and nobody's trying to stop you. But if failure provides an interesting twist in the story, or if something unpredictable could happen, you need to break out the dice.

$\hrulefill$

**TAKING ACTION\@: THE 30-SECOND VERSION**

1. Describe what you want your character to do. Determine if someone or something can stop you. 
1. Decide what action you're taking: create an advantage, overcome, attack, or defend. 
1. Decide on your approach. 
1. Roll dice and add your approach's bonus. 
1. Decide whether to modify your roll with aspects. 
1. Figure out your outcome. 

$\hrulefill$

### Dice or Cards

Part of determining your outcome is generating a random number, which is usually done in one of two ways: rolling four Fate Dice, or drawing a card from a Deck of Fate.

**Fate Dice**: Fate Dice (sometimes called **Fudge dice**, after the game they were originally designed for) are one way to determine outcomes. You always roll Fate Dice in a set of four. Each die will come up as \dPlus, \dBlank, or \dMinus, and you add them together to get the total of the roll. For example:

| Dice Roll                           | +/- |
|-------------------------------------|-----|
|\dMinus{}\dPlus{}\dBlank{}\dPlus{}   | +1  |
|\dPlus{}\dMinus{}\dBlank{}\dBlank{}  |  0  |
|\dPlus{}\dPlus{}\dPlus{}\dMinus{}    | +2  |
|\dMinus{}\dBlank{}\dBlank{}\dBlank{} | -1  |

**Deck of Fate**: The Deck of Fate is a deck of cards that copies the statistical spread of Fate Dice. You can choose to use them instead of dice---either one works great.

$\hrulefill$

These rules are written with the assumption that you're rolling Fate Dice, but use whichever one your group prefers. Anytime you're told to roll dice, that also means you can draw from the Deck of Fate instead.

$\hrulefill$

### Outcomes

Once you roll your dice, add your approach bonus (we'll talk about that in a moment) and any bonuses from aspects or stunts. Compare the total to a target number, which is either a [fixed difficulty](#setting-difficulty-levels) or the result of the GM's roll for an NPC\@. Based on that comparison, your outcome is:

* You fail if your total is less than your opponent's total.
* It's a tie if your total is equal to your opponent's total.
* You succeed if your total is greater than your opponent's total.
* You succeed with style if your total is at least three greater than your opponent's total.

Now that we've covered outcomes, we can talk about actions and how the outcomes work with them.

### Actions

So you've narrated what your PC is trying to do, and you've established that there's a chance you could fail. Next, figure out what action best describes what you're trying to do. There are four basic actions that cover anything you do in the game.

#### Create an Advantage \CA

Creating an advantage is anything you do to try to help yourself or one of your friends. Taking a moment to carefully aim your wand for a spell, spending several hours doing research in the school library, or tripping the thug who's trying to rob you---these all count as creating an advantage. The target of your action may get a chance to use the defend action to stop you. The advantage you create lets you do one of the following three things:

* Create a new situation [aspect](#situation-aspects)
* Discover an existing situation [aspect](#character-aspects) or another character's aspect that you didn't know about.
* Take advantage of an existing [aspect](#aspect).

If you're creating a new aspect or discovering an existing one:

* **If you fail:** Either you don't create or discover the aspect at all, or you create or discover it but an opponent gets to invoke the aspect for free. The second option works best if the aspect you create or discover is something that other people could take advantage of (like Rough Terrain). You may have to reword the aspect to show that it benefits the other character instead of you---work it out in whatever way makes the most sense with the player who gets the free invocation. You can still invoke the aspect if you'd like, but it'll cost you a fate point.
* **If you tie:** If you're creating a new aspect, you get a **[boost](#boost)**. Name it and invoke it once for free---after that, the boost goes away. If you're trying to discover an existing aspect, treat this as a success (see below).
* **If you succeed:** You create or discover the aspect, and you or an ally may invoke it once for free. Write the aspect on an index card or sticky note and place it on the table.
* **If you succeed with style:** You create or discover the aspect, and you or an ally may invoke it twice for free. Usually you can't invoke the same aspect twice on the same roll, but this is an exception; success with style gives you a BIG advantage!

If you're trying to take advantage of an aspect you already know about:

* If you fail: You don't get any additional benefit from the aspect. You can still invoke it in the future if you'd like, at the cost of a fate point.
* If you tie or succeed: You get one free invocation on the aspect for you or an ally to use later. You might want to draw a circle or a box on the aspect's note card, and check it off when that invocation is used.
* If you succeed with style: You get two free invocations on the aspect, which you can let an ally use, if you wish.

$\hrulefill$

**ACTIONS & OUTCOMES: THE 30-SECOND VERSION**

**Create an Advantage when creating or discovering aspects:**

\CA

* **Fail:** Don't create or discover, or you do but your opponent (not you) gets a free invocation.
* **Tie:** Get a boost if creating new, or treat as success if looking for existing.
* **Succeed:** Create or discover the aspect, get a free invocation on it.
* **Succeed with Style: **Create or discover the aspect, get two free invocations on it.

**Create an Advantage on an aspect you already know about:**

\CA

* **Fail:** No additional benefit.
* **Tie**: Generate one free invocation on the aspect.
* **Succeed:** Generate one free invocation on the aspect.
* **Succeed with Style:** Generate two free invocations on the aspect.

**Overcome:**

\OV

* **Fail:** Fail, or succeed at a serious cost.
* **Tie:** Succeed at minor cost.
* **Succeed:** You accomplish your goal.
* **Succeed with Style:** You accomplish your goal and generate a boost.

**Attack:**

\AT

* **Fail:** No effect.
* **Tie:** Attack doesn't harm the target, but you gain a boost.
* **Succeed:** Attack hits and causes damage.
* **Succeed with Style:** Attack hits and causes damage. May reduce damage by one to generate a boost.

**Defend:**

\DE

* **Fail:** You suffer the consequences of your opponent's success.
* **Tie:** Look at your opponent's action to see what happens.
* **Succeed:** Your opponent doesn't get what they want.
* **Succeed with Style:** Your opponent doesn't get what they want, and you get a boost.

$\hrulefill$

#### Overcome \OV

You use the **[overcome](#setting-difficulty-levels)** }action when you have to get past something that's between you and a particular goal---picking a lock, escaping from handcuffs, leaping across a chasm, flying a spaceship through an asteroid field. Taking some action to [eliminate or change an inconvenient situation aspect](get-rid-of-a-situation-aspect) is usually an overcome action; we'll talk more about that in Aspects and Fate Points. The target of your action may get a chance to use the defend action to stop you.

* If you fail: You have a tough choice to make. You can simply fail---the door is still locked, the thug still stands between you and the exit, the guards are still On Your Tail. Or you can succeed, but at a serious cost---maybe you drop something vital you were carrying, maybe you suffer harm. The GM helps you figure out an appropriate cost.
* If you tie: You attain your goal, but at some minor cost. The GM could introduce a complication, or present you with a tough choice (you can rescue one of your friends, but not the other), or some other twist. See "Succeed at a Cost" in Running the Game in Fate Core for more ideas.
* If you succeed: You accomplish what you were trying to do. The lock springs open, you duck around the thug blocking the door, you manage to lose the guards on your tail.
* If you succeed with style: As success (above), but you also gain a boost.

#### Attack \AT

Use an attack when you try to hurt someone, whether physically or mentally---swinging a sword, shooting off a fireball spell, or yelling a blistering insult with the intent to hurt your target. (We'll talk about this in Ouch! Damage, Stress, and Consequences, but the important thing is: If someone gets hurt too badly, they're knocked out of the scene.) The target of your attack gets a chance to use the defend action to stop you.

* If you fail: Your attack doesn't connect. The target parries your sword, your spell misses, your target laughs off your insult.
* If you tie: Your attack doesn't connect strongly enough to cause any harm, but you gain a boost.
* If you succeed: Your attack hits and you do damage. See [Ouch! Damage, Stress, and Consequences](#ouch-damage-stress-and-consequences).
* If you succeed with style: You hit and [do damage](#what-is-stress), plus you have the option to reduce the damage your hit causes by one and gain a boost.

#### Defend \DE

Use defend when you're actively trying to stop someone from doing any of the other three actions---you're parrying a sword strike, trying to stay on your feet, blocking a doorway, and the like. Usually this action is performed on someone else's turn, reacting to their attempt to attack, overcome, or create an advantage. You may also roll to oppose some non-attack actions, or to defend against an attack on someone else, if you can explain why you can. Usually it's fine if most people at the table agree that it's reasonable, but you can also point to an relevant situation aspect to justify it. When you do, you become the target for any bad results.

* If you fail: You're on the receiving end of whatever your opponent's success gives them.
* If you tie or succeed: Things don't work out too badly for you; look at the description of your opponent's action to see what happens.
* If you succeed with style: Your opponent doesn't get what they want, plus you gain a boost.

#### Getting Help

An ally can help you perform your action. When an ally helps you, they give up their action for the exchange and describe how they're providing the help; you get a +1 to your roll for each ally that helps this way. Usually only one or two people can help this way before they start getting in each other's way; the GM decides how many people can help at once.

### Choose Your Approach

As we mentioned in [Who Do You Want to Be?](#who-do-you-want-to-be), there are six approaches that describe how you perform actions.

* Careful: A Careful action is when you pay close attention to detail and take your time to do the job right. Lining up a long-range arrow shot. Attentively standing watch. Disarming a trap near Pepper's house.
* Clever: A Clever action requires that you think fast, solve problems, or account for complex variables. Finding the weakness in an enemy swordsman's style. Finding the weak point in a fortress wall. Fixing a clockwork servant.
* Flashy: A Flashy action draws attention to you; it's full of style and panache. Delivering an inspiring speech to the other students in your school. Embarrassing your opponent in a duel. Producing a magical fireworks display.
* Forceful: A Forceful action isn't subtle---it's brute strength. Wrestling a phanda. Staring down a thug. Casting a big, powerful magic spell.
* Quick: A Quick action requires that you move quickly and with dexterity. Dodging an arrow. Getting in the first punch. Disarming a bomb as it ticks 3... 2... 1...
* Sneaky: A Sneaky action is done with an emphasis on misdirection, stealth, or deceit. Talking your way out of getting arrested. Picking a pocket. Feinting in a sword fight.

Each character has each approach rated with a bonus from +0 to +3. Add the bonus to your dice roll to determine how well your PC performs the action you described.

So your first instinct is probably to pick the action that gives you the greatest bonus, right? But it doesn't work like that. You have to base your choice of approach on the description of your action, and you can't describe an action that doesn't make any sense. Would you Forcefully creep through a dark room, hiding from the guards? No, that's being Sneaky. Would you Quickly push that big rock out of the way of the wagon? No, that's being Forceful. Circumstances constrain what approach you can use, so sometimes you have to go with an approach that might not play directly to your strengths.

### Roll the Dice, Add Your Bonus

Time to take up dice and roll. Take the bonus associated with the approach you've chosen and add it to the result on the dice. If you have a stunt that applies, add that too. That's your total. Compare it to what your opponent (usually the GM) has.

### Decide Whether to Modify the Roll

Finally, decide whether you want to alter your roll
by [invoking aspects](#invoking-aspects)---we'll talk about this a lot in Aspects and Fate Points.

# Challenges, Contests, and Conflicts

We've talked about the four actions (create an advantage, overcome, attack, and defend) and the four outcomes (fail, tie, succeed, and succeed with style). But in what framework do those happen?

Usually, when you want to do something straightforward---swim across a raging river, hack someone's crystal ball---all you need to do is make one overcome action [against a difficulty level](setting-difficulty-levels) that the GM sets. You look at your outcome and go from there.

But sometimes things are a little more complex.

### Challenges

A challenge is a series of overcome and create an advantage actions that you use to resolve an especially complicated situation. Each overcome action deals with one task or part of the situation, and you take the individual results together to figure out how the situation resolves.

To set up a challenge, decide what individual tasks or goals make up the situation, and treat each one as a separate overcome roll.

Depending on the situation, one character may be required to make several rolls, or multiple characters may be able to participate. GMs, you aren't obligated to announce all the stages in the challenge ahead of time---adjust the steps as the challenge unfolds to keep things exciting.

$\hrulefill$

The PCs are the crew of an airship caught in a storm. They decide to press on and try to get to their destination despite the weather, and the GM suggests this sounds like a challenge. Steps in resolving this challenge could be calming panicky passengers, repairing damaged rigging, and keeping the ship on the right heading.

$\hrulefill$

### Contests

When two or more characters are competing against one another for the same goal, but not directly trying to hurt each other, you have a contest. Examples include a broom chase, a public debate, or an archery tournament.

A contest proceeds in a series of exchanges. In an exchange, every participant takes one overcome action to determine how well they do in that leg of the contest. Compare your result to everyone else's.

If you got the highest result, you win the exchange---you score a victory (which you can represent with a tally or check mark on scratch paper) and describe how you take the lead. If you succeed with style, you mark two victories.

If there's a tie, no one gets a victory, and an unexpected twist occurs. This could mean several things, depending on the situation---the terrain or environment shifts somehow, the parameters of the contest change, or an unanticipated variable shows up and affects all the participants. The GM \hyperref[Situation-Aspects]{creates a new situation
aspect }reflecting this change and puts it into play.

The first participant to achieve three victories wins the contest.

### Conflicts

Conflicts are used to resolve situations where characters are trying to harm one another. It could be physical harm (a sword fight, a witches' duel, a battle using spells), but it could also be mental harm (a shouting match, a tough interrogation, a magical psychic blast).

$\hrulefill$

**CONFLICTS: THE 30-SECOND VERSION**

1. Set the scene.
1. Determine turn order.
1. Start the first exchange.

* On your turn, take an action.
* On other people's turns, defend against or respond to their actions as necessary.
* At the end of everyone's turn, start a new exchange or end the conflict.

$\hrulefill$

#### Setting the Scene

Establish what's going on, where everyone is, and what the environment is like. Who is the opposition? The GM should write a couple of situation aspects on sticky notes or index cards and place them on the table. Players \href{}{can suggest situation aspects}, too.

The GM also establishes zones, loosely defined areas that tell you where characters are. You determine zones based on the scene and the following guidelines:

Generally, you can interact with other characters in the same zone---or in nearby zones if you can justify acting at a distance (for example, if you have a ranged weapon or magic spell).

You can move one zone for free. An action is required to move if there's an obstacle along the way, such as someone trying to stop you, or if you want to move two or more zones. It sometimes helps to sketch a quick map to illustrate zones.

$\hrulefill$

Thugs are attacking the characters at Pepper's house. The living room is one zone, the kitchen another, the front porch another, and the yard a fourth. Anyone in the same zone can easily throw punches at each other. From the living room, you can throw things at people in the kitchen or move into the kitchen as a free action, unless the doorway is blocked. To get from the living room to the front porch or yard requires an action.

$\hrulefill$

#### Determine Turn Order

Your turn order in a conflict is based on your approaches. In a physical conflict, compare your Quick approach to the other participants'---the one with the fastest reflexes goes first. In a mental conflict, compare your Careful approach---attention to detail will warn you of danger. Whoever has the highest approach gets to go first, and then everyone else goes in descending order. Break ties in whatever manner makes sense, with the GM having the last word.

GMs, it's simplest if you pick your most advantageous NPC to determine your place in the turn order, and let all your NPCs go at that time. But if you have a good reason to determine turn order individually for all your NPCs, go right ahead.

#### Exchanges

Next, each character takes a turn in order. On their turn, a character can take one of the \hyperref[Anchor-24]{four actions}. Resolve the action to determine the outcome. The conflict is over when only one side has characters still in the fight. 

# Ouch! Damage, Stress, and Consequences

When you're hit by an attack, the severity of the hit is the difference between the attack roll and your defense roll; we measure that in shifts. For instance, if your opponent gets +5 on their attack and you get a +3 on your defense, the attack deals a two shift hit (5 - 3 = 2).

Then, one of two things happens:

* You suffer stress and/or consequences, but you stay in the fight.
* You get taken out, which means you're out of the action for a while.

$\hrulefill$

**STRESS & CONSEQUENCES: THE 30-SECOND VERSION**

* Each character starts with three stress boxes.
* Severity of hit (in shifts) = Attack Roll - Defense Roll
* When you take a hit, you need to account for how that hit damages you. One way to absorb the damage is to take stress; you can check one stress box to handle some or all of a single hit. You can absorb a number of shifts equal to the number of the box you check: one for Box 1, two for Box 2, three for Box 3.
* You may also take one or more consequences to deal with the hit, by marking off one or more consequence slots and writing a new aspect for each one. Mild consequence = 2 shifts; moderate = 4 shifts; severe = 6 shifts.
* If you can't (or decide not to) handle the entire hit, you're taken out. Your opponent decides what happens to you.
* Giving in before your opponent's roll allows you to control how you exit the scene. You also get one or more fate points for doing this!
* Stress and mild consequences vanish at the end of the scene, provided you get a chance to rest. Other consequences take longer.

$\hrulefill$

##  What Is Stress?

If you get hit and don't want to be taken out, you can choose to take stress.

Stress represents you getting tired or annoyed, taking a superficial wound, or some other condition that goes away quickly.

Your character sheet has a stress track, a row of three boxes. When you take a hit and check a stress box, the box absorbs a number of shifts equal to its number: one shift for Box 1, two for Box 2, or three for Box 3.

You can only check one stress box for any single hit, but you can check a stress box and take one or more consequences at the same time. You can't check a stress box that already has a check mark in it!

## What Are Consequences?

Consequences are new aspects that you take to reflect being seriously hurt in some way. Your character sheet has three slots where you can write consequences. Each one is labeled with a number: 2 (mild consequence), 4 (moderate consequence), or 6 (severe consequence). This represents the number of shifts of the hit the consequence absorbs. You can mark off as many of these as you like to handle a single hit, but only if that slot was blank to start with. If you already have a moderate consequence written down, you can't take another one until you do something to make the first one go away!

A major downside of consequences is that each consequence is a new aspect that your opponents can invoke against you. The more you take, the more vulnerable you are. And just like situation aspects, the character that creates it (in this case, the character that hit you) gets one free invocation on that consequence. They can choose to let one of their allies use the free invocation.

$\hrulefill$

Let's say that you get hit really hard and take a 4-shift hit. You check Box 2 on your stress track, which leaves you with 2 shifts to deal with. If you can't, you're taken out, so it's time for a consequence. You can choose to write a new aspect in the consequence slot labeled 2---say, Sprained Ankle. Those final 2 shifts are taken care of and you
can keep fighting!

$\hrulefill$

If you're unable to absorb all of a hit's shifts---by checking a stress box, taking consequences, or both---you're taken out.

## What Happens When I Get Taken Out?

If you get taken out, you can no longer act in the scene. Whoever takes you out narrates what happens to you. It should make sense based on how you got taken out---maybe you run from the room in shame, or maybe you get knocked unconscious.

## Giving In

If things look grim for you, you can give in (or concede the fight)---but you have to say that's what you're going to do before your opponent rolls their dice.

This is different than being taken out, because you get a say in what happens to you. Your opponent gets some major concession from you---talk about what makes sense in your situation---but it beats getting taken out and having no say at all.

Additionally, you get one fate point for conceding, and one fate point for each consequence you took in this conflict. This is your chance to say, "You win this round, but I'll get you next time!" and get a tall stack of fate points to back it up.

## Getting Better --- Recovering from Stress and Consequences

At the end of each scene, clear all of your stress boxes. Recovery from a consequence is a bit more complicated; you need to explain how you recover from it---whether that's drinking a healing potion, taking a walk to calm down, or whatever makes sense with the consequence. You also need to wait an appropriate length of time.

* Mild consequence: Clear it at the end of the scene, provided you get a chance to rest.
* Moderate consequence: Clear it at the end of the next session, provided it makes sense within the story.
* Severe consequence: Clear it at the end of the [scenario](#scenario), provided it makes sense within the story.

$\hrulefill$

**RENAMING MODERATE AND SEVERE CONSEQUENCES**

Moderate and severe consequences stick around for a while. Therefore, at some point you may want to change the name of the aspect to better fit what's going on in the story. For instance, after you get some medical help, Painful Broken Leg might make more sense if you change it to Hobbling on Crutches.


# Aspects and Fate Points

An aspect is a word or phrase that describes something special about a person, place, thing, situation, or group. Almost anything you can think of can have aspects. A person might be the Greatest Swordswoman in Hereva. A room might be On Fire after you knock over an oil lamp. After a time-travel encounter with a pre-historic Phanda, you might be Terrified. Aspects let you change the story in ways that go along with your character's tendencies, skills, or problems.

You spend fate points---which you keep track of with Fate Point tokens from Evil Hat Productions or glass beads or poker chips or some other tokens---to unlock the power of aspects and make them help you. You earn fate points by letting a character aspect be compelled against you to complicate the situation or make your life harder. Be sure to keep track of the fate points you have left at the end of the session---if you have \hyperref[refresh]{more than your refresh}, you start the next session with the fate points you ended this session with.

$\hrulefill$

You earned a lot of fate points during your game session, ending the day with five fate points. Your refresh is 2, so you'll start with five fate points the next time you play. But another player ends the same session with just one fate point. His refresh is 3, so he'll begin the next session with 3 fate points, not just the one he had left over.

$\hrulefill$

### What Kinds of Aspects Are There?

There's an endless variety of aspects, but no matter what they're called they all work pretty much the same way. The main difference is how long they stick around before going away.

Character Aspects: These aspects are on your character sheet, such as your \hyperref[Anchor-26]{high concept} and \hyperref[Anchor-27]{trouble}. They describe personality traits, important details about your past, relationships you have with others, important items or titles you possess, problems you're dealing with or goals you're working toward, or reputations and obligations you carry. These aspects only change under very unusual circumstances; most never will.

$\hrulefill$

Examples: Captain of the Skyship Nimbus; On the Run From the Witches of Chaosah;Attention to Detail; I Must Protect My Brother

$\hrulefill$

Situation Aspects: These aspects describe the surroundings that the action is taking place in. This includes aspects you create or discover using the create an advantage action. A situation aspect usually vanishes at the end of the scene it was part of, or when someone takes some action that would change or get rid of it. Essentially, they last only as long as the situational element they represent lasts.

$\hrulefill$

Examples: On Fire; Bright Sunlight; Crowd of Angry People; Knocked to the Ground

$\hrulefill$

To get rid of a situation aspect, you can attempt an overcome action to eliminate it, provided you can think of a way your character could accomplish it---dump a bucket of water on the Raging Fire, use evasive maneuvers to escape the enemy fighter that's On Your Tail. An opponent may use a Defend action to try to preserve the aspect, if they can describe how they do it.

Consequences: These aspects represent injuries or other lasting trauma that happen when you get hit by attacks. \hyperref[Anchor-29]{They go away slowly}, as described in Ouch! Damage, Stress, and Consequences.

$\hrulefill$

Examples: Sprained Ankle; Fear of Spiders; Concussion; Debilitating Self-Doubt

$\hrulefill$

Boosts: A boost is a temporary aspect that you get to use once (see "What Do You Do With Aspects?" next), then it vanishes. Unused boosts vanish when the scene they were created in is over or when the advantage they represent no longer exists. These represent very brief and fleeting advantages you get in conflicts with others.

$\hrulefill$

Examples: In My Sights; Distracted; Unstable Footing; Rock in His Boot

$\hrulefill$

**Player vs. Player**

The only time that fate point might not go to the GM is when you're in conflict with another player. If you are, and you invoke one of that player's character aspects to help you out against them, they will get the fate point instead of the GM once the scene is over.

$\hrulefill$

### What Do You Do With Aspects?

There are three big things you can do with aspects: invoke aspects, compel aspects, and use aspects to establish facts.

#### Invoking Aspects

You invoke an aspect to give yourself a bonus or make things a bit harder for your opponent. You can invoke any aspect that you

1. know about, and
1. can explain how you use it to your advantage---including aspects on other characters or on the situation.

Normally, invoking an aspect costs you a fate point---hand one of your fate points to the GM. To invoke an aspect, you need to describe how that aspect helps you in your current situation.

$\hrulefill$

* I attack the zombie with my sword. I know zombies are Sluggish, so that should help me.
* I really want to scare this guy. I've heard he's Scared of Mice, so I'll release a mouse in his bedroom.
* Now that the guard's Distracted, I should be able to sneak right by him.
* This spell needs to be really powerful---I'm the Head Teacher of Chaosah, so powerful spells are my bread and butter.

$\hrulefill$

What does invoking the aspect get you? Choose one of the following effects:

* Add a +2 bonus to your total. This costs a fate point.
* Reroll the dice. This option is best if you rolled really lousy (usually a −3 or −4 showing on the dice). This costs a fate point.
* Confront an opponent with the aspect. You use this option when your opponent is trying something and you think an existing aspect would make it harder for them. For instance, a thug wants to draw his dagger, but he's Buried in Debris; you spend a fate point to invoke that aspect, and now your opponent's level of difficulty is increased by +2.
* Help an ally with the aspect. Use this option when a friend could use some help and you think an existing aspect would make it easier for them. You spend a fate point to invoke the aspect, and now your friend gets a +2 on their roll.

Important: You can only invoke any aspect once on a given dice roll; you can't spend a stack of fate points on one aspect and get a huge bonus from it. However, you can invoke several different aspects on the same roll.

If you're invoking an aspect to add a bonus or reroll your dice, wait until after you've rolled to do it. No sense spending a fate point if you don't need to!

Free invocations: \hyperref[succeed-with-style]{Sometimes you can invoke an aspect for free}, without paying a fate point. If you create or discover an aspect through the create an advantage action, the first invocation on it (by you or an ally) is free (if you succeeded with style, you get two freebies). If you cause a consequence through an attack, you or an ally can invoke it once for free. A \hyperref[Boosts]{**boost**} is a special kind of aspect that grants one free invocation, then it vanishes.

##### Compelling Aspects

If you're in a situation where having or being around a certain aspect means your character's life is more dramatic or complicated, anyone can compel the aspect. You can even compel it on yourself---that's called a self-compel. Compels are the most common way for players to earn more fate points.

There are two types of compels.

Decision compels: This sort of compel suggests the answer to a decision your character has to make. If your character is Queen Coriander, for example, you may need to stay to lead the defense of the City of Qualicity rather than fleeing to safety. Or if you have a Defiant Streak a Mile Wide, maybe you can't help but mouth off to the Dean of Discipline when he questions you.

Event compels: Other times a compel reflects something happening that makes life more complicated for you. If you have Strange Luck, of course that spell you're working on in class accidentally turns the dour Potions Master's hair orange. If you Owe Prince Acren a Favor, then Prince Acren's messenger shows up and demands that you perform a service for him just when it's least convenient.

In any case, when an aspect is compelled against you, the person compelling it offers you a fate point and suggests that the aspect has a certain effect---that you'll make a certain decision or that a particular event will occur. You can discuss it back and forth, proposing tweaks or changes to the suggested compel. After a moment or two, you need to decide whether to accept the compel. If you agree, you take the fate point and your character makes the suggested decision or the event happens. If you refuse, you must pay a fate point from your own supply. Yes, this means that if you don't have any fate points, you can't refuse a compel!

$\hrulefill$

**How Many Fate Points Does the GM Get?**

As GM, you don't need to track fate points for each NPC, but that doesn't mean you get an unlimited number. Start each scene with a pool of one fate point per PC that's in the scene. Spend fate points from this pool to invoke aspects (and consequences) against the PCs. When it's empty, you can't invoke aspects against them.

How can you increase the size of your pool? When a player compels one of an NPC's aspects, add the fate point to your pool. If that compel ends the scene, or when an NPC gives in, instead add those fate points to your pool at the start of the next scene.

Fate points you award for compels do NOT come from this pool. You never have to worry about running out of fate points to award for compels.

$\hrulefill$

##### Establishing Facts

The final thing that aspects can do is establish facts in the game. You don't have to spend any fate points, roll dice, or anything to make this happen---just by virtue of having the aspect Ruddy Duck's Pilot, you've established that your character is a pilot and that you fly an airship named the Ruddy Duck. Having the aspect Mortal Enemy: Order of the Phoenix Dragon establishes that the setting has an organization called the Order of the Phoenix Dragon and that they're after you for some reason. If you take the aspect Witch of the Mysterious Circle, you not only establish that there's a group of witches called the Mysterious Circle, but that their type of magic exists in the setting and that you can perform it.

When you establish facts of the setting this way, make sure you do it in cooperation with other players. If most people want to play in a setting with only the six schools of magic listed in the Impossible Triangle, you shouldn't unilaterally bring in a separate magic organization into it through an aspect. Make sure that the facts you establish through your aspects make the game fun for everyone.

### Composing Good Aspects

When you need to think of a good aspect (we're mainly talking about character and situation aspects here), think about two things:

* How the aspect might help you---when you'd invoke it.
* How it might hurt you---when it would be compelled against you.

For example:

$\hrulefill$

I'll Get You, Mayor Bramble!

* Invoke this when acting against Mayor Bramble to improve your chances.
* Get a fate point when your dislike for Mayor Bramble makes you do something foolish when trying to get him.

Overly-Heightened Nerves

* Invoke this when being extra vigilant and careful would help you.
* Get a fate point when this causes you to be jumpy and be distracted by threats that aren't really there.

$\hrulefill$

Obviously, your trouble aspect is supposed to cause problems---and thereby make your character's life more interesting and get you fate points---so it's okay if that one's a little more one-dimensional, but other character and situation aspects should be double-edged.

# Stunts

Stunts are tricks, maneuvers, or techniques your character has that change how an approach works for your character. Generally this means \hyperref[Anchor-15]{you get a bonus in certain situations}, but sometimes it gives you some other ability or characteristic. A stunt can also reflect specialized, high-quality, or exotic equipment that your character has access to that gives them a frequent edge over other characters.

There's no definitive list of stunts that you pick from; much like aspects, everyone composes their own stunts. There are two basic templates to guide you in composing your stunts, so you do have something to work from.

The first type of stunt gives you a +2 bonus when you use a certain approach in a certain situation. Use this template:

Because I [describe some way that you are exceptional, have a cool bit of gear, or are otherwise awesome], I get a +2 when I [pick one: Carefully, Cleverly, Flashily, Forcefully, Quickly, Sneakily][pick one: attack, defend, create advantages, overcome] when [describe a circumstance].

For example:

$\hrulefill$

* Because I am a Smooth Talker, I get a +2 when I Sneakily create advantages when I'm in conversation with someone.
* Because I am a Lover of Puzzles, I get a +2 when I Cleverly overcome obstacles when I am presented with a puzzle, riddle, or similar conundrum.
* Because I am a World-Class Duelist, I get a +2 when I Flashily attack when engaged in a one-on-one swordfight.
* Because I have a Big Kite Shield, I get a +2 when I Forcefully defend when I use my shield in close combat.

$\hrulefill$

Sometimes, if the circumstance is especially restrictive, you can apply the stunt to both the create an advantage action and the overcome action.

The second type of stunt lets you make something true, do something cool, or otherwise ignore the usual rules in some way. Use this template:

Because I [describe some way that you are exceptional, have a cool bit of gear, or are otherwise awesome], once per game session I can [describe something cool you can do].

For example:

$\hrulefill$

* Because I am Well Connected, once per game session I can find a helpful ally in just the right place.
* Because I am Quick on the Draw, once per game session I can choose to go first in a physical conflict.
* Because I can Run Circles Around a Leopard, once per game session I can show up anywhere I want to, provided I could run there, no matter where I started.

$\hrulefill$

These templates exist to give you an idea of how stunts should be constructed, but don't feel constrained to follow them exactly if you have a good idea. If you'd like to read more about the construction of stunts, see *Skills and Stunts* in *Fate Core*.

Remember that we allow your character **four** stunts before requiring you to spend refresh and that each school has two **school stunts** to choose from. These are covered under [Stunts and Refresh](#stunts-and-refresh) in the [Character Creation](#character-creation) chapter.

# Getting Better at Doing Stuff: Character Advancement

People change. Your skills sharpen as you practice them. Your life experiences accumulate and shape your personality. Fate Accelerated Edition reflects that with character advancement, which allows you to change your aspects, add or change stunts, and raise your approach bonuses. You do this when your character reaches a milestone.

## Milestones

Stories in TV shows, comic books, movies, and even video games usually continue from episode to episode, season to season. It took Frodo three big books to take the Ring to the fiery mountain. It took Aang three seasons to defeat the Fire Lord. You get the idea. FAE can tell those kinds of stories; you play many game sessions in a row using the same characters---this is often called a campaign---and the story builds on itself. But within these long stories, there are shorter story arcs, like single episodes of a TV show or single issues of a comic, where shorter stories are told and wrapped up. FAE can do that too, even within a longer campaign.

In FAE, we call those wrap-ups milestones---whether they're small ones for short stories, or really big ones at the end of many sessions of play. FAE recognizes three types of milestones, and each one allows you to change your character in certain ways.

### Minor Milestones

A minor milestone usually occurs at the end of a session of play, or when one piece of a story has been resolved. Rather than making your character more powerful, this kind of milestone is more about changing your character, about adjusting in response to whatever's going on in the story if you need to. Sometimes it won't really make sense to take advantage of a minor milestone, but you always have the opportunity in case you need to.

After a minor milestone, you can choose to do one (and only one) of the following:

* Switch the ratings of any two approaches.
* Rename one aspect that isn't your high concept.
* Exchange one stunt for a different stunt.
* Choose a new stunt (and adjust your refresh, if you already have four stunts).

Also, if you have a moderate consequence, check to see if it's been around for two sessions. If so, you can clear it.

### Significant Milestones

A significant milestone usually occurs at the end of a \hyperref[scenario]{scenario} or the conclusion of a big plot event (or, when in doubt, at the end of every two or three sessions). Unlike minor milestones, which are primarily about change, significant milestones are about learning new things---dealing with problems and challenges has made your character generally more capable at what they do.

In addition to the benefit of a minor milestone, you also gain both of the following:

* If you have a severe consequence that's been around for at least two sessions, you can clear it.
* Raise the bonus of one approach by one.

$\hrulefill$

**RAISING APPROACH BONUSES**

When you raise the bonus of an approach, there's only one rule you need to remember: you can't raise an approach bonus above Superb (+5).

$\hrulefill$

### Major Milestones

Major milestones should only occur when something happens in the campaign that shakes it up a lot---the end of a big story arc, the final defeat of a main NPC villain, or any other large-scale change that reverberates around your game world.

These milestones are about gaining more power. The challenges of yesterday simply aren't sufficient to threaten these characters anymore, and the threats of tomorrow will need to be more adept, organized, and determined to stand against them.

Achieving a major milestone confers the benefits of a significant milestone and a minor milestone. In addition, you may do all of the following:

* Take an additional point of refresh, which you may immediately use to purchase a stunt if you wish.
* Rename your character's high concept (optional).
* Change your character's school (optional).


# Being the GM

The GM has many responsibilities, such as presenting the conflict to the players, controlling NPCs, and helping everyone apply the rules to the situation in the game.

Let's talk about the GM's jobs.

### Help Build Campaigns

A campaign is a series of games you play with the same characters, where the story builds on what happened in earlier sessions. All the players should collaborate with the GM to plan how the campaign will work. Usually this is a conversation among all of you to decide what sort of heroes you want to play, what sort of world you live in, and what sorts of bad guys you'll have. Talk about how serious you want the game to be and how long you want it to last.

$\hrulefill$

* Taking one of Coriander's flying ships and discovering the den of the airship pirates near Qualicity.
* Magic-wielding Hippiah farmers stand against the invading soldiers of the evil Steel Empire.
* Students of several magical schools of Hereva band together to solve mysteries and uncover secrets of each of their schools.

$\hrulefill$

<!--% FIXME: Incorporate those sections into this text?-->

**LEARNING HOW TO BE A GM**

Being a GM and running games can seem intimidating and difficult at first. It's a skill that takes some practice to master, so don't worry---you'll get better the more you do it. If you'd like to read more about the art of GMing Fate, there are several chapters in the Fate Core rules that you should check out: Running the Game, Scenes, Sessions, and Scenarios, and The Long Game are particularly helpful. Fate Core is available for free at [www.evilhat.com](https://www.evilhat.com).

$\hrulefill$

### Build Scenarios and Run Game Sessions

A scenario is one short story arc, the sort of thing you might see wrapped up in one or two episodes of an adventure television show, even if it's a smaller part of a bigger story. Usually you can wrap up a scenario in one to three game sessions, assuming you play for three or four hours at a time. But what is a scenario, and how do you build one?

#### Scenarios

<!--% FIXME: Need to figure out a better way to word this.-->

A scenario needs two things: A bad guy with a goal, and a reason the PCs can't ignore it.

Bad guy with a goal: You've probably figured this out already. The campaign's main opposition, or one of his allies, is probably your bad guy.

Something the PCs can't ignore: Now you have to give the PCs a reason to care. Make sure the bad guy's goal is up in the PCs' faces, where they need to do something about it or bad things will happen to them, or to people or things they value.

#### Running Game Sessions

Now that your bad guy is doing something the PCs will pay attention to, it's time to start them off. Sometimes the best way to do that, especially for the first session of a new story arc, is to put them right in the action. Once the PCs know why they should care about what's going on, you just get out of the way and let them take care of it.

That said, there are a bunch of tasks the GM needs to perform to run the session:

* Run scenes: A session is made up of individual scenes. Decide where the scene begins, who's there, and what's going on. Decide when all the interesting things have played out and the scene's over.
* Adjudicate the rules: When some question comes up about how to apply the rules, you get final say.
* Play the NPCs: Each player controls their own character, but you control all the rest, including the bad guys.
* Keep things moving: If the players don't know what to do next, it's your job to give them a nudge. Never let things get too bogged down in indecision or because they don't have enough information---do something to shake things up.
* Make sure everyone has a chance to be awesome: Your goal isn't to defeat the players, but to challenge them. Make sure every PC gets a chance to be the star once in a while, from the boldest Chaosah Witch to the meekest Hippiah Witch.

### Setting Difficulty Levels

When another character is opposing a PC, their rolls provide the opposition in a conflict, contest, or challenge. But if there's no active opposition, you have to decide how hard the task is.

Low difficulties are best when you want to give the PCs a chance to show off and be awesome. Difficulties near their approach ratings are best when you want to provide tension but not overwhelm them. High difficulties are best when you want to emphasize how dire or unusual the circumstances are and make them pull out all the stops.

#### Rules of Thumb:

* If the task isn't very tough at all, give it a Mediocre (+0)---or just tell the player they succeed without a roll.
* If you can think of at least one reason why the task is tough, pick Fair (+2).
* If the task is extremely difficult, pick Great (+4).
* If the task is impossibly difficult, go as high as you think makes sense. The PC will need to drop some fate points and get lots of help to succeed, but that's fine.

$\hrulefill$

**OPTIONAL RULE: APPROACH-RELATED TARGET NUMBERS**

Sometimes being Careful makes things a lot easier; sometimes it just takes too long. The GM may wish to adjust the target number up or down by 1 or 2 depending on whether you choose a fitting or a problematic approach. This makes things a bit more complex, but for some groups it's worth it.

$\hrulefill$

### Bad Guys

When you make a bad guy, you can stat them out exactly like the PCs, with approaches, aspects, stress, and consequences. You should do this for important or recurring bad guys who are intended to give the PCs some real difficulties, but you shouldn't need more than one or two of these in a scenario.

Mooks: Other bad guys are mooks---unnamed thugs or monsters or goons that are there to make the PCs' day a little more difficult, but they're designed to be more or less easily swept aside, especially by powerful PCs. Here's how you create their stats:

1. Make a list of what this mook is skilled at. They get a +2 to all rolls dealing with these things.
1. Make a list of what this mook is bad at. They get a −2 to all rolls dealing with these things.
1. Everything else gets a +0 when rolled.
1. Give the mook an aspect or two to reinforce what they're good and bad at, or if they have a particular strength or vulnerability. It's okay if a mook's aspects are really simple.
1. Mooks have zero, one, or two boxes in their stress track, depending on how tough you imagine them to be.
1. Mooks can't take consequences. If they run out of stress boxes (or don't have any), the next hit takes them down.

<!--% FIXME: Make better themed NPCs?-->

$\hrulefill$

**CYCLOPS HOUSE BULLY**

Cyclops House Bully, Cowardly Without Backup

Skilled (+2) at: Frightening other students, weaseling out of trouble, breaking things

Bad (-2) at: Planning, studying

Stress: \DB{} (first hit takes them out)

$\hrulefill$

**STEEL ASSASSIN**

Steel Assassin, The Night Is Ours

Skilled (+2) at: Sneaking, ambushing

Bad (-2) at: Standing up to determined opposition

Stress: \SA{}

$\hrulefill$

**SKY SHARK**

I'm a Shark, Vulnerable Belly

Skilled (+2) at: Flying, biting

Bad (-2) at: Anything that isn't flying or biting

Stress: \SB()

$\hrulefill$

Groups of Mooks: If you have a lot of low-level bad guys facing the PCs, you can make your job easier by treating them as a group---or maybe a few groups. Instead of tracking a dozen bad guys, you track three groups of four bad guys each. Each of these groups acts like a single character and has a set of stats just like a single mook would:

* Choose a couple of things they're skilled at. You might designate "ganging up" as one of the things the group is good at.
* Choose a couple of things they're not so good at.
* Give them an aspect.
* Give them one stress box for every two individuals in the group.

$\hrulefill$

GANG OF THUGS

Axe Handles & Crowbars

Skilled (+2) at: Ganging up, scaring innocent people

Bad (-2) at: Thinking ahead, fighting when outnumbered

Stress: \SB (4 thugs)

$\hrulefill$

Fate Core has a way of handling this, called mobs (see the "Creating the Opposition" section of the **Running the Game** chapter in Fate Core). Feel free to use that option if you prefer. Note that it may lead to very strong mobs, unless you start with extremely weak mooks---if you want to give your PCs a serious challenge, that could be one way to do it.

There are more NPCs and creatures in the \hyperref[CreaturesInHereva]{Creatures in Hereva} section. 

# Magic in Hereva

*Hereva is a world full of magic. Magic permeates all life in Hereva, from the simplest tasks to the most powerful of spells. The name of this magic is called "Rea" and it is vital to all life on Hereva. Six schools were created to study Rea and its effects on the world from their unique perspectives.*

## Where does magic come from?

Hereva was born from the magical forces known as "Chaos & Evolutions". "Chaos & Evolutions" exist within the cosmos and Hereva formed when the magic elements of the cosmos evolved from the chaos. Hereva's magical nature permeates it so thoroughly that any practitioner of magic will have little difficulty in finding the resources to manipulate some form of magic. Magic is not without discipline though, and practitioners of magic must undergo specialized training in order to properly channel the magical energies within the world. 

Magic in Hereva is unlike our concepts of "Qi" and "Mana". Because magic formed the universe of Hereva it is called "Rea", which is an abbreviation of "Reality" . Rea can be loosely thought of as collecting the by-products of a task. By concentrating on making a potion for a loved one the practitioner can harness some of the Rea used in creating the potion. Conversely if there is no attachment, or if the practitioner is careless then the Rea is lost and the Rea must be obtained via other means. 

Rea can be harnessed by the following methods:

* The process of summoning
* The time or duration while making something
* Attention, dedication, or care
* Emotional engagement

Rea is used in all aspects of life on Hereva. Farmers in the village of Squirrel's End use Rea to grow healthier crops (and tastier crops, as the locals will attest). They also use Rea to ward off the denizens of the forest (which can also attest to the tastiness of the crops in Squirrel's End). Most Witches of Hereva don't use their Rea for mere "chores", preferring instead to save their Rea for more important matters. This is why you'll find witches doing things like sewing on a button or purchasing starfruit from market rather than conjuring up a new outfit or summoning starfruit: the cost in replenishing the Rea outweighs the benefit.

Most practitioners don't actively monitor their Rea usage; they can sense when they are nearing depletion and can seek out ways to regenerate or obtain Rea as needed. How much Rea a person can maintain / channel varies greatly. Some folks are able to charge up luminous levels of Rea. A few witches are skilled in channeling and manipulating large amounts of Rea without the need to build up reserves. Such channeling can be dangerous, though; while it is difficult to get too much Rea it is possible certain spells can consume all of the Rea from the caster and the surrounding area. Such spells can lead to disastrous results.

Training in one of the schools of magic allows for more controlled and focused use of Rea. Without this training Rea becomes more of a "guided intention" rather than a direct command of Rea. By careful study, focus, and desire one can learn to harness Rea to conform to their will. As the Herevans learned how to control Rea they quickly realized this power could cause havoc if not properly taught, and formed the schools of magic to properly teach the various ways of harnessing Rea. They perfected their craft via trial and error, and compiled that information into spell books and potion recipes. Some schools became more secretive about their methods for controlling Rea, while others remained open to all students (mostly from necessity, but some because of the charters of their founding members). Most of the people of Hereva know a little bit of the schools of magic they were exposed to (for instance the farmers of Hereva know a diluted form of Hippiah magic passed on from one generation to the next) but there is a huge difference between mimicking the magic of a school and knowing all of their secret knowledge.

Magmah, Aquah, and Hippiah are especially tuned to channel Rea into the physical world. Ah and Zombiah focus their Rea outside of the physical world. Chaosah is attuned to both the physical and non-material worlds.

Rea must be replenished from time to time. While Rea can be replenished in various ways (meditation, careful focus on a project, dedication to a project, etc.) these take time and effort to achieve. Certain potions and rare ingredients can be purchased to replenish Rea, but these can be quite costly and are used by a limited number of practitioners. Beware anyone who claims to have invented a way to regenerate Rea without effort; such "perpetual Rea machines" are usually little more than "get-Ko-quick" schemes.


## The Impossible Triangle

Hereva's magic-system, or "Chaos & Evolutions" has evolved into six magic schools. They are represented on the Impossible Triangle diagram.

\begin{figure}[H]
    \centering
        \includegraphics[scale=0.18]{images/2015-06-08_magic-system-of-hereva_by-David-Revoy.jpg} 
    \end{figure}

## Reading The Impossible Triangle

The best way to read the Impossible Triangle is to view it on several axes: at the top is order, and the bottom is chaos. The left is spiritual and the right is material. With this we can read the positions of the different schools and better understand their affinities.

Starting from the bottom is **Chaosah**, harnessing the primal forces of chaos and physics. Moving to the left we see **Aquah**, the fusion of the untamed forces of chaos with the calm order of the spirit; much as water can turn from quiet and calm to rough and tumultuous. On the top left is **Ah**, representing the evolution into spiritual beings (beings requiring no physical form) and the order that comes from shirking off the material form. Moving right is **Hippiah**, the combination of harnessing the spiritual aspects of all living things and using them in order to enact the growth of living things. At the top-right is **Zombiah**, which views the spiritual as something to be harnessed to drive and animate the physical world. Moving down towards chaos is **Magmah**, which views the natural order as something to be changed and refined through the mixture of alchemy and chaos. 

The following sections explore each of the magical schools in more depth.


# Magic School: Chaosah

Chaosah is at the base of the impossible triangle, and forms the basis of every other magic in Hereva. It's the school of **time, gravity, particle physics, and underground divine forces**. Some have postulated that Chaosah magic flows and controls all of the magic on Hereva in minute ways, but Chaosah practitioners tend to work in closed quarters and are loathe to share their magical secrets with outsiders. Chaosah magic, more than other magical systems, values practical magic over flashy displays (and instances where this value is not heeded have shown exceptional consequences). Chaosah magic can alter the shape and size of living beings, and can even open portals (referred to as "black holes") into worlds outside of Hereva. Chaosah does not operate as a traditional school. Students do not submit applications for enrollment; instead Chaosah prefers to use their own recruiting methods.

## Chicory and the founding of Chaosah

Chaosah was founded by a young witch named Chicory. She was a curious witch, and loved to tinker with magic. One night she was playing around and happened upon the "Chaosah Black Hole" spell (which is a signature spell for all Chaosah witches). She named her magic "Chaosah" because she felt she was channeling the chaos of Hereva in order to perform her magical incantations.

\begin{wrapfigure}{r}{0.50\textwidth}
    \centering
        \includegraphics[width=0.5\textwidth]{images/2019-10-17_black-hole.jpg} 
\end{wrapfigure}

Her discoveries caught the attention of other like-minded witches and she taught them what she had learned. Eventually more students arrived and Chicory found herself spending every waking moment teaching students. Her original plan was to have several of her brighter students teach the newer students, but the students wanted to hear the words of Chicory direct from the source. So she appointed her first apprentice. The apprentice is known as "the unnamed apprentice" in the Chaosah literature. This apprentice was eager to please Chicory. Chicory grew more and more frustrated and impatient the longer she was away from her research, and her apprentice tended to receive the results of her frustration and impatience.

One day Chicory needed something from the top shelf of one of the potion racks. "The unnamed apprentice" wanted to please an already frustrated Chicory, and decided to get her the potion as quickly as possible. Unfortunately the apprentice did not think there was enough time to get a ladder and instead decided to climb the wobbly potion shelves to get the potion. The shelves buckled under the weight, and the potions shattered and spilled all over "the unnamed apprentice". Several of the potions combined in strange and unpredictable ways, and "the unnamed apprentice" vanished. Even the records of the event still exhibit potion-induced time distortions and it is feared the true fate of "the unnamed apprentice" may never be known. This lead to one of the first rules of Chaosah: "A true witch of Chaosah doesn't climb the potion shelves".

After the potion incident Chicory selected someone to replace her (now absent) apprentice. Though she could no longer have a true apprentice (as the rituals surrounding apprenticeship are made for life, and the hope was the apprentice was still alive, somewhere) she selected an eager young witch named Thyme to take on the role. Thyme worked closely with Chicory to ensure that Chicory had more time to do her research, but despite Thyme's best efforts to wall off her mentor there were still folks who demanded they receive instruction from Chicory herself. The demands of running the school and "being the leader of Chaosah" took their toll on Chicory. Legend has it that Chicory decided she had completed her teaching of the Witches of Chaosah and decided to seclude herself in a pocket dimension where she could continue her research undisturbed. Whether she will ever return is the stuff of speculation and legend. Before the demise of Chaosah there were a small band of witches who would hold a vigil waiting for the eventual return of Chicory. Those who were closest to Chicory (including Thyme) figured it was best to let her do her research in peace, lest she return in a more powerful and agitated state.

Thyme took on the duties of running the school but some students felt she lacked the authenticity of Chicory. Gradually those students drifted away to other schools. Thyme resented their departure, and that resentment fueled her late nights of research into Chaosah Magic. Some say this period of research was greater than anything Chicory had published in the formative years of Chaosah. There are more manuscripts with Thyme's handwriting on them than any other Chaosah witch to date. Thyme even perfected the micro dimension through trial and error. Only foolish witches make the connection to Thyme's experiments in the micro dimension and Thyme's current stature.

Gradually the school of Chaosah became more secretive and secluded as the students departed. The school did minimal recruiting effort to keep the school going. There were many periods where Chaosah would be very selective, and periods where Chaosah would take anyone that wanted to come. One thing that all of students had in common was the ability to withstand Thyme's fits of rage. Only Cumin has managed to withstand enough of Thyme's rants and threats long enough to become Thymes apprentice. Whether Thyme made Cumin an apprentice as a favor to someone, or just to get Cumin to stop nagging her is anyone's guess. Some Chaosah witches wondered if Cayenne would have been Thyme's apprentice had Cumin not been accepted before Cayenne arrived. (They also wondered if Thyme kept sending Cumin on dangerous missions in an effort to get Cayenne the apprenticeship promotion.)

## Burying failures

A curious habit of Chaosah is the habit of burying items. Chaosah has a long-standing tradition of taking their physical mistakes and literally burying them in the ground. This practice traces its origins back to Chicory, the founder of Chaosah magic. An unnamed student approached her with a potion or a spell that did not work (the tradition speaks of both spells and potions, depending on who tells the story). Chicory looked at the potion / spell and declared "This wasn't worth the Rea it took to make it. Perhaps you can reclaim some of the Rea you wasted in making it in the effort you'll use to bury it". As Chicory was a wise and feared teacher of Chaosah the wisdom became that any project that had no hope of recouping the cost involved in making it would be buried, both to reclaim the Rea used in creating it and as a deterrent to creating such foolishness in the future. Usually the creator is tasked with burying their failed projects and experiments, although there was one class that so displeased Chicory that she made the whole class dispose of each other's failures as punishment.

Even a failed Chaosah experiment can be quite powerful, and burying them can lead to unintended consequences. Potions can leak, spells can be read by unskilled practitioners, and defective talismans can wreak all sorts of havoc. Worse, these are the ones that Chaosah witches deem are not good enough to keep around. A failed potion labeled "growth potion" could shrink you, make every other part of you grow, or cause your hair to fall out (and those are the best case scenarios). This is of no concern to Chaosah witches who deem such interactions with their failures as happy little accidents.

## Chaosah and The Great War

During the great war all of the members of Chaosah were wiped out by the forces of Ah. This upset the balance of Hereva and caused The Great Tree of Komona to perish, leaking Rea throughout the land. Realizing Chaosah was an integral part of the magic of Hereva the adepts of Ah recruited Zombiah to resurrect enough of Chaosah to train an heir. Only three of the witches of Chaosah were able to be adequately resurrected: Cayenne, Thyme and Cumin. Once the witches of Chaosah returned the balance of Hereva was restored. As part of their return they are charged with producing an heir for the school of Chaosah as the terms of their resurrection prevents them from continuing it themselves. After their resurrection the witches of Chaosah signed a treaty with Ah. This treaty states that the witches of Chaosah will train their heir and then give up their physical forms. The treaty also locks down Chaosah to the bare essentials for practicing their magic. It is Ah's hope that by this treaty Chaosah can be contained.

## Chaosah Today

Chaosah is a shell of its former glory. Bound by the terms of the treaty they while away their time training an heir. They have few other pursuits outside of training Pepper as their heir. The remaining witches of Chaosah feel they have been stripped of their former glory and are no doubt figuring out how to best leverage this heir to win back their rightful place as one of the most powerful and feared schools of Hereva.

$\hrulefill$

**A True Witch of Chaosah**

* "[is a] mean witch of Chaosah." (ep. 11)
* "[commands] fear, obedience and respect." (ep. 11)
* "... has influence over the powerful!" (ep. 11)
* "... does not use magic for daily chores." (ep. 12)
* (regarding frivolous potions) "... wouldn't create these kinds of potions." (ep. 12)
* "... needs neither compass nor map. A starry sky will suffice." (ep 17)

**Other wisdom of Chaosah**

* "A good dark stare is worth a dozen curses! ... Stare them down. Dominate them!" --- Cayenne (relayed by Pepper in ep. 17)
* "To know the edible plants is to know where to find ready-made potions against hunger!" --- Cumin (relayed by Pepper in ep. 17)

$\hrulefill$

## Creating a Chaosah Character

Chaosah characters tend to prefer to work in secret. A Chaosah character that has been raised in the Chaosah school should have a "Good" rating for their Sneaky Approach. Thyme is very Sneaky, and her students have learned to be sneaky as well. Pepper, on the other hand, spent her formative years as a Hippiah witch. She is not nearly as Sneaky as Thyme, Cumin, or Cayenne. Her Sneaky Approach is "Average", while her Quick Approach is "Good". Pepper favors getting through things quickly rather than being sneaky about it.

Chaosah witches are also fairly clever, so having a "Fair" rating for your Clever approach would be helpful. Cumin is an example of a Chaosah witch that is not particularly clever, so her Clever approach is "Average". She makes up for this by being extra careful (Careful = "Great", which is one level above "Good", reflecting her experience).

Chaosah witches do not tend to be Quick or Flashy, so setting those to "Average" is advised.

## Chaosah School Stunts

Each member of Chaosah may have the following school stunts:

* Because the School of Chaosah is **Feared throughout Hereva**, once per game session I can **intimidate** someone who is afraid of Chaosah Witches.
* Because the School of Chaosah **controls the chaos**, I get +2 when I **Cleverly Overcome Obstacles** when I can start a chain reaction of events.

# Magic School: Magmah

Magmah is at the middle-right of the triangle, and like its counterpart **Hippiah** is considered a major school of magic. It's the house of **cooking, baking, grilling, boiling, frying, steaming, and toasting** but is also the school of **metallurgy, alchemy, and thaumaturgy**. It's also similar to Hippiah in that it is a well documented magic school, though Magmah charges more for access to its lessons than Hippiah. Adepts of Magmah tend to use a blend of Hippiah and Magmah magic to create amazing dishes, as well as strong materials for other magical schools (notably Zombiah, which uses those materials for their animating purposes). Magmah is a part of daily life in Hereva, and has found uses in both the home as well as for wars and industry. During the "great crisis" (a moment in Hereva where drought and blight were at their peak) the schools of Magmah and Hippiah realized that they could do more good by teaching a diluted form of their magic to the people of Hereva. Unlike Hippiah, though, Magmah charged "modest fees" for their knowledge, and set up subscription services where eager participants could receive lessons via crystal ball or via Herevan mail. Because of these lessons many folks on Hereva know a small portion of Magmah, though the difference between a dabbler in Magmah Magic and an true adept of Magmah Magic is huge. This also helped swell the number of students wanting to attend the school of Magmah, which has become very selective in which students get to attend. Admission fees for the school of Magmah can be quite expensive, though scholarships are available to qualified students.

## Founding of Magmah

Magmah was founded by three witches who started off using Hereva's magic for their catering business. Their names were Savory, Mint, and Chive. Their magic primarily focused on creating tasty cuisine, baking delicious pastries and cakes, and delighting their patrons. All of these spells formed the core of Magmah magic: using magic to heat and combine different materials into a different material. The three witches soon realized their magic could also be used for creating pots, pans, and other cooking utensils. They expanded their knowledge to create silverware and dishes for their patrons, crafting exquisite one-of-a-kind place-settings for every meal. Their catering business fourished as folks demanded to know who catered the meal and where the fine dishes came from. Eventually the school of Magmah was created as Savory, Mint, and Chive couldn't keep up with the demand and needed to train additional students to help with their business. Heliantha, was responsible for founding the school, while Savory, Mint, and Chive kept the catering side of Magmah active. Heliantha was more interested in teaching and researching techniques rather than the day-to-day operations of the catering business. She quickly formed a group of students who were eager to learn the secrets of Magmah. Each of these students spent their time researching and perfecting the recipes which were later used in the Magmah catering business. Paprika, one of the first students enrolled in the Magmah school, figured out that the magic of Magmah could be used to make intresting firework displays and showy displays of different colored fires and fireworks. This quickly became part of the Magmah catering business, which not only provided the catering but also the entertainment for any function.

During The Great Crisis where food was scarce the school chose to help others learn how to extend and prepare what they had. They worked in tandem with the school of Hippiah to ensure that folks were making delicious dishes out of the plants and food they had. This diluted form of Magmah magic is present throughout Hereva as generations of Herevans pass this knowledge on to their kids and their community. There was worry that passing this knowledge might cause demand for Magmah's dishes to dwindle but it has only served to make folks wonder how a real Magmah witch would prepare these dishes. Demand for Magmah catered events skyrocketed until the school had to raise prices to stave off demand.

The School of Magmah doesn't cater many events these days (the price per plate for Magmah to cater an event is quite high, even by wealthy Komonan standards) but their catering can still be found at events like coronations, exclusive dinners, and the occasional charity dinner (for a "modest" fee per plate, of course). Original Magmah silverware and plates are valued by collectors and command high prices at auction (and in the underground thieves markets as well). Magmah ensures that every piece of silverware and dish is accounted for, and charges the host if any of these go missing. Needless to say, bussing tables quickly and efficiently, and ensuring that the guests don't aimlessly wander off with an errant spoon or fork, becomes a priority at these events.

Komona is synonymous with the School of Magmah, deriving a lot of their wealth from Magmah's ties with the city. As Magmah's school and catering business thrived so too did Komona. Magmah was a large donor of many city projects, and the tourism to Magmah's occasional charity dinners brought folks from all over Hereva. The founding witches all found themselves incredibly wealthy. Cerberus still has one of the unoccupied mansions of Savory which is available for occasional tours. Unfortunately getting into Cerberus is still a bit troublesome as the gated community is so exclusive that even Mayor Bramble himself has trouble getting past the checkpoint.

## Magical Contests

Magmah hosts an occasional contest for determining who has best mastered the techniques of Magmah. Competition is fierce and those who make it through the rigorous tests and trials are considered for deeper studies. These contests are held in cooperation with the government of Komona. Mayor Bramble spared no expense for the latest magical contest, having a giant floating arena constructed for grand magical spectacles. The success of the last contest brought huge amounts of prestige and Ko to the city of Komona, guaranteeing that another magical contest is certain to happen.

Magmah's potion contests came about from the desire to determine who was capable of making the tastiest dishes in all of Magmah. Different formats were considered, including one format that pitted several Magmah students against one another in a large arena. These contests went on for some time until one inept participant created a dish that smelled so bad and was so foul to the taste it caused all of the judges to resign on the spot. From there it was decided that magic demonstrations would be more suitable for the competition format, and Magmah held several of these amongst themselves before challenging other schools to show their skills. Over time participation in the magical contests has been more political, with each school using the contest as a matter of prestige and honor. One can look back at the participation in these contests to get a sense of how well each school views the others and how much they wish to impress the other schools. It wasn't until recently that Chaosah, Hippiah, and Aquah even attended these contests. Why they have all participated in this latest contest (outside of the large amount of money) is a question that only each school can answer.

## Magmah and The Great War

The Great War started as a result of an argument between Magmah and Aquah. Nobody knows for certain what the argument was, but tensions were already high between Aquah and Magmah over other slights and altercations. Magmah log believed that Aquah was responsible for certain weather patterns that roamed straight through Komona. Aquah denied this, but then countered that Magmah would hover over various lakes in Magmah and drain them for water. Tensions rose between the two, and eventually they boiled over into a major conflict in which all of the schools participated. As part of the treaty (and as punishment for starting the war) the school was renamed "Kielbasah" by Aquah for a period of 10 years after the war. "Kielbasah", Aquah explained, "is the smell we closely associate with all of Magmah". It was also the name that Magmah strongly objected to, so all the better. During that time all records referring to Magmah were magically altered to reflect the change, including Magmah's own spell books, newspapers, and historical records. Even writing "Magmah" on a piece of paper was magically altered into "Kielbasah", much to the delight of young children growing up during this time. So powerful was this spell that it even detected misspellings and would replace it in casual conversations. Once the 10 years of penance had expired the name "Magmah" once again appeared in the records. There are still folks who have trouble remembering what Magmah's name is, so hearing someone mention "Kielbasah" is not uncommon, though it is considered necessary and polite to gently correct them when it occurs.

## Magmah today

<!-- FIXME: Need more about Magmah in current times.-->
Magmah was the school that rebuilt the quickest of the six schools of Hereva. After the Great Tree of Komona was nearly destroyed many came from across Hereva to witness what had happened to the tree. Komona soon grew a marketplace where folks come from all over Komona to purchase their wares. Magmah magic is one of the more popular schools of magic, delivering impressive results in a short amount of time. Magmah is the only school with a waiting list of potential applicants. Becoming a Magmah witch is not easy; participants must pay 5,000 Ko (non-refundable) just to submit an application to be on the waiting list. The school is highly selective, and each applicant must pass a series of tests in order to be considered for the school.

The current leader of Magmah is Vanilla, who was a student of Heliantha. The most famous of the Magmah witches is Saffron, who has bested all of the other Magmah witches in several competitions, and has participated in two magical contests. She was able to open her own consulting business after folks witnessed her entry in the first Magical Contest. 
She is a celebrity throughout Hereva, traveling in the finest luxury vehicles to various gatherings. 

While Magmah is highly selective that doesn't mean that they are the best-of-the-best. The Magmah of today is more concerned with image and style than the Magmah of old. Students who sign up for Magmah might find themselves learning more about silverware placement and flashy displays than powerful magic. That isn't to say that Magmah witches are incapable of producing powerful results, but they may require more effort than previous generations of Magmah witches.

## Creating a Magmah Character

Magmah characters are very concerned about appearance and status, so having at least a "Good" rating in Flashy is advised. Magmah witches are also well-connected, so they prefer making grand entrances instead of sneaking in through the back door (though if things are going bad they're likely to attempt sneaking out through the kitchen). Having an "Average" or "Fair" in your Sneaky approach would be fine.

Magmah witches also prefer speed over being careful so having your Quick approach be "Fair" and your Careful approach be "Average" would make you an excellent Magmah witch.

## Magmah School Stunts

Each member of Magmah may have the following school stunts:

* Because the School of Magmah is Well Connected throughout Hereva, once per game session I can find a helpful ally in just the right place.
* Because the School of Magmah can control fire, I get +2 when I Flashily create advantages related to fire and fireworks.


# Magic School: Zombiah

Zombiah is at the top-right of the Impossible Triangle, and it's one of the three sub-schools of "Evolution". It's the most curious of the houses, finding itself at the intersection of the macabre (**death, zombies, and darkness**)  and transformation (**recycling, re-use, and animation/reanimation**). Zombiah is not specifically about raising the dead, but rather concerns itself with bringing out the innate movement in all things. It wasn't until centuries of economic strife that Zombiah was applied to formerly living creatures.

While Zombiah can be used for reanimating formerly living creatures it's main use is for the machinery of Qualicity. Zombiah requires that resurrection only happen if the spirit is willing. (It's unclear to outsiders how this negotiation between body and spirit occurs, but Zombiah insists it does happen.) There is a quasi-acceptance between adherents of the school of Ah and Zombiah. Adherents of Zombiah view their magic as just another tool to help aid in making lives easier through automation, and prolonging the usefulness of the body by bringing the spirit back if it is willing. Ah followers contend the body must rest once the spirit takes leave, lest the spirit forever wish to remain within it's former home. Numerous times have the philosophers of Hereva tried to address this question of the connection of body and spirit. Some believe that only followers of Ah know the answer but the school of Ah refuse to participate in the discussion. The practice of Zombiah continues, though primarily on mechanical constructs and the dwindling few who will themselves into resurrection. There are signs that part of the treaty from The Great War resulted in the curtailing of the use of Zombiah on living beings but those details are kept hidden in a magical lock box that few can access. 

Adepts of Zombiah tend to mingle their magic with Magmah in order to craft better materials to animate. Those that have mastered both can create amazing clockwork machinery that mimics life in minute detail. Zombiah magic has been used to create machines such as robot butlers, electrical generators, factory assembly lines, and the like. Some folks in Hereva are still uneasy around moving, non-living constructs. Some have described Zombiah's constructs as uncanny and eerie, though some find them charming and endearing. This has drawn mixed responses from the other magic schools who wonder if such constructs are capable of independent thought. Some folks speculate that the reason Ah continues to monitor Zombiah is to see if these constructs might have a spiritual existence. 

## Founding of Zombiah

Zombiah began out of necessity. The residents of what would later become Qualicity were dwindling in number. They spent long days working the land and gathering food, but those efforts were not enough. The people couldn't put in enough hours to get enough food which resulted in less food and more starvation. Young Apiaceae saw her mother and father suffering as they tried to provide for their children. Apiaceae thought and thought. While her parents were able to use some magic to help with their gathering, they were exhausted by the end of the day, and had even less reserves to spend on gathering for the next day. One day, while Apiaceae and her sister Anemone were playing with some makeshift dolls Apiaceae thought "I wish this doll could do the work for my parents". Suddenly the doll moved a bit. Apiaceae was startled. She called over to her sister Anemone and together they wished the doll to life. It made some awkward movements and then fell to the ground. Excited, Apiaceae and Anemone rushed to tell their parents about their amazing discovery. Their parents were angry with them. Why were they using their precious Rea to make a doll move? That was frivolous. When Apiaceae protested that she was trying to help her parents got even more angry. If Apiaceae and Anemone were going to waste their time making dolls move then their parents threatened they were ready for hard days labor gathering food for the rest of the starving people. Apiaceae was crushed, but undeterred. Over the next few days she and her sister secretly got the doll to move with ever more coordination, until the doll was able to move and set down blocks. The initial experiments were exhausting, but as Apiaceae and Anemone practiced they realized they could make the doll move with less effort. Then other toys were pressed into service until the contents of an entire toy box were busily performing chores, moving things around, and emulating the work of Apiaceae and Anemone's parents. It became difficult for Apiaceae and Anemone to hide these animated toys from their parents. Apiaceae took careful notes of what caused different toys to become animated and both girls slowly figured out what worked well and what didn't. Eventually Apiaceae got the idea to combine several of their toys together to create a larger humanoid-like figure to help the girls with their chores. The automaton worked well, and Apiaceae and Anemone found they had more time to work on perfecting their incantations. Eventually their secret experiments were discovered by their parents. At first their parents were furious, but Apiaceae and Anemone pleaded their reasoning for making their toys move. Their parents realized that their daughters were doing this out of love for them and the others in the city.

The capabilities of the automaton grew quickly. It mastered simple tasks with ease, like following while carrying an object. Soon it was taking on more complex tasks like drying and putting away dishes (with a few minor accidents along the way). Before long it was able to do rudimentary agricultural work like tilling and planting. The girls kept perfecting the automaton, carefully noting everything they learned while teaching the automaton new tasks. Neighbors started to notice this hulking mass of cobbled-together toy parts doing chores around the house. The neighbors were terrified because they didn't know how this was possible, but eventually they realized the practical benefits of such a device. One fault of the automaton was that it was rather flimsy because it was made of toys. A neighbor offered to help make sturdier arms and legs out of wood, which the girls accepted graciously. Another offered an old chest for a sturdier body. Eventually the automaton grew in strength and capability, and Apiaceae and Anemone found themselves directing the automaton to the various houses so it could help with chores while their neighbors were out working. Demand outstripped supply, so Apiaceae and Anemone began sharing what worked so that others in their neighborhood could build their own automatons. Some were more successful than others, and the ones that were more successful began trading ideas for improvements with the girls. These notes and ideas formed the basis for Zombiah magic.

As Apiaceae and Anemone matured they decided to dedicate their lives toward helping folks automate their mundane tasks. Gradually the people of the city didn't have to work as hard for food and sustenance and the city began to flourish. The ruling monarch at the time, Prince Regent Monarda, welcomed Apiaceae and Anemone into his court and asked them to help him spread automation to the rest of the city. They agreed and soon their automation ushered in a golden age of prosperity and culture. Apiaceae and Anemone worked to share their knowledge and Zombiah grew even further.

Unfortunately tragedy struck when Prince Regent Monarda was assassinated prior to his coronation. An automaton programmed to help the Prince Regent get dressed that morning was given a secret program that caused it to kill the Prince Regent on his coronation day. The city was thrown into chaos. The obvious chaos was the power vacuum this created because Prince Regent Monarda had no heirs, but far worse was the realization that someone could command these automatons to do harm to living beings. All of the automatons were shut down and their programming carefully reviewed. This caused the city to slide back into the old ways before the automation. Uncertainty reigned throughout the kingdom. What good was a tool if you couldn't trust it? Who would lead the people and bring back the golden age of prosperity?

Apiaceae and Anemone worked hard to convince the people of the city that building and improving automatons were still a positive change that benefited all of the citizens. Anemone worked even harder to explain to people just how Zombiah magic could be used to create these machines and what their capabilities were. They implored the people to continue using the automatons to help them with their tasks. Many people still found value with the machines, but some were still horrified that automatons could be used for assassination and murder. These folks tried first to make the automatons illegal, and then worked to create laws for how the automatons could be used. Anemone helped draft up the laws that eventually were programmed into every automaton. These laws prevented the automatons from being used to harm others. This helped settle the panic over these automatons, but there have still been incidents where automatons and machines have caused harmed due to faulty programming.

Apiaceae and Anemone were not as directly involved with the monarchs that followed Prince Regent Monarda, though they were responsive whenever requested to demonstrate their latest innovations to the court. Some monarchs were more receptive than others. Gradually the aristocracy of the city became more receptive to Zombiah magic, and the second golden age occurred. The second golden age echoed of the first golden age, but the effects last into the present day. Later monarchs embraced Zombiah magic, and all of the citizens of Qualicity practice some form of Zombiah magic for their daily activities. Apiaceae and Anemone created a Zombiah school in the heart of Qualicity, and various branches of the school also formed throughout the rest of Qualicity.

Apiaceae and Anemone worked hard to ensure that their Zombiah magic was available to everyone in the city. Long nights of teaching, crafting new spells, and fielding questions began to take their toll on Anemone. She was starting to fall behind in her work with her sister Apiaceae, and eventually she had trouble making it through lectures before having to cancel class from exhaustion. Apiaceae tried to pick up Anemone's work but gradually the sisters realized they needed others to help them teach. They selected several of their brightest and most compassionate students to begin training on their behalf and help manage the school. Anemone continued to deteriorate until she was unable to even get up from bed. Her exhaustion caused other illnesses to take hold. Despite Apiaceae's best efforts to help her sister, Anemone died. Apiaceae was stricken with grief. She begged Anemone to hold on and keep going. Her mind raced. In that moment of panic, grief, and denial she pleaded with Anemone to wake up again. Rea surged in the room, and Anemone's eyes re-opened. Apiaceae was startled. She didn't understand how she managed to bring Anemone back to life. Anemone was still frail, but recalled Apiaceae's begging and pleading, and felt there was more to do. Her spirit agreed, despite the pleading of other spirits to join them. But something was different about Anemone's appearance. Before her death her eyes were soft and brown. Now they were a milky white. Anemone was still Anemone, and recalled past events in vivid detail. This was the first resurrection using Zombiah magic.

Other resurrections were performed using Zombiah magic. One of the critical components for resurrection is a spirit that is willing to return to inhabit the body. No unwilling spirit has ever successfully resurrected a body. When the spirit feels they would rather return to the spirit realm they may do so at any time. Many spirits complete what they needed to do and once again allow themselves to die. Some feel their work is never completed and remain in their resurrected state until the body is so exhausted that the spirit is unable to continue inhabiting it. Only a few cases of this exhaustion have been recorded. 

Eventually Apiaceae was also resurrected with the magic she discovered. She and Anemone documented their experiences as best they could (They admit that the spirit world was impossible to describe outside of broad sensations happening all at once). Both sisters continued their work until it was clear they would need to pass leadership of the school to others. By this time the city of Qualicity was named and automation flourished in its halls. Monarchs not only embraced Zombiah but were also practitioners themselves. More than once Apiaceae and Anemone found themselves as personal tutors to gifted (and not-so-gifted) royal students. Exhaustion was setting in again and Apiaceae and Anemone gathered their closest disciples together and gave them the leadership of Zombiah and the charge to continue their work long after they were gone. After they finished the sisters held each others hands and their spirits departed together. The room fell silent as Apiaceae and Anemone's bodies crumpled to the floor. Their work was completed and it was up to the disciples to continue their work. Soumbala was one of the disciples present, and has been the head of Zombiah magic ever since, carefully guiding Zombiah through the Great War to the present day.

## Zombiah and The Great War

Zombiah tried to stay out of The Great War for as long as possible. They had no quarrel with Magmah or Aquah, or any of the rest of the schools. Zombiah was closest with Magmah, having used their magic to strengthen their materials. During the war they closed off ties with Magmah and refused to aid any school, either with automation or resurrection. Unfortunately, both Magmah and Aquah viewed Zombiah's refusal to help them as secretly aiding the other side, so Zombiah was dragged into the war despite their refusal to engage with it. Zombiah re-ignited their friendship with Magmah by helping Magmah with automated soldiers and weapons in the hopes that Magmah would overpower Aquah and stop the war. This caused Zombiah to be directly attacked by Aquah, so Zombiah began building their own automated defense to deter Aquah's attacks. Several key figures in Zombiah's army were killed, so Zombiah began resurrecting their own people to continue fighting. This brought the wrath of Ah upon Zombiah, which began attacking Zombiah, both with physical attacks and spiritual attacks. Ah used spirits on the reanimated dead to coax the spirits back into the spirit realm. Zombiah did their best to ward off the attacks, but eventually Ah succeeded in their assault and occupied Qualicity, where they used this position to ultimately defeat Chaosah. Ah created a treaty where Zombiah agreed to cease resurrections unless they were used for specific purposes. One of those purposes was in dire emergencies, so when Chaosah was destroyed Zombiah was called upon to resurrect Chaosah and restore the magical balance of Hereva. This treaty is sealed away in a magic lock-box, where all parties must travel to renew the treaty. Other conditions of the treaty are that Zombiah magic may be used for demonstration purposes, such as magical contests. It may also still be taught to students, but in a diluted form. The Zombiah of today has refocused itself on its automation beginnings in order to rebuild from the war and the occupation.

## Zombiah today

Zombiah still permeates the city of Qualicity. Queen Coriander is one of its star pupils, being both adept at creating and repairing machinery, and also showing her skills in magical contests. Soumbala is Coriander's teacher and has guided her in the ways of Zombiah. Soumbala has been a huge influence on Coriander, so much so that Coriander is happier when she is alone building machines rather than performing her royal duties.

Zombiah's influence can be felt in the culture of Zombiah. Automatons perform many tasks in Qualicity. Qualicity also exports some of their automatons to other schools. Despite this, some of the other schools are still uneasy around automatons. Ah is notable for their rejection of such machinery. At Coriander's coronation there was a dispute about who would be serving guests. Ah threatened to refuse to participate if any automatons were serving guests. It was later decided that Magmah would bring their own staff to cater the event rather than rely on the automatons of Qualicity. Hippiah also finds the automatons strange, but do not have the same visceral response as Ah. Aquah and Magmah are more receptive of the automatons, having seen their use during the great war. Chaosah seems ambivalent about the whole question of automatons, showing neither appreciation or contempt for them. It should be noted that younger generations of these schools are more receptive to the automatons presence and do not tend to share the same fears and distaste of automation as their elders.

Resurrections are still performed and taught, but only during lessons and demonstrations. The treaty with Ah expressly forbids larger-scale resurrections. Should someone perform a resurrection without the permission of Ah there could be grave consequences (pun not intended). A representative from Ah must be present for non-demonstration Zombiah resurrections. This is to prevent Zombiah from overstepping their treaty obligations. It also lessens the possibility that a resurrection is successful, as the other spirits might be able to persuade the recently-departed spirit to forego the corporeal experience and return with them to the spirit realm. Having to wait for a representative from Ah for any resurrection can take days, and there is talk that Ah tends to not prioritize these requests.

## Creating a Zombiah Character

Zombiah witches value cleverness above all other approaches. Having a "Clever" approach of Good or higher would make you stand out in the Zombiah school. They also prefer folks that are "Careful" and "Quick" so having a Fair rating in those approaches would be beneficial. Coriander is not particularly sneaky so her "Sneaky" rating is Mediocre, but other Zombiah witches might be more sneaky than her. After all, the throne of Qualicity and the Technologist's Union is one vacancy away from being in contention again.

## Zombiah School Stunts

Each member of Zombiah may have the following school stunts:

* Because the School of Zombiah has intimate mechanical knowledge, I get +2 when I Quickly overcome obstacles relating to machinery.
* Because the School of Zombiah can infuse inanimate objects with spirits, once per session I can animate an otherwise inanimate object. (Be careful of the consequences of such actions! Reanimating the dead might bring unwanted attention from Ah!)


# Magic School: Hippiah
<!--% FIXME: Need more on what "Evolutions" Schools of magic are.-->

Hippiah is at the top-middle of the triangle, and it's the central sub-school of "Evolutions" magic. It's the house of **plants, creatures, insects** (yes, even pests like mosquitoes), and **all living things**. It also related to **medicine and healing**, providing both plant-based cures for ailments. Hippiah is one of the most highly regarded schools of Evolution magic, as it is the school most willing to share its knowledge. Hippiah documents farming, gardening and fertility in great depth, serving as a guide for most of the farmers of Hereva. Although Hippiah was always an open school it realized during the great crisis that it could not conscientiously keep its secrets and began spreading its knowledge throughout Hereva. While farmers know a bit of Hippiah magic the difference between a Hippiah adept and a mere practitioner is staggering. Few outside the Hippiah schools have seen the full extent of Hippiah magic, though there are stories of vegetables and grain that are so tasty as to make even the schools of Magmah weep with joy. Hippiah magic techniques are so popular they tend to seep into other school's curriculum, though those schools tend to ignore the source and claim it as their own.

## Founding of Hippiah

Hippiah magic was one of the earliest magical traditions to be discovered on Hereva. Its magic was perfected in the fields and passed down via word-of-mouth and tradition. It wasn't until much later that Hippiah was formalized as a school of magic. These traditions were compiled together into a single volume by Dandelion, a witch that wanted to ensure that none of the traditions she had learned would be forgotten. Over time others kept asking her for copies of her notes and book. Copying these notes became tedious, so Dandelion found other witches to help her create copies of the book and spread the knowledge so it wouldn't be lost. Dandelion believed that these rituals were so important to the growth of Hereva that she worked her entire life to ensure that anyone who asked for this knowledge would have it. Eventually her notes were transcribed in digital form and are one of the most copied and transferred resources of knowledge on the crystal ball network. The witches continued their study and practice and the Hippiah school was formed.

Other witches came to Hippiah in search of perfecting their knowledge. Spells that were harder to replicate were streamlined and tweaked until they were easier to perform. Other spells for rapid plant growth were created. Unfortunately these spells didn't get as wide circulation as the spells that Dandelion created because the witches who developed those spells felt a certain pride in their accomplishments and wanted to be compensated for their efforts. These spells were tucked away in the various tomes at the Hippiah School, where adepts will taught those spells under strict agreement that they never be shared with others. One anonymous author decided to break that agreement and published "Secret Spells of Hippiah Knowledge (Spells THEY Don't Want You to Know)", which was a hastily compiled grouping of spells. Some of them were correct, but many of the spells were misremembered by the author. One such spell, "Hyper Growth Plants" is rumored to cause plants to grow quickly, but in odd, misshapen ways that are inedible (not because they are poisonous, but because the plant gets really cross with you if you try to eat it). Much hype and superstition about this book arose, to the point where copies of the book are more treasured for being a rare and transgressive artifact than for any utility of the spells themselves.

Today's adepts of Hippiah still believe in the free sharing of knowledge, but maintain that deeper levels Hippiah knowledge require training in order to adequately perform. On occasion someone dabbling in Hippiah magic stumbles upon one these rituals, and the results of such uncontrolled growth have made the decision to keep this knowledge within the confines of Hippiah seem rather sound.

Many doctors in Hereva use some form of Hippiah magic in their practice. Certain plants, when combined, create potent cures for various ailments of Hereva, and doctors use these to treat their patients. Some of them mix their own plants together to create these medicines, but a thriving business formed in Hereva to pre-mix and package these medicines for use. This was in part because some of the original Hippiah recipes were scant on actual measurement details, using words like "pinch" and "dollop", and frequently referring to items that were in Dandelion's kitchen that have unknown measurements. These companies painstakingly figured out the right proportions and sell the results to doctors, who can spend their time treating patients rather than mixing the somewhat lengthy prescriptions.

## Hippiah and The Great War

Hippiah played more of a support role in the war. Near the end of the fighting each day the Hippiah Witches would scour the battlefields for wounded and assist as needed. They also helped to grow provisions for each side. Some might argue that Hippiah's involvement prolonged the war, but some also argue that the kindness of Hippiah also helped certain battles not even be fought. One such battle was "The Thirty Second Battle" between Magmah and Aquah. Hippiah witches on each side had helped the combatants grow some incredibly tasty plants. Magmah created an irresistible vegetarian feast, while Aquah made seaweed-based dishes that were delicious. As the time for battle approached each side was overtaken by the mouth-watering aromas from the other side. It's called "The Thirty Second Battle" because within thirty seconds each side was begging the other side to put aside their weapons and just eat. That battle signaled not only that peace was possible, but that there was more in common with both sides than they cared to admit. Had it not been for the other houses warring wit each other The Great War might have ended on that battlefield.

Hippiah witches were also present after the destruction of Chaosah. While attending other wounded they saw and heard an enormous explosion of Rea. Several of the Hippiah witches were dispatched to determine what happened. The devastation was unlike anything they had seen before. Worse, they noticed that their powers were starting to fade. Ah had declared victory over Chaosah and was content with their victory, but Hippiah witches begged Ah to restore whatever was left of Chaosah lest Hereva's magic forever be lost. It was Hippiah witches that escorted Zombiah witches to the battlefield to resurrect as many Chaosah witches as they could. Were it not for Hippiah's insistence that Chaosah be restored the magic in Hereva would be nonexistent.

## Hippiah today

Hippiah maintains a modest school building near the farmers of Squirrel's End. The number of students studying Hippiah Magic have dwindled over the years. Before The Great War there were folks on waiting lists to get into Hippiah and learn the secrets of making better crops, but with the rise of the popularity of the school of Magmah there are less folks considering Hippiah magic. Hippiah magic is not highly regarded by the rest of the schools, even though it is important for daily life in Hereva. Folks who are dismissive of Hippiah not being a flashy or defensive magic have not witnessed the power of Hippiah's rapidly growing plants, or been caught in one of the various traps that Hippiah witches set for various trespassers into their gardens. Basilic is the current leader of Hippiah and runs a more traditional school where students are expected to show their aptitude at an early age. Students who don't meet her expectations are subjected to her sharp tongue and withering stare.

Hippiah witches still patrol Hereva looking to be of service to the lost and the wounded, but their patrols are fewer and less frequent than they used to be. Their resources are stretched thin, but they still feel a duty to help those in need as much as they can. They regularly teach classes to farmers on how to best use Hippiah magic in their day-to-day farming. These classes serve dual purpose: to help teach those willing to learn, and to try to recruit folks into the Hippiah school. Hippiah tends to prefer recruiting younger students as they are more open to learning and haven't learned bad habits for casting Hippiah magic. 

## Creating a Hippiah Character

The Hippiah school prefers to get results and get results quickly. Having a "Quick" approach of Good would make an ideal Hippiah witch. However care must be taken so that the plants that are created can thrive. Having an Average or greater "Careful" approach would make excellent plants. Hippiah does not value flashiness, so having a Mediocre "Flashy" approach would make you the perfect Hippiah witch.

## Hippiah School Stunts

Each member of Hippiah may have the following school stunts:

* Because the School of Hippiah focuses on growing plants, I get +2 when I Quickly create advantages related to plant growth.
* Because the School of Hippiah understands medicine, I get +2 when I Carefully overcome obstacles when someone is injured or poisoned.
* Because the School of Hippiah understands the balance of nature, once per game session I can locate someone who is hiding among plants or in a forested area.

# Magic School: Aquah

Aquah sits at the middle-left of the triangle. It is the school of **water, oceans, wind, clouds and the abyss**. Very few Herevans know much about this school, in part because its chief requirement is learning how to breathe underwater before one can even begin to attend courses. This requirement is acceptable to Aquah, as they are content to commune amongst themselves and the denizens of the sea. They rarely interact with those on land, and those who fail to meet the standards for the Aquah school find themselves washed up on the shore (or rather, someone else usually finds them). As they tend to be out-of-sight, out-of-mind for most land folk it is easy to forget this school, but the wise know it is foolhardy to discount them.

There has always been a certain unease with the folks practicing Aquah and the other schools. Part of this is the power that Aquah holds over the day-to-day life of the folks of Hereva. Aquah can flood an entire town in an instant, or cause water tornadoes to appear at will. Aquah tends not to bother with such displays, because they prefer to be alone, but the rumors of Aquah's power have only grown in their absence. Those who remember The Great War know how devastating Aquah's power can be. Rapid erosion, drought, floods, mudslides, and the like are just a sample of what Aquah is capable of producing.

## Founding of Aquah

Aquah traces its origins to an unnamed mermaid (called "The First One" by those who practice today). Her curiosity for how her underwater world worked prompted her to experiment with various spells to see what would happen. These spells started with small spells to heat the water in order to determine how it flowed. These spells quickly grew into influencing the currents, which created waves and vortexes. Pleased with her progress, she continued her discoveries. Other mer-folk gathered around and learned her spells. They grew their skills until they were able to influence the weather on the rest of Hereva. They found they could make certain areas more hospitable to mer-folk, and the waters of Hereva grew with their discoveries. When they discovered their spells were causing folks on land to lose homes and get swept into the sea they apologized and the waters of Hereva receded slightly. Some on land were impressed by the powers of Aquah and asked if they could learn their secrets. They learned how to breathe underwater and eventually learned how to function as well as, and in some cases better than, the mer-folk that taught them. Aquah's influence began to spread as land-folk learned the joys of living under water.

## Aquah and The Great War

Aquah was one of the main participants of The Great War. It started because of an argument between Aquah and Aquah. Tensions were already high between both schools, with Aquah accusing Aquah of sending destructive weather patterns straight through Komona. Severe thunderstorms, ice storms, snow, and tornadoes wreaked havoc and caused damage and flooding throughout the city. Aquah denied these allegations, claiming that they didn't directly control the weather like that, and said that was part of the dangers of being in a floating city. Aquah countered that they noticed Magmah  hovering over lakes and draining them of water in order to keep up Magmah's water reserves. More than once an Aquah settlement near the edges of a lake found itself sitting in a patch of dry land with The City of Komona floating overhead. As part of the treaty (and as punishment for escalating the war) the Aquah school was renamed "Wharrgablah", by Magmah, referring to the sound that Komonans most closely associated with these damp folks. This was a name that Aquah strongly objected to, so it was agreed that Aquah could pick a name for Magmah that they also found disagreeable. During that time all records referring to Aquah were magically altered to reflect the change, including Aquah's own spell books, newspapers, and historical records. Even writing "Aquah" on a piece of paper was magically altered into "Wharrgablah", much to the delight of young children growing up during this time. So powerful was this spell that it even detected misspellings and would replace it in casual conversations. Once the 10 years of penance had expired the name "Aquah" once again appeared in the records. There are still folks who have trouble remembering what Aquah's name is, so hearing someone mention "Wharrgablah" is not uncommon, though it is considered necessary and polite to gently correct them when it occurs.

## Aquah today

After the war Aquah became more isolated and withdrawn from the rest of the schools. It wasn't until the second magic contest at Komona that most of the other schools have even seen someone from Aquah. Even the treaty ratification ceremonies held at Ah's temples were not attended by Aquah. Aquah opted instead to ratify in absentia, and sent their terse, yet polite, regrets for being unable to attend. Aquah's favorable performance in the second magic contest loosened some of the tensions between Aquah and the rest of the schools, but there is still unease. The coronation of Coriander was another time where members of Aquah were in attendance. It is possible that Aquah has relaxed their isolationist stance toward the other schools.

Members of Aquah itself do not feel they are being isolationist; they find little in common with the air-breathers. Ah finds the whole process of pushing air to form words slow and cumbersome, opting instead for using a form of telepathy and subtle signals to convey complex messages. Members of Ah are very good at reading faces and expressions, and can quickly determine if someone is being genuine or is a threat.

Aquah's cities are remarkable, combining coral, shells, and natural structures into their design. At times it can be difficult to determine if one is looking at an Aquah city or a natural formation. Careful and trained eyes can make the distinction.

Aquah society is mostly egalitarian, though there is a council that meets as needed to discuss the issues of the day. These meetings are open to other members of Aquah, though few tend to concern themselves with the day-to-day business.

Aquah students are taught via telepathy, though Spirulina seeks to change this. Telepathy has the advantage of giving quick thought transfer on a topic, but has caused Aquah to forget many spells and discussions that certain teachers did not find relevant. Aquah characters may take the Aspect "Learned Aquah magic via telepathy" to show that they learned Aquah magic quickly from the thoughts of their teachers, but might have missed something along the way. Spirulina and those she has convinced have the aspect "There's more to Aquah Magic than what we're taught telepathically" which signifies a deeper knowledge of Aquah magic and may expose them to secret Aquah lore.

The school of Aquah still trains students, though most students come from within Aquah society. The Great War limited the influx of new students from outside of Aquah society. Prior to the war there were a handful of students who "took the plunge" (if you'll pardon the pun), but those numbers have dwindled. A true witch of Aquah is content to stay within the world of the sea, but some have ventured to the land of the air-breathers. 

## Creating an Aquah Character

Aquah characters are very Forceful, so a "Good" Forceful approach will make your character succeed in the Aquah school. They also value Careful memorization of Aquah's traditions so your Careful approach should be "Fair". Aquah does not value those who are Flashy, feeling that is more of a "dry one" trait, so being "Mediocre" in that approach is preferred.

## Aquah School Stunts

Each member of Aquah may have the following school stunts:

* Because the School of Aquah can control the weather and climate, once per game session I can modify the weather in a location.
* Because the School of Aquah understands the flow of water, I get +2 when I Forcefully attack using water.

# Magic School: Ah

Ah is at the top-left of the triangle, and it's the last sub-school of "Evolutions" magic. It's the school of the **after-life, spirits, and souls**. It, like Chaosah, is also a magic system that can open the door to **parallel universes**. Because Ah deals with spirits and other planes of existence its secrets are carefully guarded. Many myths and legends about Ah have arisen because of this secrecy, and the Ah's reluctance to share does little to disprove such wild conjecture. Some say that the spirit world might someday unravel the fabric of Hereva itself. Some believe Ah guards the secrets to eternal life. Others folks claim the reason Ah is practiced in secret is because the spirits are selective with whom they communicate and are easily angered with banal questions about future events. Whatever the truth, the rumors about Ah are easier to find than actual written documentation, as Ah strictly prohibits documenting their practices. They prefer instead to pass their secrets through oral traditions to trusted adepts. Adherents of Ah choose their members carefully (some believe the spirits themselves select who can learn the ways of Ah). There are no accounts of outsiders learning Ah.

Practitioners of Ah travel in small nomadic bands, preferring solitude over any company. Perhaps this is because the spirits are always guiding, talking, and listening so the followers of Ah are never truly alone. Perhaps it is to always be ready lest a spirit pass on knowledge that isn't received.

There are many adepts of Ah, each wandering the lands of Hereva. It is considered good fortune by many in Hereva to see and be visited an adept of Ah. Rarely do they stay for long; their paths are guided by invisible winds in unknowable directions.

## Founding of Ah

Ah members believe the spirits themselves created the school of Ah. Sansevieria is considered the first practitioner of Ah. Her quest was one of spiritual purity. She tried many things to achieve this purity: arranging certain plants, sitting quietly and reflecting, and so forth, but she still felt restless in her pursuit. One day, as she sat, she had visions of spirits coming to talk to her. Rather than be afraid she welcomed them. They sat and had tea. Her mind showed pictures of the school that would eventually lay the groundwork for present-day Ah. She thanked the spirits, and when she got up she started forming the school. Subsequent sessions with the spirits gave her new insight into how the world of Hereva worked, and how the spirit world worked. She also realized that the spirits themselves had different personalities. Some were kind and helpful, while others were mischievous and playful. More than once she found herself following the instructions of a spirit, only to realize much too late that she was being set up for something humorous to happen. Eventually the spirits all agreed that her school was ready to accept other students. Sansevieria traveled to various places in Hereva to find students. This lead to the first group which included the current founder, Wasabi. Wasabi was also interested in spiritual purity, but her vision of the school was more of discipline and steadfastness. Sansevieria taught that the path th spiritual purity was one of iterative change, but Wasabi felt that shedding the body of any non-spiritual urge would achieve spiritual purity sooner. Wasabi's teaching resonated with the rest of the students. Students preferred to speak with Wasabi about the path to spiritual purity rather than Sansevieria. Eventually Sansevieria was pushed out of her own school. She quietly packed up her meager belongings and disappeared. Some believe she still walks Hereva in her quest for spiritual purity, but most believe she achieved her goal and became one with the spirits along her journey.

Ah is located under the setting moons of Hereva. It is one of the fixed points in Hereva's geography and never changes, unlike some places on Hereva. The temples of Ah are sparse structures with few decorations adorning the structures. Ah fashioned their temples atop natural hyperboloid-like structures with long winding stair-cases to reach the platform. Water falls from the edges of these magnificent structures and pools beneath them in teal-colored waters. 

The land of the setting moons is also the final resting place for many of the dragons of Hereva. They came to this place seeking comfort and solitude long before Ah built their temples. The dragons pay little heed to Ah's practitioners and Ah leaves the dragons in peace. Ah has a beautiful ceremony for the passing of these great creatures, but few outsiders have witnessed it. The ceremony (as explained) calls the spirit of the dragon forth and guides it up to the heavens. As a result the temples sit in juxtaposition between natural beauty and draconic remains. Ah temples and structures are adorned with the bones of dragons. This leads to beautiful structures with a somewhat foreboding appearance. Members of Ah also fashion ornamental jewelry from these dragon bones, and wear them in their hair. This practice is a reminder of the transience of life, and that they too shall one day join the dragons and other members of Ah as spirits.

## Ah and The Great War

Ah was the last school to get involved with The Great War. They felt that the quarrel was best sorted out among the other schools. What dragged them into the war was the violation of the principles. Zombiah's resurrections on a large scale were what first got Ah's attention (Ah firmly believes that the body is a prison and that anything to bring about reunification of the body and the spirit is anathema to them). The second was Chaosah's increasing involvement in bringing about disorder and chaos to the rest of the schools. Both of these angered Wasabi and lead her to declare war on all of the schools. Ah's vengeance was swift. No school was prepared for the assault from angry spirit warriors. Ah also convinced many of the Zombiah warriors to join them, which brought about Zombiah's defeat and eventual surrender. Magmah and Aquah also surrendered and signed treaties, both with each other and with Ah. Chaosah was the only school that did not relent, and was eventually destroyed in a cataclysm unlike anything seen in Hereva. Chaosah's destruction destroyed the balance of Rea in Hereva. The Great Tree of Komona split into two, and Komona started descending to the ground. Ah was pressured by Hippiah and the other schools into reviving all of the members of Chaosah they could. As part of their conditions for ending the war all of the schools have treaties with Ah that are ratified yearly and kept in the temples of Ah. It's these treaties that have given Ah leverage over the other schools.

## Communicating with Spirits

Ah is the only school that communicates directly with the spirit world. Ah students begin by using verbal and ritualistic means of communication, but later develop the ability to project their thoughts into the spirit world. The spirits respond in a myriad of ways. Some may project their responses into the minds of those who are open to receiving, but others prefer more physical communication (knocking over objects, moving a stylus, or other means). Spirits will not touch the bodies of the living, lest they become bonded with the spirit of the living. This bonding can be quite painful for both spirits. A spirit needing physical form will more readily inhabit a non-living entity rather than compete with a living entity. Spirits asked to possess a living entity will flatly refuse because of the pain both parties will endure.

If a spirit is forced to bond with a living entity the living entity will receive three stress. Those that are not taken out from the bonding have the duration of the scene with the spirit until the spirit will choose to leave. When the spirit leaves the body it takes an additional two stress. A spirit will automatically leave a body when it is taken out and will refuse to bond with any other bodies. 

Spirits are capricious beings with their own agendas. When a practitioner of Ah asks the spirits for truth the spirits may give cryptic answers or answers that are true from their perspective. They will not lie when asked for truth but are under no obligation to reveal everything they know from a simple question. Asking questions like "which of these doors leads to the exit" will reveal a truthful answer, but questions like "give us directions to the crystal fountain" may end with truthful, yet disappointing results. ("I'm a spirit, not a tour guide").

## Ah today

The Great War put Ah in a position of power over the other schools. They have signed treaties with each of the schools that have stripped each school of some of their powers. Zombiah agreed not to perform resurrections unless for dire need or for magical contests. Magmah and Aquah agreed to be peaceful to each other and to be more aware of the other's needs. Hippiah was the least affected by the treaty, agreeing only to not get into conflict with the other schools. Chaosah, however, was the school that was hardest hit by agreeing to Ah's terms for peace. Those terms are discussed in the section on Chaosah. Ah continues to promote their version of order through the peace deals they have brokered with each school. Wasabi even instituted The Unity Tree as a symbol of unity. It represented the impossible triangle, but managed to put Ah at the top of the pyramid-like structure. Ah considers its spiritual path is the right way forward in Hereva, and does everything it can to ensure compliance wit its goals.

## Creating an Ah Character

Ah Characters tend to be very careful, in part because of their strict training under the watchful eye of Wasabi and the spirits. Ah characters would do well to have a "Good" rating in Careful. Ah is never Flashy, so having a "Mediocre" rating in Flashy would round out your Ah character. 

## Ah School Stunts

Each member of Ah may have the following school stunts:

* Because the School of Ah trains to be like the spirits, once per session I can obtain guidance from the spirits about a location or situation.
* Because the School of Ah travels all over Hereva, I get +2 when I Carefully overcome obstacles by using my knowledge of the locations of Hereva.

# Sample Characters
This section contains pre-generated characters from the Pepper&Carrot comic. These are suggestions. Feel free to modify these characters as you see fit. 

Note that these characters in this section reflect the characters around the period of Queen Coriander's coronation. (Episode 28: The Festivities). Future supplements may expand on the characters from later episodes.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.18]{images/2015-09-15_witches-of-chaosah-family_by-David-Revoy.jpg} 
\end{figure}

\newpage{}

## Pepper

Pepper is a 16 year old orphan girl. She lives in the forests outside of the small farming village of Squirrel's End. She lives with her constant companion and familiar Carrot (so named for both his orange fur and for being found in a bushel of carrots). Her other companions are the remaining members of the magic school of Chaosah, whom she refers to as her godmothers. They tutor in the ways of Chaosah magic (the most primal form of magic in Hereva) guiding her on her path to becoming a "True Witch of Chaosah". This unfortunately has double-meaning for Pepper as she is not totally sure what a true witch of Chaosah is, and what she will become. She also doesn't trust her godmothers to be forthright with her on what a witch of Chaosah stands for.

Pepper was raised at the Little Acorns Orphanage in the village of Squirrel's End. At an early age, she showed an aptitude for magic and was enrolled in the Hippiah school of magic. While she excelled at magic, she also excelled at driving her instructors batty with various traps, potions, and other hijinks. When her instructors insisted she make a patch of flowers bloom she found the whole exercise pointless and boring. For Pepper, it was literally like watching grass grow.

In one instance, Pepper was so frustrated with her classmates and teacher making fun of her inability to grow flowers that she let off a huge blast of Rea in frustration. This drew the attention of the remaining Chaosah witches who were in search of their new heir. Pepper was reluctant to go with the Chaosah Witches, but she realized the Hippiah school was an imperfect fit. Her Hippiah instructors felt they had little choice but to let Pepper go, as Pepper was not performing well as a Hippiah witch. They also did not want the Chaosah Witches around, since the Chaosah witches had a reputation of bringing the ire of Ah with them. Since Pepper had little money and no living relatives it only took a little prodding for the villagers to help Pepper move into the Chaosah house in the forests of Squirrel's End. The alternative, they feared, would be for her to live in the village proper.

Being a witch of Chaosah is not particularly lucrative, and several times Pepper has not had enough Ko to afford even modest ingredients for her potions. It wasn't until the recent potion challenge that Pepper has had any kind of money. Much of that Ko went into augmenting her broom with additional potions and Rea for flight (Komonan wood brooms require much less Rea to fly, and fetch a hefty price). Even with her considerable skill, there have been a few times where she's had to make in-flight adjustments.

\newpage{}

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/2019-10-30_sketch-for-book1_by-David-Revoy.jpg} 
\end{wrapfigure}
**PEPPER**

High Concept: "Powerful Chaosah Witch in Training"

Trouble: Never enough Ko

School: Chaosah

Other Aspects: "Not sure what a True Witch of Chaosah is"; "There has to be a shortcut"

APPROACHES

Careful: Mediocre (+0)

Clever: Fair (+2)

Flashy: Average (+1)
 
Forceful: Fair (+2)

Quick: Good (+3)

Sneaky: Average (+1)

STUNTS

% FIXME: Need to square away the number of stunts that Pepper has

(Note that Pepper gets one additional stunt compared to the other Chaosah Witches. Because she is not a **True Witch of Chaosah** she is unable to intimidate others who fear Chaosah witches.)

School of Chaosah: Because the School of Chaosah **controls the chaos**, I get +2 when I **Cleverly Overcome Obstacles** when I can start a chain reaction of events.

Traps: Because I **set traps near my house** I get +2 when I **Cleverly Create Advantages** when **dealing with intruders in the forest of Squirrel's end**.

Potion Creator: Because I **create too many potions** once per game session I can **find the right potion at the right time** at my house at Squirrel's End.

Former Hippiah Witch: Because I am a **Former Hippiah Witch** once per game session I can **cast a Hippiah spell at Mediocre (+0)** rather than at Poor (-1)

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 2

\newpage{}

## Thyme

Thyme is the oldest of the remaining witches of Chaosah. She leads the Chaosah Witches with fear and domination. She is the apprentice of Chicory, the founder of the school of Chaosah. Thyme is wise and measured with her thinking but is also quick to anger when pressed. Her current apprentice is Cumin, a stout and kindly witch. Secretly Thyme wishes that her apprentice was Cayenne. 

\newpage{}
\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/2016-03-15_preparatory-Thym-sketch-for-episode15_by-David-Revoy.jpg} 
\end{wrapfigure}

**THYME**

High Concept: "Head witch of Chaosah"

Trouble: Aching joints and back

School: Chaosah

Other Aspects: "Chaosah shall retake it's rightful place as the most powerful school in Hereva"; Taught by Chicory herself

APPROACHES

Careful: Fair (+2)

Clever: Good (+3)

Flashy: Mediocre (+0)

Forceful: Average (+1)

Quick: Poor (-1)

Sneaky: Great (+4)

STUNTS

Elderly Chaosah Witch: Because I am **An elderly witch of Chaosah** once per game session I can remember something about the history of Chaosah or Hereva.

Locate buried failures: Because I am an **older Chaosah Witch** once per session I can **find a buried item** that a Chaosah Witch previously buried.

School of Chaosah (Fear): Because the School of Chaosah is **feared throughout Hereva**, once per game session I can **intimidate someone who is afraid of Chaosah Witches**.

School of Chaosah (Chain Reactions): Because the School of Chaosah **controls the chaos**, I get +2 when I Cleverly overcome obstacles when I can start a chain reaction of events.

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 3

\newpage{}

## Cayenne

Cayenne is the most visible and foreboding of the Chaosah witches. She commands respect and does not take orders from anyone other than Thyme. During the Great War, she was the most feared Chaosah witch, casting spells and causing devastation wherever she roamed. Little is known of her past, which suits Cayenne just fine. Her life is dedicated to Chaosah and to protecting the head of Chaosah, which is Thyme. Most times she can be seen chewing on a piece of grass, which she uses to focus herself on the present. She specializes in teaching spells and can often be seen teaching Pepper how to cast spells. She does not suffer Pepper's antics and potion-making lightly, often chastising Pepper about what a "true witch of Chaosah" should be doing. She has perfected the intimidating stare of Chaosah witches and can often be seen using her powers to further Chaosah's goals.

\newpage{}
\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/cayenne_portrait.png} 
\end{wrapfigure}

**CAYENNE**

High Concept: "Enforcer of Chaosah"

Trouble: Quick Temper

School: Chaosah

Other Aspects: Studied all of the books of Chaosah magic;"My past is not who I am now"


APPROACHES

Careful: Fair (+2)

Clever: Average (+1)

Flashy: Mediocre (+0)

Forceful: Great (+4)

Quick: Average (+1)

Sneaky: Good (+3)

STUNTS

Skilled Chaosah Teacher: Because I am a **skilled spell teacher of Chaosah** I get +2 when I **Carefully Overcome Obstacles** when a Chaosah Witch has trouble with a Chaosah spell.

Locate Buried Failures: Because I am an **older Chaosah Witch** once per session I can **find a buried item** that a Chaosah Witch previously buried.

School of Chaosah (Fear): Because the School of Chaosah is **feared throughout Hereva**, once per game session I can **intimidate someone who is afraid of Chaosah Witches**.

School of Chaosah (Chain Reactions): Because the School of Chaosah **controls the chaos**, I get +2 when I Cleverly overcome obstacles when I can start a chain reaction of events.

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 3

\newpage{}

## Cumin

Cumin is a kindly witch and Pepper's favorite of the three Chaosah witches. She specializes in creating potions as well as studying the various plants of Hereva to use in her potions. Her potions are potent and powerful, and she is happy to share her knowledge. Perhaps the reason Pepper creates potions over other means of incantation relates to how approachable Cumin is, compared to the other witches of Chaosah.

Cumin started off her Chaosah training as an apprentice of Thyme. Despite being in her sixties and an accomplished witch, she still suffers under the apprentice relationship; being bossed around almost instinctively by the older witch. Cumin is only somewhat aware of her strained relationship with Thyme as her apprentice. She became suspicious after an errand to Aquah where she nearly drowned in the attempt (Cumin complained that Thyme hadn't informed her that Aquah was so far under water, which Thyme said should have been obvious to even the dimmest of witches).

Cumin is also an excellent cook and makes tasty dishes rivaling the dishes of Magmah witches. She is always eager to taste any dishes and explores flavor with wild abandon. She can usually be found near the food of any gathering, sampling everything there and conjuring up new ideas for her own dishes.

\newpage{}

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.8\linewidth]{images/cumin_portrait.png} 
\end{wrapfigure}

**CUMIN**

High Concept: "Potions Teacher of Chaosah"

Trouble: "Thyme would rather have Cayenne as a protégé than me"

School: Chaosah

Other Aspects: Deep knowledge of Hereva's plants; Haven't met food I didn't like.


APPROACHES

Careful: Great (+4)

Clever: Average (+1)

Flashy: +1 Average (+1)

Forceful: Mediocre (+0)

Quick: Fair (+2)

Sneaky: Good (+3)

STUNTS

Potions Teacher: Because I am a **potions teacher of Chaosah** I get +2 when I **Quickly Create Advantages** when creating potions.

Sense of Smell: Because I have an **intense sense of smell**, once per game session I can **locate ingredients** for potions or food.

School of Chaosah (Fear): Because the School of Chaosah is **feared throughout Hereva**, once per game session I can **intimidate someone who is afraid of Chaosah Witches**.

School of Chaosah (Chain Reactions): Because the School of Chaosah **controls the chaos**, I get +2 when I Cleverly overcome obstacles when I can start a chain reaction of events.

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 3

\newpage{}

## Saffron

Saffron is a young girl from the floating city of Komona. She showed aptitude for magic at an early age, and after showing how easy it was to use magic to counterfeit Ko she was sent to the Magmah school to put her skills to good use. She runs a magic service "Saffron's Witchcraft" in the highest-rent districts of Komona. Her primary clientèle ranges from merchants looking to improve their wares and the alchemists of Zombiah researching materials. Saffron's business thrives; so much so that a steady queue of people form outside of her office looking for her advice. Even the Komonan mint looked to her to see how best to improve the Ko coins so they were harder to counterfeit. She has risen to prominence as one of the youngest entrepreneurs of Komona. The merchants of Komona are more than happy for Saffron's custom, as she tends to make large purchases of ingredients for her various potions and spells.

\newpage{}

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/2018-02-06_saffron-fire_by-David-Revoy.jpg} 
\end{wrapfigure}

**SAFFRON**

High Concept: "Young Entrepreneurial witch of Magmah"

Trouble: "I should be running the school of Magmah"

School: Magmah

Other Aspects: "Anyone who is anyone knows who I am"; "Never satisfied with what I have"


APPROACHES

Careful: Mediocre (+0)

Clever: Average (+1)

Flashy: Good (+3)

Forceful: Fair (+2)

Quick: Average (+1)

Sneaky: Fair (+2)

STUNTS

Popular magic consultant: Because I **own a popular magic consulting business** once per game session I can **divulge a hidden truth** about one of my clients.

"I'm on the list": Because I am **famous throughout Hereva** once per game session I can **gain access** to places that would otherwise be off-limits to less-famous people.

School of Magmah (Connected): Because the School of Magmah is **Well Connected throughout Hereva**, once per game session I can **find a helpful ally in just the right place**.

School of Magmah (Fire): Because the School of Magmah can **control fire**, I get +2 when I **Flashily create advantages** related to fire and fireworks.

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 3

\newpage{}

## Coriander

Coriander is a young girl from the engineering city of Qualicity. She is a witch of Zombiah, a magic school known for its ability to bind a spirit to non-living matter. This is used both in reanimating previously living beings as well as animating mechanical beings. Being an only child she quickly got bored and created herself a "sister" called Mensi. Her father and mother were the king and queen of Qualicity, but sadly they came to an unfortunate end when she was a child. Coriander grew up with the knowledge that she would eventually be queen, but her interests lie more in making things. She can usually be found working on her pet projects; crafting new and ingenious inventions and pushing the boundaries of technology.

\newpage{}
\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/2019-09-17_coriander-the-queen_by-David-Revoy.jpg} 
\end{wrapfigure}

CORIANDER

High Concept: "Reluctant queen of Zombiah"

Trouble: "Rulers of Zombiah can't be too cautious"

School: Zombiah

Other Aspects: "I've been tinkering on something like this"; "Still working out the bugs"

APPROACHES

Careful: Fair (+2)

Clever: Good (+3)

Flashy: Average (+1)

Forceful: Average (+1)

Quick: Fair (+2)

Sneaky: Mediocre (+0)

STUNTS

Queen of Technologists Union: Because I am **Queen of the Technologists Union** once per game session I can **command one of my Technologist Union subjects** to help me.

"Back in my lab": Because I **create things in my lab** once per game session I can **produce an item** that I previously created in my lab.

School of Zombiah (Mechanics): Because the School of Zombiah has intimate mechanical knowledge, I get +2 when I Quickly overcome obstacles relating to machinery.

School of Zombiah (Spirits): Because the School of Zombiah can infuse inanimate objects with spirits, once per session I can animate an otherwise inanimate object. (Be careful of the consequences of such actions! Reanimating the dead might bring unwanted attention from Ah!)

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 3

\newpage{}

## Shichimi

Shichimi is a young girl from the school of Ah. She spends most of her days wandering the lands of Hereva. At night she camps under the three setting moons of Ah. She is accompanied by her companion, the two-tailed fox Yuzu. She has a tendency to take things literally which can get her into trouble. She is high up in the ranks of Ah, but the ascetic demands of Wasabi, the leader of Ah, tests Shichimi's patience and resolve. Shichimi wanders more of Hereva than any of the other adepts of Ah, exploring and learning from the various cultures and traditions of the various magical schools of Ah. She is the first student of Ah to participate in Komona's magical contests, and has learned much from them. Her potion of rapid growth was learned from Hippiah's school,  and her "flashy" spell at the most recent magic contest was learned while consulting with Saffron from the Magmah school. Her penchant for wandering into other schools has brought her to the attention of Wasabi, who keeps a close watch over Shichimi and what she is learning.

\newpage{}
\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/2017-10-15_inktober2017_mysterious_by-David-Revoy.jpg} 
\end{wrapfigure}

SHICHIMI

High Concept: "Wandering adept of Ah"

Trouble: Tempted by comfort

School: Ah

Other Aspects: "My camping gear has everything I need"; "Takes things too literally"

APPROACHES

Careful: Good (+3)

Clever: Fair (+2)

Flashy: Mediocre (+0)

Forceful: Average (+1)

Quick: Fair (+2)

Sneaky: Average (+1)

STUNTS

% FIXME: Add one more stunt

Wandering student: Because I am the **only student of Ah to participate in multiple magic contests**, once per game session I can **create a minor spell or potion** that is related to one of the other schools at Mediocre (+0).

Well-prepared: Because I am **well-prepared**, once per game session I can **produce a small item** from my gear.

School of Ah (Spirits): Because the School of Ah **trains to be like the spirits**, once per session I can **obtain guidance from the spirits** about a location or situation.

School of Ah (Travelers): Because the School of Ah **travels all over Hereva**, I get +2 when I **Carefully overcome obstacles** by using my knowledge of the locations of Hereva.

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 3

\newpage{}

## Wasabi

Wasabi is the leader of the school of Ah. She values order over all other things and feels that only by order can Hereva be brought closer to the world of the spirits. She is exceptional with communicating with the spirit world, but disorder and distractions keep her from communicating with them as much as she would like. She uses her wandering students to gather information from other schools and keeps a constant flow of knowledge about each of the schools. She wears white robes to signify her level of connection with the spirit world.

Wasabi is a very powerful character. We recommend keeping Wasabi as a non-player character (NPC). That said, if your GM agrees then she can make a formidable addition to any party.

\newpage{}
\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/wasabi_portrait.png} 
\end{wrapfigure}

WASABI

High Concept: "Leader of the school of Ah"

Trouble: Too many distractions

School: Ah

Other Aspects: "There are no secrets from me"; "Close to the spirit world"

APPROACHES

Careful: Great (+4)

Clever: Good (+3)

Flashy: Poor (-1)

Forceful: Average (+1)

Quick: Fair (+2)

Sneaky: Fair (+2)

STUNTS

"Information is Power": Because I have **Collected information about all of the schools of Hereva** once per game session I can **reveal a fact about one of the other schools**.

"Come to my aid": Because I **lead Ah with an iron fist** once per game session I can **summon one of my followers** to **temporarily halt what they are doing and help me**.

School of Ah (Spirits): Because the School of Ah **trains to be like the spirits**, once per session I can **obtain guidance from the spirits** about a location or situation.

School of Ah (Travelers): Because the School of Ah **travels all over Hereva**, I get +2 when I **Carefully overcome obstacles** by using my knowledge of the locations of Hereva.

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 3

\newpage{}

## Camomile

Camomile is a human / raccoon witch of Hippiah. She is one of the most powerful witches in Hippiah, partly because of her driven nature and partly because of her appearance. Many witches of Hippiah regard raccoons as pests that demolish gardens, so Camomile's acceptance into the school caused some Hippiah witches to be concerned. Camomile always felt like she had to work extra hard to show her teachers that she was worthy of being called a Hippiah witch. She studied long hours into the night and did double the work of her classmates in the fear that she would be expelled for not measuring up to the real and perceived higher standards of her teachers. This made her one of the top students of the class, which also brought attention from the other students, who teased her. This gave Camomile focus to be the best student she possibly could be, but also left her feeling like an impostor in her own school. Eventually a kind teacher named Millet took Camomile aside. Millet was a human / opossum witch, and she relayed her experiences to Camomile. Millet said she also felt she had to work harder to get the acceptance of her peers, but that it wasn't because she was inferior. She explained that people have a hard time with folks and experiences that are different from them. This all stems from fear of the unknown. Fear is a powerful motivator, and can cause others to try to find comfort in that fear, whether it is by belittling someone or worse. The problem, Millet said, is not with Camomile, but with the others. The only person she needed to impress was herself. Let the others figure it out for themselves. Camomile took this advice to heart and ignored the taunts and teasing of the other students. Many of the students eventually matured and realized that Camomile was a confident and capable witch. She continues to grow as a confident Hippiah witch, but her main drive is competition with herself, not the adoration of the other Hippiah students. When it was time to select a student to represent Hippiah at the Magic Contest she was overwhelmingly chosen by the other Hippiah students to represent them.

\newpage{}
\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/2017-10-26_inktober2017_squeak_by-David-Revoy.jpg} 
\end{wrapfigure}

CAMOMILE

High Concept: "Protector of Hippiah Traditions"

Trouble: Sensitive about being teased for being part raccoon

School: Hippiah

Other Aspects: "I wonder what's in there"; Rather be around plants than people

APPROACHES

Careful: Average (+1)

Clever: Fair (+2)

Flashy: Mediocre (+0)

Forceful: Average (+1)

Quick: Good (+3)

Sneaky: Fair (+2)

STUNTS

Part Raccoon: Because I am **part raccoon** once per game session I can **sense danger** that others might not recognize.

School of Hippiah (Plants): Because the School of Hippiah **focuses on growing plants**, I get +2 when I **Quickly create advantages** related to plant growth.

School of Hippiah (Medicine): Because the School of Hippiah **understands medicine**, I get +2 when I **Carefully overcome obstacles** when someone is injured or poisoned.

School of Hippiah (Nature): Because the School of Hippiah **understands the balance of nature**, once per game session I can **locate someone who is hiding** among plants or in a forested area.

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 3

\newpage{}

## Quassia

Quassia is a teacher at the Hippiah school. She teaches the classes related to rapid growth of plants, along with other classes related to plant heartiness, shape, and vitality. She is part-elf and spent many years running through forests and being around nature. She joined Hippiah as a young witch and gained the respect of her peers. Her knowledge and ability to convey that knowledge to other students made her an invaluable asset in the classroom. She was given the title of Hippiah Teacher by Basilic (the current head of Hippiah magic) and she quickly became one of the most popular teachers of Hippiah Magic. She routinely brings her students to the Hippiah school courtyard to have them demonstrate their skills on seeds. Those who can grow their plants get her approval, and those who can't draw her ire and sharp tongue. Despite her popularity she feels she understands the plants better than the students, but continues to be patient with them to a point. She aspires to one day lead Hippiah Magic but for now is content to teach as much as she can.

\newpage{}
\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/chara_quassia.jpg} 
\end{wrapfigure}

QUASSIA

High Concept: "Teacher of Hippiah Traditions"

Trouble: Does not suffer failure well

School: Hippiah

Other Aspects: One with the forest;Understands plant emotions better than humans

APPROACHES

Careful Fair (+2)

Clever Good (+3)

Flashy: Mediocre (+0)

Forceful: Average (+1)

Quick Average (+1)

Sneaky Fair (+2)

STUNTS

Hippiah Teacher: Because I am a **Hippiah Teacher** once per game session I can **animate a plant** to do my bidding for that session.

School of Hippiah (Plants): Because the School of Hippiah **focuses on growing plants**, I get +2 when I **Quickly create advantages** related to plant growth.

School of Hippiah (Medicine): Because the School of Hippiah **understands medicine**, I get +2 when I **Carefully overcome obstacles** when someone is injured or poisoned.

School of Hippiah (Nature): Because the School of Hippiah **understands the balance of nature**, once per game session I can **locate someone who is hiding** among plants or in a forested area.

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 3


\newpage{}

## Spirulina

Spirulina is one of the students of Aquah's school of magic. She's the first person from Aquah to engage with the "dry ones" ever since The Great War. She was raised by her family to be curious and question traditions, so when word of a second magical contest leaked to the students of Aquah she volunteered to represent Aquah. She is fiercely loyal to Aquah, but regards the isolationist practices of Aquah as cowardice. "Shall we retreat into oblivion, or stand tall next to the dry ones?"

Her curiosity lead her not only to learn all of the oral traditions of Aquah but to question their validity. She is constantly revising spells, making them more potent or less costly in Rea. She exhausted her instructors by bombarding them with questions until they set up a separate study path for her to learn. She is also the first student to commit these traditions into a more permanent format, which shocked and dismayed her teachers. Aquah transmits their knowledge telepathically to their students, relying on memorization and rote learning for generations of teachers. Spirulina found this tedious, and wondered if there was a better way to convey this information. She worked out how a spell might be written down for others to read and use. She tested it out on several of the other students. At first the students were disgusted by the crude line drawings, but eventually they realized they could retain the information faster than by telepathy alone. Later Spirulina learned that she was not the first to discover this. One of the teachers in the long line of Aquah teachers felt that telepathy was the purest means of transmission, and abhorred the tradition of "writing" that the dry ones used. After questioning her instructors further she discovered an unused closet containing hundreds of magical artifacts that hadn't been taught over those many years. The teachers were stunned. Here were spells and techniques that had faded from memory. Here were corrections for misremembered spells, conversations about the efficacy of one spell technique over another, and fragments of ideas that were never explored further. Here were famous names from Aquah's past bickering over correct technique acting as living beings, and not as infallible and irrefutable icons. Spriulina's persistence reinvigorated Aquah's magic from dogmatic belief in what one's instructors taught them to one of curiosity. Naturally some instructors did not agree with this change, and tension inside the school is mounting between those who believe their ways of teaching by rote and memorization are correct and those who wish to explore the old magical artifacts.

She often wonders if being in Aquah schools has kept her from learning all she can about the magic in Hereva, and wants to learn more. She is proud of her Aquah heritage, but is an outspoken proponent of bringing Aquah back from its isolationist past. 

\newpage{}
\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/2017-02-10_Spirulina-sketch_by-David-Revoy.jpg} 
\end{wrapfigure}

SPIRULINA

High Concept: "Fiercely curious student of Aquah"

Trouble: "If it is carved in stone then we must erode and break the stone"

School: Aquah

Other Aspects: "There's more to Aquah magic than what we're taught telepathically"; "Fish on dry land"


APPROACHES

Careful: Fair (+2)

Clever: Fair (+2)

Flashy: Mediocre (+0)

Forceful: Good (+3)

Quick: Average (+1)

Sneaky: Average (+1)

STUNTS

Telepathic: Because I am a **strong witch of Aquah**, once per game session I can **telepathically send a short message** to anyone who is not part of the School of Aquah.

Well-read: Because I have **read the ancient manuscripts of Ah**, once per game session I can **cast a spell** that is related to one of the other schools at Average (+1).

School of Aquah (Weather and climate): Because the School of Aquah can **control the weather and climate**, once per game session I can **modify the weather** in a location.

School of Aquah (Water): Because the School of Aquah **understands the flow of water**, I get +2 when I **Forcefully attack** using water.

STRESS \SC{}

CONSEQUENCES

Mild (2):

Moderate (4):

Severe (6):

REFRESH: 3

# Using Magic

<!--FIXME: Elaborate-->
*This section describes how to cast magic in Hereva*

## Using Rea (Casting Spells and Creating Potions)

Casting a spell or creating a potion in Hereva uses Rea. Rea ("Reality") is part of every day life in Hereva. All creatures in Hereva are capable of casting spells using Rea, but only those who study it can wield it effectively. Magic in Hereva is unlike our concepts of Qi and Mana. Rea is best thought of as collecting the by-products of a task. By concentrating on making a potion for a loved one the practitioner can harness some of the Rea used in creating the potion. Conversely if there is no attachment, or if the practitioner is careless then the Rea is lost and the Rea must be obtained via other means. 

Rea can be harnessed by the following methods:

* The process of summoning
* The time or duration while making something
* Attention, Dedication, or Care
* Emotional engagement

Rea is used for all aspects of life on Hereva. Farmers in the village of Squirrel's End use Rea to grow healthier crops (and tastier crops, as the locals will attest). They also use Rea to ward off the denizens of the forest (which also attest to the tastiness of the crops in Squirrel's End). Most Witches of Hereva don't use their Rea for mere chores, preferring instead to save their Rea use for more important matters. This is why you'll find witches doing things like sewing on a button or purchasing starfruit from market rather than conjuring up a new outfit or summoning starfruit: the cost in replenishing the Rea outweighs the benefit.

Most practitioners don't actively monitor their Rea usage. They can sense when they are nearing depletion and can seek out ways to regenerate or obtain Rea as needed. How much Rea a person can maintain / channel varies greatly. Some folks are able to charge up luminous levels of Rea. A few witches are skilled in channeling and manipulating large amounts Rea without the need to build up reserves. Such channeling can be dangerous, though; while it is difficult to get too much Rea it is possible certain spells can consume all of the Rea from the caster and the surrounding area. Such spells can lead to disastrous results.

Training in one of the schools of magic allows for more controlled and focused use of Rea. Without this training using Rea becomes a guided intention rather than a direct command. With careful study, focus, and desire one can learn to harness Rea to conform to their will. As the Herevans learned how to control Rea they quickly realized this power could cause havoc if not properly taught, and formed the schools of magic to properly teach the various ways of harnessing Rea. They perfected their craft via trial and error, and compiled that information into spell books and potion recipes. Some schools became more secretive about their methods for controlling Rea, while others remained open to all students (mostly from necessity, but some because of the charters of their founding members). Most of the people of Hereva know a little bit of the schools of magic they were exposed to (for instance the farmers of Hereva know a diluted form of Hippiah magic passed on from one generation to the next) but there is a huge difference between mimicking the magic of a school and knowing all of their secret knowledge.

Magmah, Aquah, and Hippiah are especially tuned to channel Rea into the physical world. Ah and Zombiah focus their Rea outside of the physical world. Chaosah is attuned to both the physical and non-material world.

Rea must be replenished from time to time. While Rea can be replenished in various ways (meditation, careful focus on a project, dedication to a project, etc.) these take time and effort to achieve. Certain potions and rare ingredients can be purchased to replenish Rea, but these can be quite costly and are used by a limited number of practitioners. Beware anyone who claims to have invented a way to regenerate Rea without effort; such perpetual Rea machines are little more than "get-Ko-quick" schemes.

Only students and members of the magical school can effectively cast spells from that school. Those who are untrained in a particular school can still cast Poor (-1) spells regardless of the approach they take. Shichimi is an exception to this, having learned a little bit of each magical tradition in her journeys. She is able to cast Mediocre (+0) spells once per session (see Shichimi's stunts for more details). Pepper is also an exception, having learned some Hippiah Magic. She can cast Hippiah Spells at Mediocre (+0).

## Casting spells

Casting a spell is the same as as any other action in Fate. You can use Create Advantage, Overcome, Attack, or Defend with the approach that matches what you're attempting to do. You may also use aspects or stunts to modify your action. See TAKING ACTION for more details.

Spells are cast against the difficulty of the spell and any opposition that may interfere with the casting of the spell. If there is no opposition you either automatically succeed or test against the difficulty of casting the spell. If there is opposition you cast the spell against the difficulty of that opposition. (e.g.: If you are a Magmah witch creating a torch in a dark room this is trivial for you, and no roll is required). If, however, you are in a hurricane trying to make a torch via a spell you'll roll against Great difficulty. If you're a Chaosah witch creating a small Chaosah black hole to remove potions with no opposition then you create it and no roll is required. If, however, you're creating a Chaosah Black Hole in order to prevent a giant asteroid from hitting Hereva while riding the back of a Dragon Cow then you'll roll against a difficulty of Fantastic or greater. Hope you studied!

Casting a spell using a school you haven't studied defaults to Poor (-1). Some witches may have experience with multiple schools (Pepper with Hippiah Magic, Shichimi with multiple schools). Witches that have some experience with another school may cast spells from that school at Mediocre (+0).

### Example: Casting a Spell

Saffron is in a dark room. She wants to create some light in the room, so she creates a floating ball of fire next to her. This is a trivial spell for her so her player doesn't need to roll to create the spell. Saffron creates the floating ball of fire next to her. The aspect "floating ball of fire providing light" exists for the remainder of the scene. 

Camomile is running away from the guardian of an abandoned castle. She darts into the woods, and casts a spell to create some vines to trip the guardian. She chooses to Create an Advantage "thick vines on the ground" and rolls using her Quick approach at Good (+2) level (she's in a hurry). The GM decides this test is at Great (+3) difficulty, and Camomile rolls a +1 with the dice. She succeeds, so the aspect "thick vines on the ground" exists for the remainder of the scene. Camomile hopes the guardian won't be able to cut through the vines!

## Magical Focus for each school

### Chaosah

* Relates to Physics and our concept of General Relativity.
* Works with dimensions, space, and time.
* Can disintegrate objects and reconstruct them with enough witches.
* Focuses on small changes having large results.

### Magmah
* Relates to metallurgy and materials science, fire, heat, lifestyle, and cooking
* Works with poshness, metal purification, food preparation
* Focuses more on flashy spells and spells of comfort (food, poshness, riches)

### Hippiah
* Relates to Plants and Plant Growth
* Works with healing and nourishment
* Focuses on helping folks, even if reluctantly

### Aquah
* Relates to water, storms, and weather
* Works with motion of the tides, weather systems, and aquatic life
* Focuses on powerful spells that aren't flashy

### Zombiah
* Relates to reanimation, crafts, and creativity
* Works with building mechanical structures and complex machinery
* Focuses on animation and reanimation of stagnant items

### Ah
* Relates to spirits and the spirit world
* Works with listening to spirits and using spirit-based magic for information gathering and direction.
* Focuses on knowledge and order, but can be used offensively in certain cases. 
* (Depending on the campaign spirits may either be able or unable to interact with the physical world)

## Example spells for each school

### Chaosah

* Chaosah Black Hole: Creates a black hole, which opens a portal to another dimension
* Disintegrate / Reintegration spells
* Micro-dimension
* Small changes having large impact

### Magmah

* Fireball (as a weapon or as a torch for light)
* Transformation (potion of poshness, transform one material into another material, tempering metal)
* Cooking / Purifying to make something edible and tasty

### Hippiah

* Rapid growth for plants and other materials
* Healing / health restoration
* Feeling the balance of nature and noticing when and where it has been disturbed
* Communicating with animals

### Aquah

* Weather control / Storms
* Communicate with sea creatures via telepathy
* Water control (water spouts, water blast)

### Zombiah

* Reanimation spells (resurrection)
* Understand and repair machinery
* Bring constructs and machinery to life

### Ah

* Communicate with spirits
* Spirit blast
* Direct spirits to help find or retrieve objects or move obstacles

## Duration of a spell

The default duration for aspects created with a spell is the length of the scene. Some spell effects might last longer. (A spell to carve a Chaosah symbol into the rocks lasts for as long as the rocks exist and haven't been eroded.) Some spell effects might be immediate with temporary consequences. (A spell to create a flash of light lasts for an instant, but the aspect "Temporarily Can't See" will continue for those who couldn't avert their eyes). Making a spell last longer than a scene increases the difficulty of the spell. Creating a fireball that lasts for a scene might be trivial for Saffron (no roll required), but making a torch that burns for several years unattended would be of Great Difficulty for her. Pepper may have Good Difficulty creating a micro-dimension lasting for a few hours, but creating a permanent micro-dimension laboratory that doesn't collapse is closer to Legendary difficulty (or more) for even the most advanced Chaosah witches.

## Creating Potions

Potions are spells that are created, carried, and used later. Potions may be created by using either the Quick or Careful approaches. They may also require other ingredients that the characters may have or may need to locate. Once the characters have found the required ingredients they combine those ingredients to create the potion. Use the Quick or Careful approach to Create an Advantage to combine the ingredients correctly and create the potion aspect. The potion then contains the aspect that you wish to create. The level of your approach is then recorded as the potions Effectiveness. Write down the aspect and the effectiveness of the potion.

Camomile is working on a potion of rapid growth. She uses her Quick approach to create the potion and rolls a Poor (-1) result. Her player then writes "Potion of Rapid Growth (Good)" on an index card.

Potions created at Poor level or lower are unstable and unusable. Depending on the scene the GM may choose to have the potion do nothing, have the opposite effect, or become a potion of something unexpected. If the goal was for Camomile to create a potion of rapid growth and she creates a Poor potion then the potion may have no effect, have the opposite effect (it becomes a potion of rapid shrinking), or does something unexpected (it becomes a potion of itchiness, a potion of rapid hair growth, or something equally unpleasant.)

## Using potions

Using a potion is the same as casting a spell. Use the effectiveness of the potion as the starting level for the spell and roll to find if the potion succeeds. On a successful roll the aspect from the potion is added to the character, object, or scene.

### Example of creating and using potions
 
Pepper is creating a Potion of Genius. She uses her Quick (Good) approach to create the potion. She doesn't add any other bonuses to her roll, and rolls a Mediocre result (+0), which doesn't add anything to her Quick (Good) approach. Her potion now has the aspect "Potion of Genius" at the level of Good. The GM determines that making a cat into a genius is Good difficulty. She rolls again and gets an Average (+1) result, which is more than the difficulty of Good. When she gives it to Carrot he takes the aspect "Temporary Genius" throughout the duration of the scene. If she were to attempting to make inanimate rock a temporary genius the GM may set the difficulty at Fantastic or higher (Making an inanimate rock a genius is difficult, even by Hereva standards), so even the Great level of the potion would have no effect. 


## Optional rule: potions from other disciplines

If you are creating a potion that is based off the magic from one of the other schools you create at the same level as your ability with that school. Even if your Quick or Careful is higher than your skill with the other school's magic you still create potions and spells at your level of familiarity with that particular school's magic. 

Example: If Shichimi is creating a growth potion that uses Hippiah magic, so her potion is created at Mediocre. Good thing Camomile didn't participate or the canary might have been much larger!

## Optional rule: quality of ingredients

Certain ingredients may be of better quality than others. If the GM wishes they may keep track of the quality of the ingredients (based on the Fate ladder, from Terrible to Good) for the potion. Add the ingredients together and use that for a potion creation bonus.

Shichimi is creating a potion for rapid growth (a Hippiah-based potion). Her rating in Hippiah spells is Mediocre (+0). She has sourced ingredients for the spell (Poor, Mediocre, Good, Good, Great). She adds them together to get +6, which is capped off at Good (+2). She then rolls and gets a Poor (-1) result. This means that her Potion of Growth has the aspect Potion of Growth and an effectiveness of Fair. 

## Optional Rule: Too many potions

Potion bottles are brittle things. They can leak, break, or slip out of one's hands at inopportune times. If a player has more than three potions on their person at one time they may fumble them and cause them to break. The GM may require a test against a character's Careful Approach to ensure that one or more of the potion bottles doesn't break during a combat or other strenuous task. Breaking one potion bottle means that potion is now affecting the player who broke it. The GM determines which potion bottle broke and the potion's aspect now applies to the player character. For instance, if Pepper has three potions and manages to break one the GM selects from her list of potions. She is carrying two potions of invisibility and one potion for slowing down time. The GM decides that one of the potions of invisibility shattered while climbing a rock face, so now she's invisible against the rock face, which adds a level of difficulty to her rock climbing since she can't see her hands. She'd better be extra careful!

Be creative with the results of combining too many potions together. Combining a potion of resurrection with a Potion of Poshness and a Potion of Rapid Growth could mean having a well-dressed canary stomping through Komona. Who knows what might have happened had Pepper brought an actual potion with her? Perhaps a potion of invisibility might have made an invisible resurrected giant posh canary, or it could have interacted badly with the other potions to make light-reflecting resurrected giant posh canary. Let your imagination and the imagination of the table be your guide.

## Rea Exhaustion

If you run out of Fate Points while casting a spell or making a potion you may take the consequence Rea Exhaustion to gain a one-time Fate Point. Rea Exhaustion is not something to be taken lightly --- Rea Exhaustion affects your refresh level and adds an additional consequence to your character. Each level of Rea Exhaustion subtracts one from your current Refresh. If a character has three refresh and takes the moderate consequence of Rea Exhaustion their Refresh drops from 3 to 2. If they take a second consequence of Rea Exhaustion at the Severe level and their refresh drops from 2 to 1.

In the standard rules the GM awards one Fate Point for taking the Rea Exhaustion consequence. A more challenging game may be had by using the optional Balance of Rea rule below.

The consequence Rea Exhaustion can only be removed via rest or meditation or concentrated study. Each period of deep rest, meditation, or study reduces the Rea Exhaustion consequence by one point. Any refresh points that were removed because of Rea Exhaustion are replenished.

## Optional Rule: The Balance of Rea

Groups that prefer a more challenging game and more consequences for Rea Exhaustion may opt to use The Balance of Rea rule in their game. The default Rea Exhaustion consequence gives one Fate Point for Rea Exhaustion. This Fate Point comes from the GM's pool of points. The Balance of Rea rule changes where that Fate Point comes from. When a player character takes the Rea Exhaustion consequence they may select another player character that is closest to the player character, or the player character with the most Fate Points. That player then gives the player taking Rea Exhaustion one of their Fate Points. If this reduces the giving player's character to zero Fate Points they must then take a moderate Rea Exhaustion consequence and receive a Fate Point from one of the other players that has Fate Points. If that player is then reduced to zero then the Fate Points are distributed and subsequent consequences are taken. If all players have the consequence Rea Exhaustion and the players have run out of Fate Points to give then the GM may compel the group to accept a world aspect that grants each player one Fate Point. This aspect should reflect a massive Rea Shift in the world of Hereva. At the end of the Great War each of the Chaosah Witches were exhausted of their Rea. This caused a large Rea drain which caused the Great Tree of Komona to break. With the Great Tree broken, all of the magic of Hereva began to flow into another dimension, which caused considerable damage to the magic and residents in all of Hereva. The decision of how severe the exhaustion of Rea would be for your party of witches is up to you and your players.

 Some examples of large-scale Rea Exhaustion:

*  A dimensional rift appears, where curious and dangerous inter-dimensional beings lay in wait.
*  The spike in the flow Rea draws the attention of some of the other schools, who come to investigate what happened (This could draw unwanted attention to the characters if they're investigating something secret).
*  The depletion of Rea makes it more difficult to to regain Rea in this area, so Refresh might not be possible (the area is completely burned out).
*  Reality is distorted from the power drain. Subtle and not-so-subtle changes occur throughout the world (the human shopkeeper in the Komona Square is now part-rat, Star Fruit now have seven arms instead of five, Phandas are now twice as big as before, etc.).

These are just some examples. Let your imagination and your players be your guide. Rea Exhaustion is not something to be taken lightly by any witch but it could be a fun way to introduce other elements into the campaign.

# Creatures in Hereva

*Hereva is populated with many different and exotic creatures. Some of them date to the prehistory of Hereva and before magic came to the world, and some are the result of the Rea Explosion near the end of the great war. Many of them are fusions of creatures that we already know (Dragon-cows are the unlikely fusion of dragons and cows, Phandas are the combination of many different creatures, etc.). Some of them are creatures like our own but with strange properties (spiders that build webs to harvest light, ants that have a space program, etc.). This section will highlight some of the creatures of Hereva. It will also help you create your own magical hybrids to populate the landscape of Hereva.*

## Creating a creature

Scholars have long debated about the origin of many of the creatures in Hereva. The prevailing theory is that many of the creatures in Hereva were the result of powerful magic near the pre-history of Hereva. While there is strong evidence, this is only one theory. The only creatures capable of saying anything definitive about the pre-history of Hereva are the dragons, but so far they've been tight-lipped on the subject. This theory gained prominence after The Great War. After the explosion of Rea from The Great Tree of Komona there were countless new hybrid of animals to explore. This caused the scholars who study the creatures of Hereva to both jump with glee at the prospects of discovering and naming these new creatures and realize (with some dread) that all of their carefully crafted taxonomies were now rendered obsolete.

This is also true of many of Hereva's human and human-like characters, which can be combinations of various animals. Camomile in particular is part raccoon and human. There are also folks in the Komona Market that exhibit these traits. If a player character wishes to combine animal and human traits it is up to the GM to determine if this is acceptable in their version of Hereva (though we strongly encourage such hybrids). 

These are generic guidelines for creating a creature in Hereva. GMs are free to use or ignore these as they please.

**Start with a base creature**: Most creatures in Hereva start with a basic creature for the body. This can be anything from a panda, ferret, beaver, fish, or any creature you can imagine. We recommend starting with Earth-based creatures first because they're the ones that folks are most familiar with. Starting with a basilisk might be cool, but not everyone can picture what a basilisk is without a reference. 

**Add other features**: Begin adding other features to your creature. What would they look like with an elephant trunk? How about a duck bill? Do they have webbed feet, or are they flippers like a dolphin? Start gradually morphing the creature so you can picture the original creature and the changes that have been made to it.

**What does the creature do?**: Once you have the basic design for your creature you will want to fill out what the creature does. Does it fly over Hereva like a majestic dragon or does it cower in the forests of Hereva? Is it primarily underground? Is it nocturnal or does it come out during the day? Is it friendly to other creatures or is it frightened of them? Is it fearsome or fearful? Do they hunt in packs or are they solitary creatures? Do they ave written language or do they use other means of communication? Do they use light to feed or do they need other means of nourishment?

When you have a clear idea of what the creature does in its daily life you can then add aspects to it like "Drawn to feed on phosphorescent spiders", or "Attracted to shiny things", or "Incapable of giving accurate directions". Remember that these characters serve two purposes: they are there to give Hereva an ecology, but more importantly they are there to interact with the characters. If the characters encounter a creature that is "Attracted to shiny things" perhaps they can follow that creature to more shiny things (such as treasure). If they encounter a creature that "Feeds on light" then maybe they can use that creature's presence to find strong light sources (should they need them). The creatures can serve as set-dressing for a location, but they can also be important clues for the characters. They can also be antagonists to the characters. Encountering a creature that is "Attracted to shiny things" while they're carrying the treasure out of an abandoned castle could mean that the creature steals their treasure, and now they have to chase the creature in order to retrieve their treasure.

<!--%FIXME: More on Hereva's creatures.-->
## Example Creatures

<!--%FIXME: Elaborate more on this-->

Here are a few creatures to give you an idea of how to design your own creatures for Hereva. Also note that not all of these creatures are hostile or even a threat to the player characters.

\newpage{}

### PHANDA

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/2016-06-08_concept-art_e17_Phanda_livestreaming_by-David-Revoy.jpg} 
\end{wrapfigure}


Phandas are feared throughout Hereva. They are fierce creatures with a ravenous, insatiable appetite; usually for whatever is in front of them. They use their claws, spikes, and horns both for defense and to pin their prey, and their long trunk to grab and hold their prey as they feast upon them. Phandas do not move quickly. A human can outrun a phanda but they rarely can intimidate them. They also cannot turn around easily and will rely on the spikes on their back to defend them from rear-based attacks. Phandas live in small packs but hunt alone and are seldom seen with two or more present. 

Location: 

Aspects:

High Concept: Hungry beast of Hereva

Approaches:

Skilled at (+2): Grappling an opponent, Tracking their opponent

Bad at (-2): Moving fast, Attacks from behind

STRESS \SD{}


\newpage{}

### ARGIOPE SPIDERS

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_argiope.jpg} 
\end{wrapfigure}

Argiope spiders are yellow spiders that are primarily found in The Haunted Jungle. Their most distinguishing feature is a grayish spiral on their body, either on the thorax or the abdomen. They are one of the few spiders in Hereva that hunt in packs, using an intricate system of webbing, signals, and gestures for capturing prey. They also have a remarkably well-developed hierarchy that is still being studied by Hereva's scientists. What can seem like a small batch of curious Argiope spiders might actually be scouting parties keeping watch over their territory and notifying the others as needed. There are stories of Argiope scouts drawing large numbers of Argiope spiders to take down their prey in what one researcher described as "methodical". Argiope spiders can become confused if their hierarchy is disrupted, which is the only way to dissuade these creatures from their appointed tasks. The red berry trees have proven the most fruitful places for Argiope spiders to congregate because many of their prey cannot resist the taste of the red berries. 

Location: The red berry trees in The Haunted Jungle

Aspects:

High Concept: Intelligent spiders

Approaches:

Skilled at (+2): Tracking their opponents, overwhelming their opponents

Bad at (-2): Independent thought if hierarchy is disrupted

STRESS \SC{}

\newpage{}

### PHOENIX

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_phoenix.jpg} 
\end{wrapfigure}

Phoenix are large birds with long feathers on the top of their heads. They are made of magma and fire, and shine with a fiery brilliance. They are near-immortal with their regenerative powers. At the end of a phoenix's life they return to the Valley of Volcanoes. When their bodies are no longer able to contain the magma and fire that binds them together they are consumed by the magma and fire and turn into ash. Those ashes sprinkle down into the magma of the volcano, where it reforms the phoenix. Phoenixes in captivity do not thrive because they are unable to regenerate (some folks have tried to capture the phoenix but are left with nothing but inert ashes and charred cages when the phoenix dies). Female phoenixes can have around three eggs per year. It is difficult for the untrained eye to discern between the male and the female phoenix, and usually can only be done at temperatures that are uncomfortable for most beings.

Phoenix tears are highly sought after for Rea poisoning, a condition where one gathers too much Rea and is incapable of expelling it via spells or other means. Rea poisoning manifests as a magical rash that disrupts magic (hence the inability to reduce the amount of Rea). A phoenix will not willingly give up their tears, opting instead to challenge the supplicant to earn their tears. Many have tried; few have succeeded.

Aspects:

High Concept:  Magical fire-based bird

Approaches:

Skilled at (+2): Airborne combat, taunting their opponents

Bad at (-2): Cold climates, underwater combat

STRESS \SE{}

\newpage{}

### DRAGONCOW

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_dragoncow.jpg} 
\end{wrapfigure}

DragonCows are docile creatures that tend to congregate over the forests of Squirrel's End. They spend most of their days chewing on trees and calmly gliding over all of Hereva. Most creatures in Hereva don't tend to bother DragonCows, partially because of their large size and partially because mature DragonCows have been known to kick their opponents with such force as to break the sound barrier. DragonCows are prized for their milk, with some finding it tasting better than ordinary cow's milk. DragonCows are much more intelligent than they appear and are capable of critical thinking and complex problem solving. Unfortunately DragonCows are a little clumsy during take-off and landing, which is why you'll no doubt be able to locate where a DragonCow has attempted take off \slash{} landing by the wreckage of trees and the ruts in the ground.

Aspects:

High Concept: Docile half-cow, half dragon creatures

Approaches:

Skilled at (+2): Airborne combat, kicking opponents

Bad at (-2): Landing / Taking off

STRESS \SE{}

\newpage{}

### FISHER OWL

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_fisher-owl.jpg} 
\end{wrapfigure}

The Fisher Owl is a unique creature in the ecology of Hereva. Most Fisher Owls appear as regular owls, and indeed many of them are indistinguishable from other owls. It is only with the presence of a fishing pole that the unique characteristics of the Fisher Owl become apparent. The Fisher Owl notices the fishing pole and immediately creates such a strong bond that it will use that fishing pole to capture its prey. Many times that will result in the owl using a piece of cheese on a hook as bait to lure unsuspecting mice towards it, but it has also resulted in owls using bait for fish, bait for reptiles, and even on occasion bait for humans (there have been no reports of a human taking the bait, however.) This of course has scientists asking how the Fisher Owls acquire little fishing poles, as none have been seen assembling poles from string, twigs, and hooks. The current theory is that poles are passed down from generation to generation though there are theories that someone or something is busily making little poles for Fisher Owls. One day scientists hope to unravel this baffling mystery.

Most Fisher Owls have been spotted in the forest of Squirrel's End but there may be other locations where they hunt. It is not inconceivable to find Fisher Owls sitting in frozen areas fishing, but one wonders where they got the tiny drills to puncture the ice.

Aspects:

High Concept: An owl that fishes

Approaches:

Skilled at (+2): Fishing, luring opponents

Bad at (-2): Telling the truth

STRESS \SB{}

\newpage{}

### SAVANT-ANT

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_savant-ant.jpg} 
\end{wrapfigure}

The Savant-Ants are a group of ants that were magically uplifted to genius intelligence by one of Pepper's potions of genius. She thought the potion had failed after testing it on Carrot and threw it out the window onto a colony of unsuspecting ants. These ants have gone on to make several discoveries including mathematics and space travel, as later witnessed by Pepper. Savant Ants look like regular ants save for their incessant need to tell other ants how many advanced degrees they have. They are currently located in Squirrel's End near Pepper's house but with their current ambitions they could be anywhere in Hereva, exploring. Some may even be sneaking into Pepper's House reading the Chaosah Tomes in the hopes of uncovering more secret knowledge for their own experiments.

Aspects:

High Concept: Genius ants

Approaches:

Skilled at (+2): Spatial reasoning, organizing

Bad at: (-2): Individuality %FIXME: Find something better for this

STRESS \SB{}

\newpage{}

### DRAGON-DRAKE

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_dragon-drake.jpg} 
\end{wrapfigure}

Dragon-Drakes are the canonical example of the strange hybrid creatures that came about after The Great Tree of Komona exploded and ripped apart the fabric of magic in Hereva. Many scientists have pondered what caused these creatures to have the head of a duck and the body of a small dragon. What is known about these curious creatures is that they behave more like ducks than dragons, playfully soaring over and around the skies of Komona. They are about the size of a standard duck, save for their long draconic tail. Some have been found floating on water but most times they spend their time in the air. Dragon-Drakes are mostly harmless, but there have been instances of Dragon-Drakes getting attached to various witches flying into the city of Komona, only to be jilted when they are waved away (mostly because the witch can't see in order to navigate the complex landing to get into Komona).

Aspects:

High Concept: A dragon-duck hybrid

Approaches:

Skilled at (+2): Flight, pecking

Bad at: (-2):  Swimming%FIXME: Need something for this

STRESS \SB{}

\newpage{}

### OVERSIZED-POSH-ZOMBIE-CANARY

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_oversized-posh-zombie-canary.jpg} 
\end{wrapfigure}

The Oversized-posh-zombie-canary is the unfortunate result of first magical potion contest held in Komona. The canary was first resurrected by Coriander using a Zombiah potion. The canary was then made posh by Saffron's "potion of poshness". Shichimi didn't realize that her potion would be used on the same entity and surmised quickly that the potion of extreme growth that she had concocted would make things worse. Saffron, mistaking Shichimi's reluctance for modesty, poured all of the extreme growth potion on the posh, zombified canary. Shichimi's potion worked better than even she could predict, turning the posh, zombified canary into an oversized, posh, zombified canary. The canary, confused and unsure what was happening, began fluttering about wildly. Naturally this spectacle got the attention of the Komonan guard, who prepared to attack this gargantuan creature. Fortunately Pepper convinced the creature through her, erm, unconventional potion. The bird was last seen sulking off to create a nest, which she guards with quiet dignity.

Aspects:

High Concept: A posh, zombified, oversized canary

Approaches:

Skilled at (+2): Flight, poshness

Bad at: (-2): Easily confused

STRESS \SD{}

\newpage{}

### OLD-SYLVAN

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_old-sylvan.jpg} 
\end{wrapfigure}

Old-Sylvan is a sentient tree that lives in the forest of Squirrel's End. He is part of Pepper's security system. He stands guard, patiently waiting for intruders or other passers-by and detains them. If necessary he will entangle any intruders in a series of vines that grow amongst his branches. He is a jovial creature, happy to have long chats with anyone who happens by but he takes his guard post seriously. He has two branches that he uses as arms and can use them to hold intruders to entangle them. Old-Sylvan is also home to several families of squirrels, who occasionally pop out of his mouth to check to see if there are any intruders nearby. They are curious squirrels and are very good at spotting any trouble that might be coming by Old-Sylvan's way.

Aspects:

High Concept: A jovial tree that is part of Pepper's security system.

Approaches:

Skilled at (+2): Snaring foes, long chats

Bad at: (-2): Moving quickly

STRESS \SD{}

\newpage{}

### THEOREM-THE-GOLEM

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_theorem-the-golem.jpg} 
\end{wrapfigure}

Theorem the Golem is part of Pepper's security system. He live in the mountains behind Squirrel's End. Most of the time he sits in quiet contemplation. Nobody has asked what he is contemplating during these moments. He sits so still during these moments of contemplation that grass, vines, and moss have grown on him. He even has a nest of pigeons on his left arm. But when an intruder flies by he springs into action. He uses a comically long ball-and-chain to keep his prisoners from getting too far away, but allows them to walk for a bit if they feel like it. He takes great pride in presenting is unwitting captives to Pepper when she remembers to come collect them. Who knows what might happen to them if she didn't check in from time to tine, as Theorem doesn't seem to be proactive in their release.

Theorem was recruited by Pepper to help with her security system. As she was wandering around her house she noticed him sitting there in quiet contemplation. When she asked what he was doing he didn't answer her. Mistaking him for a statue she began placing various trap elements around him until she realized that he was following her every move. She then asked again what he was doing. "Thinking" was his reply. She then asked him if he would be willing to help keep intruders out of her house. After a few moments he replied "yes" and that was that. Pepper never had to worry about intruders coming in from the mountains again.

Aspects:

High Concept: A quiet, contemplative golem who is part if Pepper's security system.

Approaches:

Skilled at (+2): Snaring foes, moving quickly for his size

Bad at: (-2): Conversation

STRESS \SD{}

\newpage{}

### DRAGONMOOSE

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_dragonmoose.jpg} 
\end{wrapfigure}

Dragonmoose are the result of a magical fusion of dragons and moose in Hereva. This occurred during the explosion of magic during the collapse of the Great Tree of Komona. Herevan biologists are still trying to work this one out since moose are not native to the universe of Hereva at all. It took some inter-dimensional sleuthing to even determine which creature had suddenly fused with dragons, giving proof for the inter-dimensional theorists that Hereva is connected to other universes while also giving the inter-dimensional skeptics more fuel for their claims that the universe of Hereva was a projection of other universes upon it. Their claim was that a moose was too ridiculous of a creature to exist in any universe. Some argue that their reasoning was based on flawed inter-dimensional research, especially the research that claims of a partnership between moose and squirrels. Regardless the dragonmoose still exists in the world of Hereva and can be found in the mountains of Squirrel's End. They have nests with brightly colored eggs where young dragonmoose are hatched. Since the dragonmoose is a relatively young magical concoction little of their habits, let alone ecology has been studied in depth. Several attempts to observe the dragonmoose have been attempted but most, if not all expeditions have found themselves being plucked from their observational spaces and placed gently into the dragonmoose nest, where a bemused dragonmoose nuzzles the observers until they make their escape. Pepper has used this feature of the dragonmoose as part of her security system around her house. 

Aspects:

High Concept: A fusion of dragon and moose that is too friendly.

Approaches:

Skilled at (+2): Spotting observers, Flight

Bad at: (-2): % FIXME: Come up with something here

STRESS \SD{}

\newpage{}

### BIGFISH

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_bigfish.jpg} 
\end{wrapfigure}

The Bigfish is just that: a very big fish that lives in the seas of Hereva. It uses its large mouth to eat smaller fish and sea creatures. It can also use its mouth to scrape the small islands of Hereva for any land-based creatures. It spends most of its day eating, storing the results of its fishing in its mouth for days or weeks. It then swallow its catch and begins the slow digestion process. Bigfish are indiscriminate in what they will eat as it can take days for it to suffer the consequences of its meal. The fish does have a giant uvula in its mouth which if attacked can be painful and cause the fish to expel its undigested catch. Most sea creatures have never seen a uvula though so they don't know its purpose.

Aspects:

High Concept: A giant fish that seems to consume anything in its path

Approaches:

Skilled at (+2): Swimming, Consumption

Bad at: (-2): Tactics % FIXME

STRESS \SD{}

\newpage{}

### SPIDER-ADMIN

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_spider-admin.jpg} 
\end{wrapfigure}

Spider-admins are the caretakers and administrators of Hereva's Crystal Ball Network. This network allows folks access to news, videos, entertainment, gossip, and, yes, even the more sundry things in Herevan life. Not everyone in Hereva is connected to the network since the crystal balls can be quite expensive and the costs to maintain a direct wired connection can be quite high. Fortunately there are also public access crystal balls provided in many different locations. Spider-admins make sure the network is operational and also consult with folks wanting to generate sites for folks to browse. Spider-admins are quite large and can seem intimidating at first but they are more interested in helping folks to enjoy the network they created than in intimidation. One set of administrators likes in a cave where Thyme has set up a Crystal Ball Terminal for her own use (she doesn't enjoy sharing with others, and wants the fastest connection available). Spider-admins rarely leave their cave, preferring the glow of the crystal balls they use for development and administration.

Aspects:

High Concept: Large spiders who manage the crystal ball network

Approaches:

Skilled at (+2): Crystal Ball Administration, Crystal Ball development

Bad at: (-2): Being outside of the cave% FIXME

STRESS \SC{}

\newpage{}

### GENIE OF SUCCESS

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_genie_of_success.png} 
\end{wrapfigure}

The Genie of Success is a capricious magical being who shows up when folks are at their lowest. He offers dreams of power, wealth, and glory but at a cost that is hidden within a maze of legalese and contracts. It's unclear how the genie benefits from these transactions but invariably his glowing teapot will drop on unsuspecting folks who feel the world has left them battered and broken with no clear way out. Some claim that the genie has made noble bargains in the past but on closer inspection the terms cost way more than the benefit received.

The genie himself is a handsome walrus-faced figure with broad shoulders, chiseled features, and a turban. His skin is yellow with brown stripes. He wears a red vest and golden bands that cover his lower arms. He travels in a teapot, though most times one cannot see it until he is ready to make a deal.

Aspects:

High Concept: A large, handsome genie who wants to make a Faustian deal

Approaches:

Skilled at (+2): Making deals, Sensing weakness

Bad at: (-2): Accepting defeat

STRESS \SC{}


\newpage{}

### SCORPIO

<!--\begin{wrapfigure}{r}{0.50\textwidth}-->
    <!--\includegraphics[width=0.9\linewidth]{images/creature_scorpio.jpg} -->
<!--\end{wrapfigure}-->

Scorpio are giant, heavily armored scorpion-like creatures. They have four spiky legs and a scorpion-like tail. They live in nests full of human skulls and bones, which they collect from their unwitting victims. The tail has a giant poisonous stinger which the scorpio uses to subdue its prey. Its heavily armored head has two horned mandibles which it uses to trap prey. Scorpio are used as guardians around treasure. They keep watch and are formidable foes for any adventurers caught in their path.

Scorpio venom does not kill its victims. The venom causes intense pain and incapacitates the victim. If left untreated it can paralyze the victim after several hours. Hippiah magic can remove the venom but requires intense concentration to find the venom and remove it.

It is possible to overpower a Scorpio by surrounding it but most scorpio will use a wall or other barrier as a defense against such attacks. 

Aspects:

High Concept: A large, armored scorpion with four legs and a poisonous tail.

Approaches:

Skilled at (+2): Attacking with mandibles, stinger

Bad at: (-2): Turning around, rear attacks

STRESS \SD{}

\newpage{}

### SHAPESHIFTER

<!--\begin{wrapfigure}{r}{0.50\textwidth}-->
    <!--\includegraphics[width=0.9\linewidth]{images/creature_shapeshifter.jpg} -->
<!--\end{wrapfigure}-->

The Shapeshifter is a class of beings used to guard important items such as treasures or other secret areas. It's so named because of its abilities to transform from a smooth creature into a horned creature. During its transformation it lets out a piercing yell that can crack glass and deafen its opponents. Shapeshifters are magically created creatures, usually created by powerful wizards or witches. They are formidable creatures in their natural state with pale gray skin, large teeth, and huge claws. But when they are enraged they transform into an even larger, deadlier creature with glowing skin and more demonic features. Only a handful of them have been encountered in the wild. Each time the adventurers have barely lived to tell the tale. 

Aspects:

High Concept: A large, armored creature that can transform into an even larger, deadlier creature

Approaches:

Skilled at (+2): Attacking with claws and teeth, screeching

Bad at: (-2): Surrounding attacks

STRESS \SD{}

\newpage{}

### DEMONS of CHAOSAH

The demons of Chaosah are a group of demons who have pledged their loyalty to the Chaosah witches. They live in a plane outside of Hereva's reality and must be summoned to come into Hereva's reality. During the Great War Chaosah commanded thousands of demons. How they have three who are able to be summoned. It is unclear what pact was made with such fearsome creatures to get them to do Chaosah's bidding but it is a strong bond that the Chaosah demons cannot resist. They are summoned using arcane Chaosah magic, combining summoning circles, incantations, and spells. They are quite used to having the Chaosah witches summon them for attack that they immediately dispense with any of the pleasantries of most demons and jump straight to "who do you want us to attack?". The three remaining Chaosah Demons are as follows:

\newpage{}

### HORNUK

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_hornuk.jpg} 
\end{wrapfigure}

Hornuk is a Chaosah demon with green fur and two prominent horns on his head. He is a fierce demon who not only uses his claws and horns to gore his opponents but also consumes his enemies with his gigantic maw, where they are slowly digested over hundreds of years. He is not as magically gifted as the other Chaosah demons but he doesn't have to be. He will relentlessly pursue his opponents until they either give up or are destroyed; whichever comes first.

Aspects:

High Concept: One of the Chaosah demons

Approaches:

Skilled at (+2): Attacking with horns, consuming enemies, Pursuit

Bad at: (-2): Resisting sweets

STRESS \SD{}

\newpage{}

### EYEUK

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_eyeuk.jpg} 
\end{wrapfigure}

Eyeuk is the leader of the Chaosah demons. He has red fur, four eyes, and a trident that he wields. He has three horns on his head. As the leader of the of the Chaosah demons he thinks more strategically than the other demons. He also uses his eyes to survey get a sense of what dangers might be lurking ahead. He uses his trident as a weapon and skillfully spears his opposition before they have time to react. When attacking he slows down time in order to react more quickly to his foes. He possesses the ability to cast Chaosah spells but has been locked away from traversing into other universes unless summoned; a restriction he is desperate to break. One must be careful around the clever Eyeuk, who will try to release this restriction at every turn.

Aspects:

High Concept: The leader of the Chaosah demons

Approaches:

Skilled at (+2): Noticing, goring with his horns and trident, Chaosah magic

Bad at: (-2): Polite company

STRESS \SD{}

\newpage{}

### SPIDUK

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_spiduk.jpg} 
\end{wrapfigure}

Spiduk is a Chaosah demon. She looks similar to a large purple spider. She is also skilled at Chaosah magic but lacks the ability to travel between universes (much like Eyeuk). Unlike Eyeuk though this restriction doesn't bother Spiduk, who is contented with her service to the Chaosah witches. She is most comfortable on the battlefield where she will attack several opponents at the same time using her many arms and webbing. She has also mastered Chaosah battle magic which allows her to alter reality to her choosing and gives her formidable powers. She is not to be taken lightly and many foes have learned the hard way not to cross her path. However she also appreciates the finer things in life like a nice cup of tea and will stop her battle rages to indulge a bit in the finer things like pastries, cakes, and the like before destroying her enemies.

Aspects:

High Concept: One of the Chaosah demons

Approaches:

Skilled at (+2):  Multiple attacks, Spying, Chaosah Battle Magic

Bad at: (-2): Resisting a cup of tea

STRESS \SD{}

\newpage{}

### DRAGONS OF HEREVA

Dragons roamed the lands of Hereva for as long as any memory can recall. The time they aren't soaring over Hereva's landscape they spend guarding their lairs. They are capricious beings, with their own desires and agendas. There is even a myth that the sun of Hereva is comprised of two dragons fighting a continual battle and launching great bursts of flame at each other. Most dragons do not elaborate on this myth, and will quickly change the subject should it come up.

They regard humans as curiosities and are fascinated that beings with such short lifespans are constantly inquiring about events they can't possibly know.

There is a type of dragon known as the Petite Dragon that has been known to be around members of Ah. Whether these can be considered "true" dragons or another species on Hereva has yet to be determined. After a number of years the members of Ah release these Petite Dragons into the wild, never to see them again. It is difficult to keep track of dragons in the wild as magical tagging never seems to stick on them for long. Some believe the dragons seen with members of Ah are actually young dragons, and the reason they often return to the land of the setting moons is because that is their home. Whatever the case, the dragons and members of Ah will not elaborate on their relationship.

Some dragons become magically bonded to members of Ah, allowing them to be ridden as fearsome mounts. One can only wonder how such pairings are achieved since the dragons still maintain their autonomy and draconic ways.

Near the end of a dragon's life, it will head toward the three setting moons, near the temples of Ah. There it will spend the rest of it's days in quiet rest and contemplation. The members of Ah are respectful and do not disturb any of the dragons in the area unless the dragon initiates conversation. To disturb a dragon in these final days is considered anathema, and carries stiff punishments.

### Dragons and Hereva's Pre-History

The Dragons of Hereva have lived in the land of Hereva long before human recollection. It wasn't until The Great Tree of Komona that the dragons took notice of the humans in Hereva. Whether that was because the humans didn't arrive until The Great Tree of Komona appeared is something the Komonan scholars wrestle to resolve. Many conversations with the dragons prove inconsistent at best and obfuscated at worst. One theory is that the dragons simply did not pay any attention to the humans until they wielded magic, much in the same way that humans don't pay attention to the ants until they start building superstructures in their back yard. Another theory is the dragons purposefully obfuscate human history, answering in riddles and conflicting details in the hopes of keeping humans away from some hidden lore.

\newpage{}

### Red Dragon (Fire Dragon)

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_red-dragon.jpg} 
\end{wrapfigure}

Red Dragons are reddish, scaly dragons. They have a bony neck frill that is protected by eight horns on the dragon's head; two on top and three horns on each side. This frill also has horns protruding from it. Red Dragons are temperamental, even by dragon standards. They live in the cave systems of Hereva, where they can amass large treasure hordes of gold. Red Dragons protect their lairs with their fiery breath, along with their wings, claws, and horns. Red Dragons spend most of their days sleeping near or on their hordes. They do not suffer trespassers or fools who enter their lairs, choosing instead to attack with their breath upon discovery. Should the intruder persist the dragon will exert more effort to expel the intruder. Few Herevan scientists have managed to study Red Dragons, opting instead for less temperamental creatures like the Dragon Cow instead. Red Dragons are quite intelligent and have been known to have long conversations with those they tolerate but getting to that point is quite the challenge.

Aspects:

High Concept: A red, temperamental dragon

Approaches:

Skilled at (+2):  Fire breathing, horned attack

Bad at: (-2): Tolerating intruders

STRESS \SD{}


\newpage{}

### Air Dragon

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_air-dragon.jpg} 
\end{wrapfigure}

Air Dragons are dragons that spend most of their time flying over Hereva. They are greenish in color and are optimized for flight. They have webbing on the sides of their long, aerodynamic heads. It is rare to see an Air Dragon not in flight, and any Air Dragon not in flight is probably wounded or exhausted. They are similar to dragon cows in their penchant for tearing off the tops of trees or other flora in Hereva, though some have been known to pick up livestock if hungry enough. Air Dragons are a rare sight, and seeing more than a few of them in flight is considered good luck.

Aspects:

High Concept: A greenish dragon in constant flight

Approaches:

Skilled at (+2):  Flight, Precision airborne attacks

Bad at: (-2): Being on the ground

STRESS \SD{}

\newpage{}

### Swamp Dragon

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_swamp-dragon.jpg} 
\end{wrapfigure}

Swamp Dragons are dragons that live in the swamps of Hereva. Their composition is curious, as it is unclear if they are dragons that prefer the cooling mud of the swamp or magical constructs made of mud that look like dragons. Nobody has ever seen a swamp dragon outside of the swamp so the question remains. They have a playful nature, but if threatened they can project a massive spray of mud at any attackers. This spray has proven a strong deterrent to Herevan scientists who have tried to answer the question of the dragon's composition. Swamp Dragons don't mind visitors and will chat for hours with anyone who wanders through into their swamp. They are not as territorial as Red Dragons, but they still have a territorial instinct that can prompt them to perceive a visitor as a threat (though they will at least give ample warning before unleashing their mud-spray).

Aspects:

High Concept: A muddy dragon that lives in swamps

Approaches:

Skilled at (+2):  Spray attack, playful banter

Bad at: (-2): Being outside of the swamp

STRESS \SD{}

\newpage{}

### Lightning Dragon

\begin{wrapfigure}{r}{0.50\textwidth}
    \includegraphics[width=0.9\linewidth]{images/creature_lightning-dragon.jpg} 
\end{wrapfigure}

Lightning Dragons are elemental creatures made of pure energy. Most of the time they can be found in their fearsome dragon-form but they sometimes change into other forms. Lightning Dragons are more elemental than dragon, however for reasons of taxonomy and because early Hereva Scientists saw more dragon characteristics rather than elemental characteristics the Lightning Dragon is classified as a dragon. Like the swamp dragon they tend to be more curious than aggressive and take great pleasure in their occasional visitors. Unfortunately the visitors of Lightning Dragons can find the experience a bit shocking if they're not well grounded. In their dragon for the Lightning Dragon takes the appearance of a large dragon with horns on either side of its beak and large teeth. When provoked the Lightning Dragon prefers to stay in its dragon form, using its beak to deliver charged bites to its opponent. Touching a Lightning Dragon can provide a nasty electrical shock, as the dragon itself is pure electrical energy. Herevan scientists have repeatedly tried to find the source of the Lightning Dragon's power but each time they tried the dragon overloaded their instruments. Lightning Dragons will chat with their visitors and offer both insight and wisdom to those who seek it. They are kind and patient, even with those who are obviously ill-prepared for their lessons.

Aspects:

High Concept: An electrical elemental in the shape of a dragon

Approaches:

Skilled at (+2):  Electrical Spray attack, dispensing wisdom

Bad at: (-2): Dealing with water

STRESS \SD{}



# Locations in Hereva

Hereva has many different locations to explore. Some of the locations you'll encounter in this section:

* The floating city of Komona where merchants from far and wide come to do business in the marketplace.
* Qualicity, the industrial hub of manufacturing and innovation.
* The Temples of Ah, where the adepts of Ah train to become more like the spirits they worship and converse, and where dragons find respite in their waning years.
* The Village of Squirrel's End, where Hippiah's horticultural experiments and farming have an uneasy peace with the Chaosah witches that live on the outskirts of town.
* The undersea realm of Aquah, a foreboding place of tradition and honor that does not suffer outsiders gladly.

Each location will describe landmarks and interesting places for the characters to explore, as well as some adventure seeds to give you some inspiration on how to use these locations in your campaign. These locations are not the only places you can explore in Hereva, nor are these descriptions the final word on what you may do in these locations. You can adapt them however you like to fit your own campaign.

## Where's the map?

% FIXME: Need to make this more clear, and possibly put this back into the wiki

Hereva is a designed to be both a stand-alone world or part of a larger world. It doesn't contain a map for this specific purpose. Several areas in Hereva have consistent locations but the rest of the world is up to your imagination. 

Hereva is a magical world that resists mapping, with only a few places having consistent locations (Ah for instance is always found where the three moons set). This will seem strange to those who are more used to concrete locations and concepts of latitude and longitude denoting where something is but folks in Hereva find navigating from one part to another is relatively straightforward (provided they can find their bearings). It also helps that Hereva has a thriving business dedicated to providing up-to-date maps for travelers who need the latest information (for a modest fee of course). These maps are regularly updated, and it's not uncommon to see mappers flying overhead using surveying magic to update the maps for their subscribers. Folks can still find their way around Hereva to certain landmark cities with training and magical acumen (and a magically-tuned compass). Places like Qualicity and Komona send out magical beacons to help less-skilled travelers in finding them (the markets of Komona would be much less traveled without these beacons). Ah is always be found where the three moons of Hereva set, so travelers seeking the temples of Ah are advised to keep watch for the setting moons to have their best chance of finding them.

Residents of Hereva are able to navigate this easily, and with broom-based flying technology it doesn't take long to get to any location within Hereva. That doesn't mean that every location is well-mapped. It's still possible to be lost in certain forests, or to get turned around while in-flight (it can also make for an interesting scenario where players believe they are heading to one place, only to find that it has moved without their knowledge, but we advise using this plot device sparingly).

## Creation of Hereva

Many legends exist about the formation of Hereva. Some claim it was formed when the dragons took to fighting each other in some grand conflict. Some also believe the star Hereva orbits is lit by the fires of dragons still locked in battle with each other. Others theorize that Hereva and its moons formed out of the dust from another magical world. Some claim Hereva is a young world, while others claim Hereva is as old as time itself. Few can say for certain what is the truth as archaeology is a young science in Hereva. Herevan archaeologists also have a more difficult time in finding undisturbed sites. Often the areas where they dig have already been disturbed by Chaosah and their incessant burying of failures. More than one archaeological expedition abruptly ended when unsuspecting diggers found the remnants of a Chaosah failure that should never have been uncovered. It's for this reason that insurance on archaeological digs is difficult to obtain. Having your entire archaeological site suddenly flung into a not-quite-stable Chaosah black hole (or worse) can really affect the bottom line of an expedition.

## Moons of Hereva and other Celestial Objects

Hereva has three moons orbiting it, which are visible during Pink Moon. They are simply referred to as "The Three Moons of Hereva". Some creative folks have tried to name the moons of Hereva, but outside of the book "The Names of the Moons" by H. R. Trifle and a moon naming contest held by the Hereva Astronomical Society, nobody else refers to the moons by those names. This isn't because of some grand conspiracy to keep the moons unnamed, but rather that the names selected for the moons weren't very memorable (one notable name for the largest moon was "Lamprocapnos spectabilis", which even the contest winner disavows, claiming that it was a statement about how the Hereva Astronomical Society can strip the fun out of everything, including a contest to name the moons). The book "The Names of the Moons" by H. R. Trifle was considered a best seller until actual readers of the book realized that the book was more about the history of H. R. Trifle, how he came up with the names of the moons (spoiler: they occurred to him in the bath), and an incomprehensible 300 page diagram of the specific reasons for each name and their historical connection. Only the most diligent readers discovered that the book didn't actually contain the names of the moons. It was later learned that this was an editorial oversight as the editor was not one of those most diligent readers who could finish reading the book. "The Names of the Moons" still maintains a cult following among those who say it's a masterpiece of Hereva Moon Scholarship, though the main reason it still sells at all is because its unique shape and size makes it perfect for raising crystal balls to ergonomic height.

Bergamot's Teapot is in an orbit opposite the three moons. It was named after Bergamot the Wise, who first postulated the existence of the teapot. Over time the teapot has been seen by many who carefully scan the skies of Hereva just as the three moons of Hereva begin their ascent. Some call the teapot a refraction of the three moons or a clever illusion. A few believe the teapot is a hoax. But those who look carefully and believe can render the teapot with little difficulty.

## Weather in Hereva

One might think that because of the chaotic nature of mapping Hereva that the weather of Hereva would be similarly difficult to predict. This is not the case as Hereva's weather is very predictable. Aquah is the school that is most in-tune with Hereva's weather, but Aquah has distanced itself from the remaining schools of Hereva. Several agencies have sprung up and made lucrative businesses for predicting Hereva's weather. So accurate are their predictions that they have been able to find when folks have changed the weather by casting weather-changing spells. A notable example of this was a Hippiah farmer who was having problems with his prized tomato plants. Somehow the rain kept avoiding his plants. He complained to the agencies saying that their predictions were somehow neglecting his farm, but they responded that they only predict the weather, not make it. Furious, he spent many nights learning weather-related spells. He then went out and conjured a small rain storm over his tomatoes. He magic skills were lacking, and his inexperience created a spell that quickly got away from him. The spell created a rain cloud that grew so large that it covered most of Hippiah and flooded their crops. Angry farmers demanded to know why the weather agencies were wrong with their forecasts. The weather agencies compared their forecasts and realized that this storm seemed to sprung up over one location: the farmer's tomato plants. Hippiah farmers demanded laws to forbid anyone from tampering with the weather in any way. But rather than punish the old man (who had suffered enough because his plants were ruined and his land was flooded) the village of Hippiah helped him to grow new plants in a different location, where his tomatoes received first prize that year. He gratefully shared his prize with the rest of the village. The flooded tomato plot was named "the swamp of folly" until it eventually drained and could be farmed again.

## Komona

The flying city of Komona is built around the base of The Great Tree of Komona. The tree's rounded branches give shade over most of the city, and its roots support the city in a perfect sphere. Despite its open appearance Komona is well protected, and all of the traffic flowing in and out of the city must pass through the gate of Komona under the watchful eyes of the Komonan Guard. Komona's residents are rich and upper-class, and property on Komona is expensive by Hereva standards. Even a tiny office space in Komona can rent for an eye-watering amount of Ko.

Part of the wealth of Komona comes from the weekly markets, where vendors and travelers from all over Hereva come to purchase food, magical artifacts, spells, and much more. Komona also infrequently sells harvested wood from the Great Tree of Komona, which can fetch very high prices. There is a thriving trade for Komonan wood, and stiff fines for anyone caught selling non-genuine or illegally-gathered wood. Some vendors have been caught casting a flotation spell on their wood to make it float like real Komonan wood, only for the spell to wear off once the poor buyer has finished the sale.

Brooms made from 60\% or more Komonan wood are exclusively sold at auction, while 10\% (or lower) wood blends can cost around 50,000 Ko or more. This has led some folks to bring a Komonan wood appraiser with them to certify the quality of the wood. Such appraisers can charge as much as a cheap Komonan wood broom for their services, so their use is usually reserved for the more expensive and exclusive purchases.

Because Komona floats above the realms of King Acren it believes it isn't subject to King Acren's taxation or any of the rules or mandates of his kingdom. This has caused friction between King Acren and the government of Komona (which only has a mayor as its highest office). Unfortunately King Acren has been unable to subjugate Komona because his rule over his kingdom is waning, and because his armies are land-based units and are not trained for airborne combat. Should King Acren be able to prove a more effective king he would certainly try to subjugate Komona, but his ineffective rule makes Komona the most desirable prize that is literally beyond his reach.

Most Komonans enjoy plenty of leisure activities in their floating metropolis, and many flying boats travel to Komona for commercial trade and other and tourist attractions. The most popular of these is the pruning of the Great Tree, where folks gather to witness the tree being reshaped by expert Komonan gardeners. Watching the tree regrow its branches shortly after being cut is breathtaking in itself, but also watching the skill of the gardeners trimming each branch with precision and care is also a meditative experience. The main draw, though, is waiting for the occasional apprentice gardener who, through inexperience or accident, cuts too much of the branch and gets tossed off of the platform by a rapidly growing branch. Such accidents are infrequent but have happened times that the Komonan Broadcasting Network dedicates most of their resources to covering this event.

Occasionally Komona opens its city gates for large-scale events, most notably the first "Potion Challenge". After the disastrous results of the first potion challenge (requiring many hours to rebuild and repair the market square) the City Elders of Komona decreed that future potion challenges must not be held in the city proper. For the second magical challenge an arena was built in the hopes of drawing the lucrative crowds to Komona while reducing the risk of damage to the city.

Saffron lives in the heart of Komona. Her office for magic consulting is in the heart of Komona's office district.

**Komona Aspects**: Hub of trade and commerce, Highly secure, Expensive

### The Komona Market

At the heart of Komona is the marketplace known as the Komona Market. This weekly market is one of the longest-running and largest markets in all of Hereva. Here one can find most anything, from foodstuffs to potion ingredients and so on. Unfortunately it's also not the cheapest place to get any of these items as renting a table in the Komona Market can be pretty expensive. The market is patrolled by both uniformed and plain-clothes Komona security guards who ensure the safety of the market. Folks travel from all over Komona to participate in its market, in part because everyone is already there.

Certain vendors at the Komonan market have experimented with delivery service where one can order items from the market and have them shipped directly to their door. Overall this has been successful but at times it can cause traffic jams at the Komonan Market where the number of folks trying to make a delivery can interfere with the number of folks trying to get to and from the market. This has caused problems for some vendors who feel they're not getting the same clientele numbers as before, and also puts pressure on the vendors to ensure that they too have some form of delivery, contributing to the delivery bottleneck issues.

Illegal items are not available in the Komona Market and anyone caught selling or buying them is barred from the market for life.

**Komona Market Aspects**: Central market for Komona, High Traffic, Expensive

### The Great Tree of Komona

The Great Tree of Komona (or more colloquially "The Great Tree") is the most revered tree in all of Hereva. There is a legend that the Great Tree of Komona sprouted when magic was young in Hereva and within minutes The Great Tree grew larger than any other tree in Hereva. Many believe the tree mimics the health of magic in Hereva, and a branch of Komonan botanists regularly sample the leaves of The Great Tree to measure and predict the flow of Rea throughout the land.

Early in the tree's life there were older branches that fell from The Great Tree. The citizens of Komona realized that the branches of The Great Tree allowed for a considerable flow of Rea. The branches could float like The Great Tree itself with minimal effort. Over time the occasional branches that fell from the tree became the center of the Komonan economy, who exported the wood throughout the land of Hereva. A centralized market sprung up in Hereva where wood and other magical wares from the tree could be purchased. Enterprising Komonans, eager to harvest more wood, figured out that with care The Great Tree could be pruned to gather more Komonan wood rather than wait for it. The tree displayed a remarkable ability to heal itself, creating fresh growth over the pruned area. This caused the tree to grow in odd shapes and the Komonan Beautification Project petitioned to have only a handful of trained arborists handle the pruning of the tree. Travelers from all over Hereva come to Komona to witness The Great Tree and buy wands, trinkets, and other items made from The Great Tree. There's even a legend about a Komonan who fashioned a table and chairs out of wood from The Great Tree. As he built his furniture the Rea he generated leached into the wood, and when he sat down to eat both he and the table floated away. Parents of Hereva relay this story to their young children as an incentive to finish their meal ("That plate is too heavy for the table. Better finish up more or the table won't be able to fly!")

There are legends that the sap of The Great Tree might be used to harvest Rea or create flying potions. Attempts to tap the tree to harvest the sap have failed because of the tree's remarkable healing powers. These failed attempts have resulted in spigots snapping in half (sometimes with great force), drill bits becoming lodged in the trunk and then expelled (sometimes with great force), or worse (often with great force). Komona has forbidden any future attempts as a safety precaution, but the lure is great and some have offered large rewards should the sap ever be extracted.

As the tree has matured the harvesting of wood from the tree has diminished. Items that are pure Komonan wood fetch ridiculous prices and most of them are sold exclusively at auction. It's rare to find items with pure Komonan wood, as all of the items sold or auctioned today are blends of Komonan and other cheaper wood. They're created using a Magmah spell that merges the woods together to create a blended material. Hippiah witches do not approve of this process. They believe merging wood together is an aberration of the original plant, and choose instead to purchase or fashion their own brooms and wands that don't use this process.
During the war

During the great war the city of Komona was shocked when the tree lifted both it and the surrounding city of Komona high up in the air. As it lifted it formed a sphere around Komona; protecting it from harm. Stranger still, as the devastation raged throughout the cities of Hereva the citizens of Komona noticed The Great Tree began to shake and writhe, as though each battle were somehow affecting the tree in some way. Over time several other cities began to float throughout Hereva, although none were as spectacular as Komona and the Great Tree. As the war spread so too did the magic from the great tree spread to all of the war-torn cities of Hereva, causing them to rise above the ground. Historians theorize this was The Great Tree trying to protect those cities from the ensuing wreckage. Many branches fell from The Great Tree during this time, and there are still sellers in the marketplace who fetch a premium for branches that fell during the great war.

Near the end of the war the members of Ah destroyed the remaining witches of Chaosah. When the last witch of Chaosah fell The Great Tree shuddered and the city of Komona began to tilt toward the ground. As the tree shuddered the Komonans saw the tree illuminated with a sickly glow, as though was losing its magical power. The shuddering grew into a frenzy until the tree erupted with a violent explosion that knocked one of its large branches clean off. The shock-wave of the eruption of Rea spread throughout Hereva and for a brief moment Hereva was inundated in Rea. The branch that snapped off flew upward. As it descended eyewitnesses noticed that it had grown its own roots and formed a shape similar to the great tree. The Great Tree faltered and as the Rea continued to flow from the tree the city of Komona descended from the sky. Ah realized that without Chaosah the balance of magic would not only destroy the great tree but would eventually rip apart Hereva itself. When Chaosah was restored The Great Tree returned to the sky. The site where the branch exploded from Great Tree healed, leaving only a tiny scar visible to the watchful eye. Eventually the branch, now a fully formed tree, came to rest in a curious orbit around The Great Tree. The branch became the satellite known as Kerberos, and the upper-class residents of Komona, eager to live in such an exclusive and special city, flocked to the newly formed city.

After The Great Tree ruptured there were stories of other branches of the Great Trees of Komona forming floating trees throughout Hereva. Zombiah has used several of these smaller trees for some of their flying machines. Hippiah has also been able to nurture these smaller trees into their own versions of The Great Tree. Magmah disavows these trees as illegitimate trees that are not from the direct lineage of the The Great Tree. Komona set up strict laws about which products may bear the name and insignia of The Great Tree of Komona. They routinely patrol the Komonan Market and regulate any and all products bearing these marks. Violators selling or purchasing counterfeit items bearing the mark of The Great Tree face stiff penalties.

Hippiah witches are careful to point out that only highly trained Hippiah witches can discern if merchandise bearing the "Genuine Great Tree of Komona" mark is correct, and state that the only thing you receive when purchasing the "authentic" merchandise is the ability to pay more for the same product. This is why Hippiah Witches are viewed with some suspicion at the Komonan Market.

**Great Tree of Komona Aspects**: Highly sought-after wood, Better floating than other wood, Controlled market for wood

### Kerberos

When Ah destroyed Chaosah The Great Tree shuddered from the release of Rea. One of the major limbs snapped off and was hurled from The Great Tree during a Rea explosion. The limb formed a separate spherical tree, which became the city of Kerberos. Kerberos floats as a satellite of Komona. Because of the rare and extreme circumstances around the formation of Kerberos (and because Komona itself was overcrowded) it was more valuable to live in Kerberos than Komona itself. Kerberos became an exclusive space for the ultra-rich to live and now exists as a magically-protected gated-community. Only those who know the secret "knock spell" or have been granted access at the gate may enter the community. So strong is the "knock spell" that even casual observers who fly by Kerberos can only see the opulent houses, but not the people of Kerberos (unless they choose to be seen).

**Kerberos Aspects**: Gated community, Secretive location for the rich and famous, Difficult to enter without the knock spell, Luxurious

### Pepper's House

Pepper and her cat Carrot live in a house overlooking the forest near the village of "Squirrel's End". The house is a strange melding of one of the trees in the forest with conventional building materials. It is unclear whether Pepper's house is magically entwined with the tree or if the tree grew inside and around her house. Perhaps both, as the tree continues growing inside the house. This could also be attributed to Pepper's own Hippiah magic and one of her out-of-control potions.

Outside of the house is a mailbox with the number "33" on it, and a sign: "WARNING - WITCH PROPERTY". It's doubtful anyone has ever read this sign as Pepper sets countless traps in the forest surrounding her house. The traps are effective and have kept many of the dangers of the forest and the idly curious from disturbing Pepper and her work. Sometimes she remembers to release those caught in her traps, but even invited guests have fallen victim to her cleverness when Pepper forgets to deactivate the traps.

(Some might ask "Well, what's with the mailbox if nobody has seen the sign?" The Hereva Postal Service has already anticipated that question and has come up with a solution. Magic. It's quite complicated and requires an awful lot of effort to get the mail to Pepper's house at great expense but it beats having to send out a search party for each of the postal workers stuck in the various traps surrounding the house. That's the level of service you can expect from the Hereva Postal Service. It's even in their motto: "Neither snow nor rain nor heat nor magical trap nor potion nor Chaosah burial site nor Cumin nor gloom of night nor whatever stays these couriers from the swift completion of their appointed rounds.")

The other witches of Chaosah (Thyme, Cumin, and Cayenne) also live in the house with Pepper. They have adorned the house with various books, potions, and even a telescope.

Pepper's house is the original house of Chaosah. Chicory had the original version of the house built, but over time it has seen many different magical accidents that have required it to be rebuilt. The latest of these magical accidents happened when Pepper tried bringing a Unity tree back from a micro dimension. Other instances include similar experiments that were brought back from micro dimensions (so many that Thyme wrote in bold letters for folks to not bring their back experiments from ANY micro dimension). There are many other instances of Chaosah experiments destroying the house that it has almost become routine for Chaosah witches to disintegrate the house and rebuild it. One might wonder why Chaosah witches don't just create more additions to the house or create another area for school-related activities. Eventually Chaosah did create another school building, but that one was lost in a freak accident involving a micro dimension and a Chaosah black hole. Fortunately all of the Chaosah students were quick enough to escape the building and watched in amazement as the building was enveloped in the collapse of the micro dimension and the black hole. Other such buildings met similar ends. It's for this reason that Chaosah only has the original house of Chicory left. This lead to the following wisdom: "A true witch of Chaosah doesn't put all of their Ko into real estate".

The largest Chaosah school was destroyed in the great war. So great and so complete was the destruction of the building that it has taken the Chaosah witches years to recover anything of value from it. Every so often Cayenne, Thyme, and Cumin will scour the spot where it once stood; the last great bastion of Chaosah's power. Having to return to Chicory's home was a major defeat for the Chaosah Witches. It's hard to blame them. Few would consider Chicory's house a seat of power, especially when the chances of turning on the faucet and having either hot water, cold water, tomato soup, or a demon's tentacle come spewing out don't instill the kind of dominance and fear that the Chaosah Witches wish to portray. (The days when the faucets pour out apple cider, though, are considered quite the treat.)

Flora and fauna around Pepper's house tend to exhibit strange behaviors. At one point the ants near her house were able to understand the whole of mathematics as the result of a potion tossed from Pepper's house. It's unclear how far the ants have taken this temporary burst of knowledge, but the evidence of a space program suggests that ant civilization has at least hit the space age.

Underneath the house are a series of tunnels and catacombs which serve as additional storage for the house above. Many potions, books, and other magical paraphernalia are housed upon the aching shelves. Since this was the original house of Chicory there are no doubt areas that have yet to be explored, or other secret passages that are yet to be uncovered. One curious witch discovered a hidden passageway that lead to one of Chicory's secret laboratories. When this witch returned to show other witches about her amazing discovery of an untouched laboratory of Chicory the laboratory wasn't there. In its place was a hastily scribbled note that simply read "Gone". Underneath that was a harshly worded post-script to quit being so nosy and to mind one's own business. The laboratory hasn't been seen since.

Pepper is the current owner of the house. The reasons for this are related to resurrected entities owning property in Hereva. Qualicity and The Technologists Union are the only places where resurrected entities may own property. This posed a problem for Chaosah after the war, as all of the living members of Chaosah had been resurrected. They were granted an extension based on the extenuating circumstances of their death (their role in The Great War). This meant that whomever was designated their heir would be the legal owner of the house. This meant that Pepper became the legal owner of the Chaosah house. Pepper took this to mean that the house was hers, and began decorating it as her own. The Chaosah Witches tolerated this for a while (after all, she prevented them from completely losing the house) but as Pepper began having larger plans for the house they asserted that her changes went too far, and that the house should be left as it was. Cleaning the living spaces and putting up trinkets is one thing, but changing the color of the walls that Chicory painted herself was simply intolerable. The witches asserted themselves more and more into Pepper's living space until she couldn't take it anymore and complained to her friend Saffron. New agreements were drawn up so that everyone could peacefully live in the house together. This hearkens back to the wisdom of Chicory who, after realizing that more and more of her students were now living in her house, said "A true witch of Chaosah under my roof is like a mouse: seldom seen and barely heard".

**Pepper's House Aspects**: Secret location, Old house of Chaosah Witches, Chaosah's buried secrets

### Squirrel's End

Squirrel's End is a village with a population of around 2,000 villagers. The village is a fertile section of the thick and dense forest that was selected by settlers who wanted a quiet area to farm and live. The forest provided ample lumber for building and over time the village grew. The village is not wealthy, but concentrates its labor on being self-sufficient and content with what the land provides them. It is part of the lands of King Acren, though his waning influence means most villagers only know his name from the sporadic taxes that his kingdom collects. The villagers work in the fields of Squirrel's End using diluted forms of Hippiah magic. The village is surrounded by the forest of Squirrel's End, which houses many dangerous creatures. Few villagers head into the forest, preferring to use their trap-making skills to protect them from any creatures that might dare to wander into the village.

The folk colors of the village are a red tartan (which Pepper wears on her arms and legs) and white. Many villagers wear these colors to signify their pride in the hard labor that keeps the village alive and thriving in such adverse conditions.

Pepper was raised at the Little Acorns Orphanage in Squirrel's End. It was here that she was exposed to Hippiah Magic and showed her magical aptitude. At present Pepper is the most famous (or infamous) person to have been raised at Little Acorns. The picture of her winning the first potion contest is displayed in a small case near the entrance of the orphanage.

**Squirrel's End Aspects**: Hard-working farmers, Surrounded by forests

### Tenebrum

Little is known about the mountain known as Tenebrum or why it is considered sacred to the school of Chaosah. What is known is how much they protect it from outsiders. A curious soul once followed the Chaosah Witches, keeping out of sight as much as possible. He was astonished to find a mountain where no mountain had been before (or at least he didn't recollect there being a mountain there). As he followed them he started to notice that the mountain began to shift beneath him until it suddenly vanished. He suddenly felt disoriented and formless the darkness enveloped him. When he awoke he found himself in the forest near a small inn with two note attached to him. The first read "Head to the inn. Tell the innkeeper that Thyme sent you. You can rest for the night". The second note was more terse. It read "Next time you venture to Tenebrum we won't be so kind". The curious soul did as instructed, and found that the innkeeper had standing instructions from Thyme (head of the Chaosah Witches) to house anyone who had found this strange mountain. Apparently he wasn't the first, though the innkeeper said he never saw the same face twice. From there the curious soul vowed never to follow Chaosah Witches again, and warned anyone he could of the sacred mountain of Tenebrum.

Several books about Chaosah mention Tenebrum a total of three times, and two of them are a warning for outsiders never to go there. The third posits that this mountain is either part of some dimensional rift on Hereva's ever-shifting geometry, appearing only to those who know enough Chaosah Magic to make it manifest. It also posits that Tenebrum is not actually on Hereva itself, but in a parallel dimension known only to Chaosah Witches. From there the book joins the chorus of the other books in warning non-Chaosah witches to avoid this at all costs, citing the same notes as the curious soul.

**Tenebrum Aspects**: Sacred mountain of Chaosah, Magically protected

### Qualicity

Qualicity is a large industrial city standing alone in the middle of a desert within the Technologist-Union. Qualicity is best known for manufacturing a variety of objects. Many mechanical gizmos and gadgets find their way into Hereva from Qualicity, though certain areas of Hereva are less welcoming of such mechanical contraptions. Qualicity is renowned for their quality mechanical work. Their ability to turn inanimate objects into lifelike beings is uncanny, which can be uncomfortable for the few that are not entranced by their mechanical beauty. Many clockwork and mechanical automatons work in Qualicity's vast manufacturing centers while most of the population of Qualicity either maintain the mechanical constructs or perform other maintenance throughout the city.

Qualicity stands upon giant pillars dug out from the surrounding ground. It sits inside this dug-out cavern inside of this deserted land. This was related to an accident from one of the earliest machines made in Qualicity. These machines (nicknamed "bugs" because of their shape) were designed to retrieve minerals from the soil surrounding Qualicity. Unfortunately there was an error in the machines' programming that neglected to instruct them to perform their mining tasks far outside of the city. The machines were so quick and efficient in their tasks that the surrounding area of Qualicity was stripped clean and Qualicity was in danger of disappearing into the chasm. After several emergency meetings the engineers at Qualicity reinforced the ground beneath Qualicity. Plans to fill in the hole were scuttled when several of the geologists confirmed that filling the hole surrounding Qualicity would be quite expensive and take major effort. The King, chief engineer of the project, declared that this hole was actually a feature, since it protected Qualicity from ground-based attack. Thus the phrase "it's a feature, not the fault of bugs" was coined.

Zombiah is the predominant magical school in Qualicity. This magical system concerns itself with animating non-living things, and formerly-living things. During periods of extreme scarcity Qualicity has staffed the factories with formerly living beings (with the consent of the spirits inhabiting those bodies, who felt a sense of duty to help wherever they could). As part of their treaty with Ah the Technologist-Union agreed to curtail their use of Zombiah as practitioners of Ah find the binding of spirits anathema to the spirit's true form. Zombiah is still practiced, but its use on living beings was curtailed per the terms of the treaty. The exact terms of the treaty are sealed, visible only to select members via a magical lock-box. Zombiah limits their practice of re-animation to non-human beings, and reanimates living beings in the presence of an adept of Ah. Whether this practice is a result of specific terms in the treaty is unclear. (Few wish to test their theories about the exact wording of the treaty lest the bonds of the treaty magically break and the terrible war resumes.) What is clear is Qualicity is using Zombiah magic to create and distribute complex machinery and clockwork beings, and flourishing.

Coriander is the only daughter of the previous King and Queen of Qualicity. She is the most famous practitioner of Zombiah magic. Coriander lives in a castle near the Technologist-Union headquarters where she tinkers away in her laboratory. She is constantly working on and perfecting various machines, and prefers making things over her duties as the monarch of Qualicity. Her coronation took place in The Cathedral.

**Qualicity Aspects**: Home of Zombiah magic and the Technologist's Union, Technological wonders


### The Cathedral

The Cathedral sits inside of the city of Qualicity. It is a beautiful structure, and has been used as the place where Qualicity's royalty is crowned. The stained glass is set in such a way that when the moons of Hereva are aligned a certain way the cathedral shines with brilliant light. It is considered a good omen for monarchs to be crowned during this time, which can lead to long periods where a monarch will be made to wait until the moons align. 

#### The name of the cathedral

Citizens of Qualicity call the cathedral simply "The Cathedral". Folks outside of Qualicity may call it the Qualicity Cathedral or the Zombiah Cathedral, but its proper name has been lost to history. Shortly after the cathedral was built one of the reigning monarchs decreed that the cathedral should be named after him. He was an inept ruler who spent most of his two-month rule renaming things to suit his whims. His staff quickly tired of his ridiculous demands and the paperwork created for enacting his renaming spree. When the king asked about the status of renaming the cathedral his staff replied with various excuses, ranging from "Oh, I'll get right on that!" to "I haven't heard back from that department. Let me look into it." Eventually it became an open secret that the king's demands were being hampered by his staff's inability or unwillingness to comply with his demands. The excuses went from the plausible to the ridiculous: "I think that department only works during the two-out-of-three moon alignments" was the most notable of the recorded excuses. Unfortunately the only remaining records of this king are the memos of his demands, and the resulting excuses. The official register of monarchs went missing prior to the coronation of this king, and reappeared well into the reign of King Rambutan. When they opened the register to record King Rambutan's reign they noticed something peculiar where this king's two-month reign should have been recorded. In the broad list of neatly and carefully written calligraphy marking the rise and passing of the previous monarchs is the hasty scribbling: "King What's-His-Name". Next to the dates of his reign is a space for notable achievements and the reason for his departure from the throne. In the space for the reason for his departure is a single and cryptic word: "Gone". No other records of this king's reign exists. This gave rise to the popular children's game "King of the hill" and the rhyme sung whenever someone reaches the top of the hill: "Long may you reign, Like good ol' What's-His-Name". Naturally the monarchs of Qualicity don't find much humor in this.

**Cathedral Aspects**: Place where monarchs of Qualicity are crowned, Sacred space

### The Temples of Ah

The Temples of Ah are located in the land of the setting moons, where the three moons of Hereva enter "Pink Moon" before settling into the horizon. Under the leadership of Wasabi the Temples of Ah have grown into a single monolithic castle and village. The Temples of Ah are fortified with walls, gatehouses, and battlements. Horns adorn the ornate roofs of the buildings, both for aesthetic purposes and for protecting from airborne attacks.

The Temples of Ah serve both as centers of worship and as the central nervous system for the school of Ah. The main temple serves both as a school for trainees and adepts, and the storehouse and main processing area of the information that Ah gathers about the rest of Hereva. Huge libraries of information exist within the temple walls with reports from the wandering adepts of Ah. It is also here that Wasabi has her combined office and living quarters so she may be ever watchful and ever present over the vast information flow. Throughout the rest of the temple complex are an array of different buildings serving as dormitories for students, information processing centers, auxiliary libraries, and places to meditate and communicate with the spirits. Dragons can be seen patrolling the area and resting in guarded poses.

Few outsiders have been inside of the Temples of Ah. Visitors are given special green robes to wear while in the temple. It is rare to see anyone who has not taken the oaths of Ah to wear the traditional red robes and bone headpieces of Ah. 

Prior to Wasabi there were several humble structures scattered throughout the land of the setting moons. Ah fashioned these older temples atop natural hyperboloid-like structures with long winding stair-cases ascending to the platform. Water falls from the edges of these magnificent structures and pools beneath them into teal-colored waters. The older temples around the perimeter of Ah have been abandoned in favor of the centralized walled structure. 
 
The land of the setting moons is also the final resting place for many dragons of Hereva who came to this place seeking comfort and peace long before Ah built their Temples. The dragons pay little heed to the Ah's practitioners and Ah leaves the dragons in peace. Ah has a beautiful ceremony for the passing of these great creatures, but few outsiders have witnessed it. The ceremony (as explained) calls the spirit of the dragon forth from the body and guides it soaring up to the heavens. The death of the dragon and the ceremony consumes the body of the dragon in a fiery vortex, leaving only the bones of the dragon in its place. Members of Ah use the bones to fashion their Temples and fashion them into ornamental jewelry to wear in their hair. This creates a curious juxtaposition between the natural beauty of the area and the various draconic remains, both in the architecture and scattered throughout the land. Members of Ah feel that the bones of such magnificent beasts are a reminder of the body's imprisonment of the spirit, and reinforces the belief that the spirit should be separated from the body when death comes to consume the body. 

**Temple of Ah Aspects**: Secretive headquarters of Ah, Dragon graveyard, Fortress and seat of power for Wasabi, Abandoned temples

### The School of Hippiah Magic

The School of Hippiah is a modest building on the outskirts of the village of Squirrel's End. The school is a "U" shaped set of buildings that have become connected over time. One of the buildings is a combination kitchen and food storage area (no doubt to prepare the tasty produce the school's students grow). The remaining buildings serve as living space,offices, and classrooms where students learn the finer points of Hippiah magic. 

The school resides next to a stream fed by a waterfall just above the school. The stream powers a mill beside the school, which is used to crush and process grain for breads and cereals. The stream is a favorite drinking spot of several animals that graze along the banks. Atop the greenhouse is a large reservoir that collects and stores water used for drinking and baking, and also for watering plants in the greenhouse. Hippiah magic prefers to mingle their buildings among the roots of trees, and the oldest tower is built beneath the roots of a one such tree. Moss grows on the roof of the buildings. They are simple and direct: the opulence of Hippiah is in the soil and fields, not in their architecture.

Students from all over Hereva can be found at the school. The school is renowned for accepting most of the students who apply, though a demonstration of a modest amount of magical aptitude is required for entry. Students are required to purchase Hippiah robes and garments as part of their uniform, which can be had for relatively little Ko. Some students have even received their robes and garments in exchange for a few days worth of the produce they help create. While the school accepts all students for admission they can be cruel and demeaning to those who do not meet their exacting standards for spell usage. Hippiah magic can be learned by all folks in Hereva, so the school focuses on perfecting the use of Hippiah magic. This can lead to taunting and teasing of those who do not live up to the standards of their teachers and classmates. It can also cause students to feel as though they don't belong. Several teachers, including beloved instructor Millet, tried to change this belief, but the push for perfectionism endures.

Many of the classes are held outdoors (weather permitting) where students can practice their growth spells in actual soil. During inclement weather students can be found tending the greenhouses, learning various cooking techniques, or perfecting their spells.

**School of Hippiah Magic Aspects**: Training area for Hippiah Magic, Open Admission

### The School of Aquah Magic

The School of Aquah is a modest series of underwater caves located in the deepest parts of Hereva's waters. It's secluded from Aquah's main cities and towns in the hopes that a quieter and more placid location can aid in the students learning. This is also because Aquah's lessons are taught via telepathy and other non-verbal cues, so concentration is essential. Phosphorescent moss adorns the walls, which complements the other luminescent plants that illuminate the caves. Several of the caves have been turned into classroom areas, while other areas serve as makeshift offices for the staff. The school acts as a communal area where students and faculty eat, sleep, and learn together. Students are free to leave the school whenever they feel they have learned enough, and the number of students can vary from a handful to several dozen at a time.

Students are required to pass a simple entrance exam to determine if they are ready for the challenges ahead. The exam varies depending on who is proctoring it, and can range from a simple memorization and recitation exercises over telepathy to more rigorous examination. The school becomes more selective as enrollment increases, with some years having the students perform complex aquatic manipulations to suss out any natural aptitude. Usually such tasks are reserved for upper-level students. This has caused many students to train rigorously to be admitted, only to realize that they have outpaced the first few years of school training. Such practices have resulted in many students being bored for their first years of schooling. Some students have begun teaching the lower-levels of these classes, which frees up the instructors to teach the higher level classes. This has lead to strange situations where students arrive at the school and begin teaching before they've taken their first class.

It is rare for someone outside of the Aquah community to be admitted to The School of Aquah. Exceptions have been made for especially gifted students, but the examinations for those students are quite challenging. The first challenge is learning how to breathe underwater, which can prove difficult for those who are used to breathing air. Those who can learn to be comfortable under water are then subjected to a series of examinations to test nonverbal communication, telepathy, and endurance. These tests are harsh, and only a few students have survived the examination process.

Many lessons are held as telepathic dialogues between teacher and students. An instructor will pose a question about how one might perform a certain task and students provide their answers. This continues with each student demonstrating their answer to the satisfaction of the instructor and the other students. All of Aquah's traditions and spells are transmitted in this way and have been for generations of students. It wasn't until recently that Spirulina discovered a cache of written spells and documents which were put away because one instructor believed that students learned better via telepathic recitation. This has lead to a renaissance of spells that were previously forgotten or not considered as important being reintroduced into the curriculum, but has also shown some of the weaknesses in the traditional teaching methods.

The communal nature of the Aquah school provides areas for sleeping, eating, exercise, sparring, and meditation. There are also areas around the school which are near hot jets of water used for relaxation and exfoliation. Days are well-regimented, with most of the school waking, sleeping, and eating around the same time. Aquah used to be very rigid with the schedule, but lately they have become more lax. Students may be permitted to eat at different times if they receive such permission from the head of the school. Some of the older students are given more freedom, and are allowed independent study sessions near the school. There is some debate among the instructors on whether this is beneficial for the students, and yearn for the more strict and disciplined approaches of the past.

**School of Aquah Aspects**: Secretive school of Aquah magic, FIXME


### The Hereva Cartography Company

The Hereva Cartography Company has its headquarters in Komona. The company offers self-updating maps for customers at a modest subscription price. The company sends representatives out to map the ever-changing geography of Hereva. Those representatives scout around to see what has changed and report their information via their proprietary magic-mapping process. The company boasts their maps are the highest accuracy possible, and their service is a bargain at twice the price. However the process takes time and there have been instances where the terrain changes faster than the map. Some have countered that the maps generated by the Hereva Cartography Company offer little that a little knowledge of Hereva geography and patterns couldn't do better. Naturally the company does not share this assessment, and claims the only way to be sure is to subscribe to their service.

Careful observers may note that the maps provided by the Hereva Cartography Company (HCC) show Hereva is slowly expanding and creating new territory every few months. Unfortunately the older maps are overwritten with the latest updates, so it's difficult to compare them over time (and HCC does not permit the saving of old maps). The official HCC statement on the expansion of Hereva is that it is not happening (and remember to keep up-to-date with the latest changes via our service). But some representatives, under conditions of anonymity and after a few strong libations, have countered this statement. Whether this is true (or just a way to get free drinks) is unclear at this time.

**Hereva Cartography Company Aspects**: One of the mapping companies in Hereva, Constantly mapping Hereva

# Playing in Hereva

Pepper&Carrot is designed to allow exploration in the world of Hereva. It can focus on several characters from one particular school, or it can have members of all of the different schools working together to overcome a common foe. This section will guide you on how to play this game and what to expect in the world of Hereva.

## Adventure Themes

There are several themes in the Pepper&Carrot comic:

* Tradition: The young witches in each of the schools question the traditions they've been given. Why do we do what we do, and is it still useful? We see this in Pepper's interactions with her godmothers, but even the rest of the young witches feel trapped and constrained by the expectations of their school and society. Placing the characters into adventures where they work against their school's traditions and societal norms will capture the spirit of a Pepper&Carrot comic.
* Exploration: Pepper is often seen exploring various areas of Hereva. Whether she's using a book as a guide for traversing a ruined castle or encountering fairies in the forest there is always the chance of adventure and exploration. Uncovering new places and situations in Hereva is common. Even something like a mundane trip to Komona's markets can lead to something magical.
* Fun: Pepper&Carrot is intended to be light-hearted and fun. Sure, there may be dangers lurking around the bend, but that danger doesn't have to be fought with weapons. Sometimes some simple cunning or lateral thinking can defeat the most dangerous of foes.

There are also several sub-themes in the comics about waste and environmentalism, dealing with failure and expectations, and realizing that our actions have consequences that are sometimes beyond our immediate comprehension. 

Note that not every adventure needs to incorporate any or all of these themes. We recommend keeping the light-hearted aspects of Pepper&Carrot but you are welcome to have adventures that are dark and ominous if everyone at your table wants dark and ominous Pepper&Carrot adventures. Having dark and ominous adventures peppered (no pun intended) with light-hearted comedic events may be an excellent way to show contrasts in the world of Hereva. Play with this to find out what works best for you and your group.

## Adventure Seeds

These are some suggested adventures you could have within the world of Hereva. They are presented by which school the adventure might highlight in the adventure. Some will resonate better if one of the characters is from that school, but can be used with characters from any of the schools.

### Chaosah Adventure Seeds

* **Chicory Returns**: Chicory breaks her self-imposed exile from her pocket universe to visit our universe. Something has happened which she believes could jeopardize not only our universe but every universe out there. The witches will need to work together to determine what fate could make Chicory break her silence.
* **Chaosah Demons**: The treaty that Ah brokered with Chaosah prevents Chaosah from bringing in demons from other worlds (a rule that was partially broken in The Birthday Party episode). Unfortunately there continues to be an imbalance in the magic of Hereva. Chaosah believes they need to bring some of these creatures into Hereva or all of the magic in Hereva will be lost. The witches must convince Wasabi (the leader of Ah) that this is necessary, and prevent her from interfering if she doesn't agree. (This could also be a trick by Thyme to bring the demons back for another go at bringing Chaosah back to its full power).

### Chaosah Buries their Failures

The Witches of Chaosah have a long tradition of burying their failures. But not all failures are complete failures, and some of them are not as "inert" as one would like.

* Potions are leaking out and combining in unpredictable ways. It's up to the characters to find the source of the problem and render the potions inert before the potions combine in even more terrible ways.
* Too much Rea in one area attracts folks looking to harvest the Rea for their own purposes. The wrong folks getting their hands on that much Rea could have dire consequences. The witches must patrol Hereva to find the strongest pockets of Rea and properly dispose of these failed experiments.
* In a fit of cleaning Cumin buried some magical artifacts that should not have been buried. Unfortunately she doesn't remember where she buried them. Normally this wouldn't be a problem, except Thyme is waiting on them and doesn't realize they're missing. Help!
* After some flooding the layer of ground that covered one of the failures eroded away and one of the failures has surfaced. The notes for one of Chicory's experiments have surfaced. This is a legendary experiment in the school of Chaosah. It's said that Chicory was close to a breakthrough before she abandoned the experiment. Finding this experiment may uncover ways to take Chaosah magic closer to the magical fundamentals of Hereva. It may also uncover the truth behind why Chicory buried such a monumental experiment. 

### Squirrel's End Adventure Seeds

* Pepper has forgotten to disarm her traps again. The witches will need to head out to disarm Pepper's clever traps. Hope they don't miss one! (This scenario will require some GM creativity. Some sample traps are provided to give you some ideas on the flavor of traps but we encourage you to make your own!)

<!--%FIXME: (List of traps from Pepper's Birthday Party)-->

### Zombiah Adventure Seeds

* Some late-night programming caused one of Coriander's constructs to break it's control programming and escape. Coriander needs help finding the construct before it corrupts the rest of its programming and does any more damage.
* Coriander is worried that someone or something is trying to remove her from the throne (both figuratively and literally). The witches need to discover who might be behind these sinister plots.
* Zombiah is forbidden from using their powers of reanimation on the dead (save for magical contests) but a faction of Zombiah practitioners bristles at this agreement. They have been secretly reanimating followers and are planning to overthrow Ah and reclaim the right to practice their magic to the fullest. It's up to the witches to locate this group and convince them to stop before Ah takes notice and destroys Zombiah like it did with Chaosah.

### Ah Adventure Seeds

* A dying dragon comes to the temples of Ah for its final resting place. During conversations with the dragon the witches learn of unfinished business the dragon wanted to accomplish. The witches swear to help the dragon with his dying request.
* Ah has been gathering intelligence about all of the schools for generations, but refuses to share that knowledge with the other schools. Unfortunately Ah has gathered data that could help one of the schools, but refuses to share it. It's up to the witches of that school to sneak in and find that information in Ah's archives before it's too late.
* If information is power then Wasabi is the most powerful witch in all of Hereva. She believes that Hereva would be best with complete order, and sends her adepts to gather information and uses the spirits both for guidance and reconnaissance. It's up to the witches to learn of her plans and stop Wasabi before she remakes the world of Hereva into her own design.

### Hippiah Adventure Seeds

* One of the Hippiah witches read from a spell book labeled "Forbidden Spells. DO NOT READ!" and accidentally cast one of them on her plants. This has caused the plant to become sentient. Creating a sentient plant wouldn't normally be a bad thing, but unfortunately this plant believes that plant life should overtake all of Hereva, and is using Hippiah magic to grow an army of self-aware plants. It's up to the witches to defeat the this rogue plant and its plant army. 
* A magical blight is causing many of the plants and trees in the forests to wither and die. The blight continues to spread and if not stopped all of the plants in Hereva will be gone. It's up to the witches to stop the blight, learn who or what caused it, and restore the plants that were lost.
* Pepper's rise to prominence as a Chaosah witch has caused some friction in Hippiah. They believe that she has used Hippiah magic in conjunction with Chaosah magic and diluted the purity of Hippiah magic. Some in Hippiah believe that Pepper must be stopped lest Hippiah magic become more entangled with Chaosah magic. It's up to the witches to determine what is going on. 

### Aquah Adventure Seeds

* Some errant Chaosah potions have leached into the water table and have created a small Chaosah Black Hole at the bottom of the sea. It's up to the witches to fix the hole before all of Hereva's water disappears into it.
* A faction of Aquah believes that they can gain more dominance over the rest of Hereva by warming the frozen parts of Hereva. This will lead to more water which will expand Aquah's territory. Unfortunately this is also wreaking havoc on the climate of Hereva and causing major issues with the weather. It's up to the witches to restore the balance before it's too late.
* A strong current swept away a sacred artifact from one of the temples. Usually such currents don't disturb much, but this current is a *mysterious* current. It's up to the witches to retrieve it and determine who or what is behind this.

### Magmah Adventure Seeds

* Someone has tampered with the annual Magmah fireworks display. It's up to the witches to find it and stop the saboteur before things go horribly wrong.
* The honor of Magmah is threatened when a rival group of chefs declare that Magmah's recipes were stolen from them. It's up to the witches to find out the truth, and possibly be involved in a cooking contest that will settle the matter once and for all of who's cuisine reigns supreme.
* Magmah is in danger of fracturing because of the rise of Saffron as the face of Magmah. Several witches believe that she is overshadowing the rest of the school and are working to ruin her. Saffron has noticed more attempts to get her out of the limelight, both figuratively and literally. It's up to the witches to determine who is behind this and save Saffron before it's too late.

### Komona adventure Seeds

* There is a rumor that a witch is selling illegal potions to various folks (including a swordsman). The witches are tasked with finding out who is responsible.
* One of the merchants in Komona Square is accused of being a spy working to undermine the city of Komona, but nobody knows who they are working for. The witches are tasked with discretely following this spy and determine who, or what they are working for and prevent them from ruining the city.
* Mayor Bramble is running for re-election for the city of Komona. His opponent, though, is a mysterious entity from the floating community of Cerberus. The campaign is getting progressively more mean with this challenger doing questionable and immoral things to ruin Mayor Bramble's chances of winning. Mayor Bramble isn't a perfect mayor but this new candidate seems far, far worse. It's up to the witches to find out who this challenger is and help Mayor Bramble win the election.


### Potion Challenges

Not only have the witches been summoned for a potion challenge, they also need to find the ingredients to make the potions!
* One of the ingredients can be a "secret ingredient" that is revealed during the potion contest.
* The secret ingredient for the potion challenge is located in a hidden location. The witches must work together to locate where the ingredient is, and (more importantly) what the ingredient is. 
* Did we mention there's a time limit? Of course there's a time limit.
* To find the ingredient the witches need to use their available resources. Naturally not all of these resources will be cooperative.
* Ideally this will spawn cooperation between the witches but certain groups may wish to season with a little more competition between the witches / factions. The group should be working toward finding the secret ingredient but the witches may have different ideas of how the ingredient should be used.


### Mapping

* One of the witches is hired by the Hereva Cartography Company to explore unmapped territory (under strict non-disclosure agreement). Unfortunately there's a reason why this territory is unmapped, and it will take the talents of several witches to explore this area and complete the task.

### The Caves of Hereva

* Thyme disappears at night to head to the caves of Hereva. One of these caves has a public crystal ball that can be used to look at the rest of the Hereva network, but Thyme has also been heading to other caves throughout Hereva. Other witches have been seen lurking near these caves. What sorts of mysteries might be uncovered if the witches follow Thyme on one of these nights, and what happens when they find out?

### Books Are Great

* Many of the old castles and fortresses in Hereva have been explored and documented by intrepid explorers, but a handful of unexplored castles remain. The Hereva Adventurers Guild recently had a feud among its membership and don't have the resources to explore the rest of these fortresses and castles. The remaining members of the Hereva Adventurers Guild (HAG) have posted fliers in Komona's market looking to pay adventurers to explore these remaining places in order to document them. This documentation, if it's up to the publisher's exacting standards, will be collated into books about these locations which are published so others can enjoy them. Witches looking for some extra Ko or exploring the unknown could explore these areas. But there is a newly formed rival group, the Hereva Adventuring Society for Teachable Excursions Network (HASTEN) which is also exploring these areas, and wants to be the first to publish the definitive guides. Can the witches find what they need, take good enough notes, and return before HASTEN explorers get there first?

# Other Folks in Hereva
<!--FIXME: Need an introduction sentence-->

*Hereva has other people and entities in it.*

# Making Hereva Your Own

*Hereva is more than the sum of what is in this book and the Pepper&Carrot comics; it's a world of magic that can be adapted as part of an existing campaign or explored as a sandbox campaign. Here are some guidelines on how to adapt Hereva to make it your own*

## Hereva and Dimensions

Hereva is a world where different dimensions are part of its reality. There's a dimension where Chaosah Demons live, waiting to be summoned for nefarious purposes, or just to be part of afternoon tea. There are otherworldly horrors that lurk, waiting for the opportune alignment of Hereva's three moons to create a rift in order to allow them to enter. Chaosah magic can also create micro-dimensions and pocket universes to seal themselves off fro the rest of the world. Dimensions can be a great way to not only allow characters to hop between worlds, but also allow things from other worlds to enter Hereva.

## Hereva and Our World

Hereva has a peculiar relationship with our world. Many things in the world of Hereva are similar to things that we have in our world (or had in our world's past). This can be a great way to introduce pieces of our world into Hereva. Want to have an easy way for characters to get a list of tasks to do? The Crystal Ball network is similar to the World-Wide Web network that we use as part of the Internet. Perhaps there's a job board that pays interested parties to perform certain tasks. The Witches of Chaosah and Ah monitor other dimensions and are keenly aware of the existence of other worlds and their technologies. Perhaps they're not the only ones, and maybe there's a cottage industry of folks who scan other dimensions looking for inventions and other things to bring into their own world. Maybe the people of Hereva were transported from our own world into Hereva as part of some magical experiment that the dragons performed? What might happen if the dragons perform that experiment again?

## Placing Hereva in Your World

The World of Hereva doesn't come with a predefined map. This is on purpose. The lack of a map allows you to place locations of Hereva into your own world. It also allows Hereva to expand to include more locations as needed. The locations are loosely defined so if you want to place areas like Squirrel's End and The Forest of Squirrel's End in the outskirts of a city you can. Distances in Hereva are also not explicitly defined so you can make journeys between locations as interesting or as quick as you choose. There's also nothing stopping you from adding your locations into Hereva; the planet is constantly evolving and changing. Most locations have been mapped by one of Hereva's cartographers but there could be new locations being added all of the time. They could also be locations that have eluded the expert cartographers because of how they choose their routes. A new, previously unexplored location would be a big deal for the cartographers and they'd love any help they can get in mapping it. And if it contains any old ruins or cities that were previously unknown the adventurers guild would need someone to explore it. 

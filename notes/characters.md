# Characters

## Pepper

Pepper is a 14 year old orphan girl. She lives in the forests outside of the small farming village of Squirrel's End. She lives with her constant companion and familiar Carrot (so named for both his orange fur and for being found in a bushel of carrots). Her other companions are the remaining members of the magic school of Chaosah, whom she refers to as her godmothers. They tutor in the ways of Chaosah magic (the most primal form of magic in Hereva) guiding her on her path to becoming a "True Witch of Chaosah". This unfortunately has double-meaning for Pepper as she is not totally sure what a true witch of Chaosah is, and what she will become. She also doesn't trust her godmothers to be forthright with her on what a True Witch of Chaosah stands for.

Pepper began as a Hippiah witch, whic gives her the ability to cast Hippiah Spells and create Hippiah Potions at Mediocre (+0).

### Aspects:

#### High Concept:

"Powerful witch in training"

#### Trouble: 

Never enough Ko

#### School:

Chaosah

#### Other Aspects:

* "Not sure what a True Witch of Chaosah is"
* "There has to be a shortcut"

### Approaches:

* Careful: +0 Mediocre
* Clever: +2 Fair
* Flashy: +1 Average
* Forceful: +2 Fair
* Quick: +3 Good
* Sneaky: +1 Average

### Stunts:

(Note that Pepper gets one additional stunt here compared to the other Chaosah Witches. She does not believe she is a **True Witch of Chaosah**, and is unable to intimidate others who fear Chaosah witches.)

* Because I **set traps near my house** I get +2 when I **Cleverly Create Advantages** when **dealing with intruders in the forest of Squirrel's End**.
* Because I **create too many potions** once per game session I can **find the right potion at the right time** at my house.

## Thyme

Thyme is the oldest of the remaining witches of Chaosah. She leads the Chaosah Witches with fear and domination.  (Elaborate)

### Aspects:

#### High Concept:

Head witch of Chaosah

#### Trouble:

Aching joints and back

#### School
Chaosah

#### Other Aspects:

* "Chaosah shall retake it's rightful place as the most powerful school in Hereva"
* Taught by Chicory herself

### Approaches:

* Careful: +2 Fair
* Clever: +3 Good
* Flashy: +0 Mediocre
* Forceful: +1 Average
* Quick: -1 Poor
* Sneaky: +4 Great

### Stunts:

* Because I am **An elderly witch of Chaosah** once per game session I can remember something about the history of Chaosah or Hereva

## Cayenne

Cayenne is a grim enforcer for Chaosah and one of Pepper's Godmothers. (Elaborate)

### Aspects:

#### High Concept:

"Enforcer of Chaosah"

#### Trouble:

"Quick Temper"

#### School:
Chaosah

#### Other Aspects:

* Studied all of the books of Chaosah magic
* "My past is not who I am now"


### Approaches:

* Careful: +2 Fair
* Clever: +1 Average
* Flashy: +0 Mediocre
* Forceful: +4 Great
* Quick: +1 Average
* Sneaky: +3 Good

### Stunts:

* Because I am a **skilled spell teacher of Chaosah** I get +2 when I **Carefully Overcome Obstacles** when **someone has trouble with a Chaosah spell**.
* Because I am a **True Witch of Chaosah** once per session I can **intimidate someone who is afraid of Chaosah**.

## Cumin

Cumin is the teacher of potions for Chaosah and one of Pepper's Godmothers. She is the protégé of Thyme (which Thyme won't let Cumin forget with her constant bossing and nagging). 

### Aspects:

#### High Concept:

Potions Teacher of Chaosah 

#### Trouble:

"Thyme would rather have Cayenne as a protégé than me"

#### School:

Chaosah

#### Other Aspects:

FIXME: These aren't quite right, but are a first stab

* Deep knowledge of Hereva's plants
* I trust Thyme as far as I can throw her

## Approaches:

* Careful: +4 Great
* Clever: +1 Average
* Flashy: +1 Average
* Forceful: +0 Mediocre
* Quick: +2 Fair
* Sneaky: +3 Good

### Stunts:

* Because I am a **potions teacher of Chaosah** I get +2 when I **Quickly Create Advantages** when **creating potions**.

## Saffron

Saffron is a 14 year old girl from the floating city of Komona. She showed aptitude for magic at an early age, and after showing how easy it was to use magic to counterfeit Ko she was sent to the Magmah school to put her skills to good use. She runs a magic service "Saffron's Witchcraft" in the highest-rent districts of Komona. Her primary clientèle ranges from merchants looking to improve their wares and the alchemists of Zombiah researching materials. Saffron's business thrives; so much so that a steady queue of people form outside of her office looking for her advice. Even the Komonan mint looked to her to see how best to improve the Ko coins so they were harder to counterfeit. She has risen to prominence as one of the youngest entrepreneurs of Komona. The merchants of Komona are more than happy for Saffron's custom, as she tends to make large purchases of ingredients for her various potions and spells.

### Aspects:

#### High Concept:

Young Entrepreneurial witch of Magmah

#### Trouble:

"I should be running the school of Magmah"

#### School:

Magmah

#### Other Aspects:

* Everyone who is anyone knows who I am
* Never satisfied with what I have

### Approaches:

* Careful: +0 Mediocre
* Clever: +1 Average
* Flashy: +3 Good
* Forceful: +2 Fair
* Quick: +1 Average
* Sneaky: +2 Fair

### Stunts:

* Because I **own a popular magic consulting business** once per game session I can **divulge a hidden truth about one of my clients**.

## Coriander

Coriander is a 14 year girl from the engineering city of Qualicity. She is a witch of Zombiah, a magic school known for its ability to bind the spirit to non-living matter. This is used both in reanimating previously living beings as well as animating mechanical beings. Being an only child she quickly got bored and created herself a "sister" called Mensi. Her father and mother were the king and queen of Qualicity, but sadly they came to an unfortunate end when she was young. Coriander grew up with the knowledge that she would eventually be queen, but her interests lie more in making things. She can usually be found working on her pet projects; crafting new and ingenious inventions and pushing the boundaries of technology.

### Aspects:

#### High Concept:

Reluctant queen of Zombiah

#### Trouble:

Rulers of Zombiah can't be too cautious

#### School:

Zombiah

#### Other Aspects:

* "I've been tinkering on something like this"
* Still working out the bugs

### Approaches:

* Careful: +2 Fair
* Clever: +3 Good
* Flashy: +1 Average
* Forceful: +1 Average
* Quick: +2 Fair
* Sneaky: +0 Mediocre

### Stunts:

* Because I am the **Queen of The Technologists Union** once per game session I can **command one of my Technologists Union subjects** to help me.

## Shichimi

Shichimi is a 14 year old girl from the school of Ah. She spends most of her days wandering the lands of Hereva. At night she camps under the three setting moons of Ah. She is accompanied by her companion, the two-tailed fox Yuzu. She has a tendency to take things literally which can get her into trouble. She is high up in the ranks of Ah, but the ascetic demands of Wasabi, the leader of Ah, can test Shichimi's patience and resolve. Shichimi wanders more of Hereva than any of the other adepts of Ah, exploring and learning from the various cultures and traditions of the various magical schools of Ah.

### Aspects:

#### High Concept:

Wandering adept of Ah

#### Trouble:

Tempted by comfort

#### School:

Ah

#### Other Aspects:

* My camping gear has everything I need
* Takes things too literally

#### Approaches:

* Careful: +3 Good
* Clever: +2 Fair
* Flashy: +0 Mediocre
* Forceful: +1 Average
* Quick: +2 Fair
* Sneaky: +1 Average

#### Stunts:

* Because I am the **only student of Ah to participate in the magical contests** once per game session I can create a Mediocre spell or potion that is related to one of the other schools.


## Camomile

Camomile is a human / raccoon witch of Hippiah. She is one of the most powerful witches in Hippiah, partly because of her driven nature and partly because of her appearance. Many witches of Hippiah regard raccoons as a pest that demolish gardens, so Camomile's acceptance into the school caused some Hippiah witches to be concerned. Camomile always felt like she had to work extra hard to show her teachers that she was worthy of being called a Hippiah witch. She studied long hours into the night and did double the work of her fellow classmates in the fear that she would be expelled for not measuring up to the real and perceived higher standards of her teachers. This made her one of the top students of the class, which also brought attention from the other students, who teased her. This gave Camomile focus to be the best student she possibly could be, but also left her feeling like an impostor in her own school. Eventually a kind teacher named Millet took Camomile aside. Millet was a human / opossum witch, and she relayed her experiences to Camomile. Millet said she also felt she had to work harder to get the acceptance of her peers, but that it wasn't because she was inferior. She explained that people have a hard time with folks and experiences that are different from them. This all stems from fear of the unknown. Fear is a powerful motivator, and can cause others to try to find comfort in that fear, whether it is by belittling someone or worse. The problem, Millet said, is not with Camomile, but with the others. The only person she needed to impress was herself. Let the others figure it out for themselves. Camomile took this advice to heart and ignored the taunts and teasing of the other students. Many of the students eventually matured and realized that Camomile was a confident and capable witch. She continues to grow as a confident Hippiah witch, but her main drive is competition with herself, not the adoration of the other Hippiah students. When it was time to select a student to represent Hippiah at the Magic Contest she was overwhelmingly chosen by the other Hippiah students to represent them.

### Aspects:

#### High Concept:

Protector of Hippiah Traditions

#### Trouble:

Sensitive about being teased for being part raccoon

#### School:

Hippiah

#### Other Aspects:

* "I wonder what's in there"
* Rather be around plants than people

### Approaches:

* Careful: +1 Average
* Clever: +2 Fair
* Flashy: +0 Mediocre
* Forceful: +1 Average
* Quick: +3 Good
* Sneaky: +2 Fair

### Stunts:

* Because I am **part raccoon** once per session I can **sense danger** that others might not recognize.

## Spirulina

Spirulina is one of the students of Aquah's school of magic. She's the first person from Aquah to engage with the "dry ones" ever since The Great War. She was raised by her family to be curious and question traditions, so when word of a second magical contest leaked to the students of Aquah she volunteered to represent Aquah. She is fiercely loyal to Aquah, but regards the isolationist practices of Aquah as cowardice. "Shall we retreat into oblivion, or stand tall next to the dry ones?"

Her curiosity lead her not only to learn all of the oral traditions of Aquah but to question their validity. She is constantly revising spells, making them more potent or less costly in Rea. She exhausted her instructors by bombarding them with questions until they set up a separate study path for her to learn. She is also the first student to commit these traditions into a more permanent format, which shocked and dismayed her teachers. Aquah transmits their knowledge telepathically to their students, relying on memorization and rote learning for generations of teachers. Spirulina found this tedious, and wondered if there was a better way to convey this information. She worked out how a spell might be written down for others to read and use. She tested it out on several of the other students. At first the students were disgusted by the crude line drawings, but eventually they realized they could retain the information faster than by telepathy alone. Later Spirulina learned that she was not the first to discover this. One of the teachers in the long line of Aquah teachers felt that telepathy was the purest means of transmission, and abhorred the tradition of "writing" that the dry ones used. After questioning her instructors further she discovered an unused closet containing hundreds of magical artifacts that hadn't been taught over those many years. The teachers were stunned. Here were spells and techniques that had faded from memory. Here were corrections for misremembered spells, conversations about the efficacy of one spell technique over another, and fragments of ideas that were never explored further. Here were famous names from Aquah's past bickering over correct technique acting as living beings, and not as infallable and irrefutable icons. Spriulina's persistence reinvogorated Aquah's magic from dogmatic belief in what one's instructors taught them to one of curiosity. Naturally some instructors did not agree with this change, and tension inside the school is mounting between those who believe their ways of teaching by rote and memorization are correct and those who wish to explore the old magical artifacts.

She often wonders if being in Aquah schools has kept her from learning all she can about the magic in Hereva, and wants to learn more. She is proud of her Aquah heritage, but is an outspoken proponent of bringing Aquah back from its isolationist past. 

### Aspects:

#### High Concept:

Fiercely curious student of Aquah

#### Trouble:

"If it is carved in stone then we must erode and break the stone"

#### School:

Aquah

#### Other Aspects:

* There's more to Aquah magic than what we're taught telepathically
* Fish on dry land

### Approaches:

* Careful: +2 Fair
* Clever: +2 Fair
* Flashy: +0 Mediocre
* Forceful: +3 Good
* Quick: +1 Average
* Sneaky: +1 Average

### Stunts:

* Because I am a **strong witch of Aquah** once per game session I can telepathically send a short message to anyone who is not part of the School of Aquah.

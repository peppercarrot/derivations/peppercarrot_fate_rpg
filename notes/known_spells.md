# Known Spells / Potions

## Chaosah

### Pepper

* Potions
    * Potion of Flight (ep01)
    * Potion of growing/shrinking size for her and Carrot (ep05)
    * Laughing Potion (ep12)
    * Mega-hair-growth Potion (ep12)
    * "Bright-side" Potion (ep12)
    * Smoke Potion (ep12)
    * Stink bubbles (ep12)

* Summoning
    * Summoning three demons of Chaosah (book1, *Incantations for Demons of Chaosah*) (ep08)
    * Summoning a Chaosahn black hole (book7, *Gravitational fields*) (ep12)

* Spells
    * Micro dimension (ep24)
    * Deglamour (ep34)

## Magmah

### Saffron

* Potions
    * Potion of Poshness (ep06)

* Spells
    * Fire spell (ep22)

## Ah

### Shichimi

* Potions
    * Potion of Giant Monster (ep06) -- influences from Hippiah techniques

* Spells
    * Blinding light (ep22)
    * Spirit barrage (ep34)

## Zombiah

### Coriander

* Potions
    * Potion of Zombification (ep06)

* Summoning
    * Skeleton summoning (ep22)

## Aquah

### Spirulina

* Spells
    * Forceful ejection from water (ep21)
    * Water Tentacles (ep22)

## Hippiah

### Camomile

* Spells
    * Rapid Growth (ep18, ep22)

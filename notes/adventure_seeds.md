# Possible adventure seeds

## Chaosah Adventure Seeds

* Chicory Returns: Chicory breaks her self-imposed exile from her pocket universe to visit our universe. Something has happened which she believes could jeopardize not only our universe but every universe out there. The witches will need to work together to determine what fate could possibly make Chicory break her silence.
* Party of the treaty that Ah brokered with Chaosah prevents Chaosah from bringing in demons from other worlds (a rule that was broken in The Birthday Party episode). However there is some need that requires Chaosah to bring these creatures into Hereva or all will be lost. The witches must convince Wasabi that this is necessary, and prevent her from interfering if she doesn't agree.


### Chaosah Buries their Failures
* The Witches of Chaosah have a long tradition of burying their failures. But not all failures are complete failures, and some of them are not as "inert" as one would like:
* Potions are leaking out and combining in unpredictable ways. It's up to the characters to find the source of the problem and render the potions inert before the potions combine in even more terrible ways.
* Too much Rea in one area attracts folks looking to harvest the Rea for their own purposes. The wrong folks getting their hands on that much Rea could have dire consequences.
* After some flooding the layer of ground that covered one of the failures eroded away and one of the failures has surfaced.
  * Chaosah won't admit to mixing their magic with one of the other magical disciplines, and maybe they were caught mixing Chaosah magic with another school's magic: Magmah, or Zombiah for instance?
  * Perhaps an experiment to find ways to take Chaosah magic closer to the magical fundamentals was abandoned too soon; just before an important breakthrough. The witches of Chaosah have heard of this legendary experiment, and the notes that surfaced contain clues for how to continue the experiments.
* In a fit of cleaning Cumin buried some magical artifacts that should not have been buried. Unfortunately she doesn't remember where she buried them. Normally this wouldn't be a problem, except Thyme is waiting on them and doesn't realize they're missing. Help!

### Squirrel's End Adventure Seeds
* Pepper has forgotten to disarm her traps again. The witches will need to head out to disarm Pepper's clever traps. Hope they don't miss one!
  * This scenario is best with some GM creativity. Some sample traps are provided to give you some ideas on the flavor of traps but we encourage you to make your own.
    * ( List of traps from Pepper's Birthday Party)

## Zombiah Adventure Seeds

* Some late-night programming caused one of Coriander's constructs to break it's control programming and escape. Coriander needs help finding the construct before it corrupts the rest of its programming and does any more damage.
* Coriander is worried that someone or something is trying to remove her from the throne (both figuratively and literally). The witches need to discover who might be behind these sinister plots.
* Zombiah is forbidden from using their powers of reanimation on the dead (save for magical contests) but a faction of Zombiah practitioners bristles at this agreement. They have been secretly reanimating followers and are planning to overthrow Ah and reclaim the right to practice their magic to the fullest. It's up to the witches to locate this group and convince them to stop before Ah takes notice and destroys Zombiah like it did with Chaosah.

## Ah Adventure Seeds

* A dying dragon comes to the temples of Ah for its final resting place. During conversations with the dragon the witches learn of unfinished business the dragon wanted to accomplish. The witches swear to help the dragon with his dying request.
* Ah has been gathering intelligence about all of the schools for generations, but refuses to share that knowledge with the other schools. Unfortunately Ah has gathered data that could help one of the schools, but refuses to share it. It's up to the witches of that school to sneak in and find that information in Ah's archives before it's too late.
* If information is power then Wasabi is the most powerful witch in all of Hereva. She believes that Hereva would be best with complete order, and sends her adepts to gather information and uses the spirits both for guidance and reconnaissance. It's up to the witches to learn of her plans and stop Wasabi before she remakes the world of Hereva into her own design.

## Hippiah Adventure Seeds

* One of the Hippiah witches read from a spell book labeled "Forbidden Spells. DO NOT READ!" and accidentally cast one of them on her plants. This has caused the plant to become sentient. Normally this wouldn't be a bad thing, but unfortunately it also believes that plant life should overtake Hereva, and is now using Hippiah magic to grow all of the plants and make them self-aware. It's up to the witches to defeat the rogue plant. 
* A magical blight is causing many of the plants and trees in the forests to wither and die. The blight continues to spread and if not stopped all of the plants in Hereva will be gone. It's up to the witches to stop the blight, learn who or what caused it, and restore the plants that were lost.
* Pepper's rise to prominence as a Chaosah witch has caused some friction in Hippiah. They believe that she has used Hippiah magic in conjunction with Chaosah magic and diluted the purity of Hippiah magic. Some in Hippiah believe that Pepper must be stopped lest Hippiah magic become more entangled with Chaosah magic. It's up to the witches to determine what is going on. 

## Aquah Adventure Seeds

* Some errant Chaosah potions have leached into the water table and have created a hole at the bottom of the sea. It's up to the witches to fix the hole before all of Hereva's water disappears into it.
* A faction of Aquah believes that they can gain more dominance over the rest of Hereva by warming the frozen parts of Hereva. This will lead to more water which will expand Aquah's territory. Unfortunately this is also wreaking havoc on the climate of Hereva and causing major issues with the weather. It's up to the witches to restore the balance before it's too late.
* A strong current swept away a sacred artifact from one of the temples. Usually such currents don't disturb much, but this current is a _mysterious_ current. It's up to the witches to retrieve it and determine who or what is behind this.

## Magmah Adventure Seeds

* Someone has tampered with the annual Magmah fireworks display. It's up to the witches to find it and stop the saboteur before things go horribly wrong.
* The honor of Magmah is threatened when a rival group of chefs declare that Magmah's recipes were stolen from them. It's up to the witches to find out the truth, and possibly be involved in a cooking contest that will settle the matter once and for all of who's cuisine reigns supreme.
* Magmah is in danger of fracturing because of the rise of Saffron as the face of Magmah. Several witches believe that she is overshadowing the rest of the school and are working to ruin her. Saffron has noticed more attempts to get her out of the limelight, both figuratively and literally. It's up to the witches to determine who is behind this and save Saffron before it's too late.

## Komona adventure Seeds

* There is a rumor that a witch is selling illegal potions to various folks (including a swordsman). The witches are tasked with finding out who is responsible.
* One of the merchants in Komona Square is accused of being a spy working to undermine the city of Komona, but nobody knows who they are working for. The witches are tasked with discretely following this spy and determine who, or what they are working for and prevent them from ruining the city.
* Mayor Bramble is running for re-election for the city of Komona. His opponent, though, is a mysterious entity from the floating community of Cerberus. The campaign is getting progressively more mean with this challenger doing questionable and immoral things to ruin Mayor Bramble's chances of winning. Mayor Bramble isn't a perfect mayor but this new candidate seems far, far worse. It's up to the witches to find out who this challenger is and help Mayor Bramble win the election.


## Potion Challenges
* Not only have the witches been summoned for a potion challenge, they also need to find the ingredients to make the potions!
  * One of the ingredients can be a "secret ingredient" that is revealed during the potion contest.
  * The secret ingredient for the potion challenge is located in a hidden location. The witches must work together to locate where the ingredient is, and (more importantly) what the ingredient is. 
  * Did we mention there's a time limit? Of course there's a time limit.
  * To find the ingredient the witches need to use their available resources. Naturally not all of these resources will be cooperative.
  * Ideally this will spawn cooperation between the witches but certain groups may wish to season with a little more competition between the witches / factions. The group should be working toward finding the secret ingredient but the witches may have different ideas of how the ingredient should be used.


## The Caves of Hereva

## Mapping

* One of the witches is hired by the Hereva Cartography Company to explore unmapped territory (under strict non-disclosure agreement). Unfortunately there's a reason why this territory is unmapped, and it will take the talents of several witches to explore this area and complete the task.

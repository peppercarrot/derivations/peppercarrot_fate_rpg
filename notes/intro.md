# Pepper&Carrot RPG
## Introduction

Pepper&Carrot is based on the characters and world of Hereva created by David Revoy and the Pepper&Carrot community. The characters and story appear in the Pepper&Carrot webcomic (available at https://peppercarrot.com). Familiarity with the comic is encouraged (really, it's quite good) but is not required to play the game.

## The system

The Pepper&Carrot RPG uses the Fate Accelerated role playing system by Evil Hat Productions. All of the rules you'll need to play the game are included. You are encouraged to play with this system to make the characters and the game your own. Additional materials for using the Fate system are available from http://faterpg.com.

## Things the players will be able to do

The world of Hereva is full of magical adventures. Players may explore Hereva and it's many secrets, or they may interact with merchants, fairies, forest-dwellers, dragons, or even The Sage of the mountain.

Players in the Pepper&Carrot RPG will be able to play as any one of the characters from the comic. Some of these characters include:

* Pepper, the main protagonist, who always seems to be getting into trouble as she tries to prove to her godmothers that she is a true witch of Chaosah despite their machinations.

* Saffron, the spoiled rich girl who owns a thriving witchcraft business but still longs for something more that money can't buy.

* Coriander, the tinkerer, who wants to transform her magic school into one of the more powerful schools for making things.

* Shichimi, the reluctant ascetic, who longs for a less strict and more comfortable existence despite the wishes of Wasabi, the leader of Ah.

* Cayenne, the Chaosah enforcer, who wants to return Chaosah to it's rightful place as a magic to be feared.

* Thyme, the Chaosah matriarch, who wants to prolong Pepper's training so her sentence of being trapped as a personal lackey of Wasabi will be postponed.

* Cumin, the Chaosah potions instructor, who is the apprentice of Thyme and wants to ensure that Thyme is happy with her performance.

There are other characters that will be introduced later on in the *Characters* section of the game.

Players may also create characters based on each of the magic schools:

* Ah, who wish to please the spirits by living a life free of material wants and desires and bring purity and obedience to Hereva.
* Chaosah, who wish to learn, understand, and control the fundamental forces of Hereva and bend them to their will.
* Zombiah, who wish to make death an unnecessary part of living, and automate the mundane tasks of life.
* Magmah, who wish to make better, stronger, and flashier materials (for a price).
* Aquah, who wish to be left alone to go with the flow, and are grudgingly brought to the surface when required.
* Hippiah, who wish to help people learn to peacefully co-exist with nature and grow not only food but themselves.

## Roleplaying?

If this is your first time encountering a Role Playing game we encourage you to check out Wikipedia for a definition of what Role Playing is.

The Pepper&Carrot Roleplaying Game is a game about taking on the personas of the characters from the Pepper&Carrot universe and role-playing the adventures of those characters in the universe of Hereva. You and the people playing the game use your imagination to think, feel, and act as though you are the character. One of the players takes on the persona of the world itself; they help set up the world, characters, and situations that the other players will experience. The traditional term for this player is the Game Mediator (or GM for short). This player has the responsibility of setting up the situations, characters, and other events that may occur. If this sounds interesting to you we'll help guide you on how to perform as the GM in this game. The rest of the players will assume the characters (usually one per player) that will participate in this world and whatever situations the GM has set up.

## Equipment

You'll need at least one set of Fate Dice (sometimes referred to as Fudge Dice). .You can find Fate Dice at a friendly local game shop, or order online from an online-retailer or direct from Evil Hat Productions (http://evilhat.com). Fate dice are four six-sided dice with two + faces, two - faces, and two blank faces. You tally the number of + faces and then subtract the number of - faces from the total. This leads to a numeric result between -4 and +4.  

# Using Magic in Hereva

## Using Rea (Casting Spells and Creating Potions)

Casting a spell or creating a potion in Hereva uses Rea. Rea (``Reality'') is part of every day life in Hereva. All creatures in Hereva are capable of casting spells using Rea, but only those who study it can wield it effectively. Magic in Hereva is unlike our concepts of Qi and Mana. Rea is best thought of as collecting the by-products of a task. By concentrating on making a potion for a loved one the practitioner can harness some of the Rea used in creating the potion. Conversely if there is no attachment, or if the practitioner is careless then the Rea is lost and the Rea must be obtained via other means. 

Rea can be harnessed by the following methods:

- The process of summoning
- The time or duration while making something
- Attention, Dedication, or Care
- Emotional engagement

Rea is used for all aspects of life on Hereva. Farmers in the village of Squirrel's End use Rea to grow healthier crops (and tastier crops, as the locals will attest). They also use Rea to ward off the denizens of the forest (which also attest to the tastiness of the crops in Squirrel's End). Most Witches of Hereva don't use their Rea for mere "chores", preferring instead to save their Rea use for more important matters. This is why you'll find witches doing things like sewing on a button or purchasing starfruit from market rather than conjuring up a new outfit or summoning starfruit: the cost in replenishing the Rea outweighs the benefit.

Most practitioners don't actively monitor their Rea usage. They can sense when they are nearing depletion and can seek out ways to regenerate or obtain Rea as needed. How much Rea a person can maintain / channel varies greatly. Some folks are able to charge up luminous levels of Rea. A few witches are skilled in channeling and manipulating large amounts Rea without the need to build up reserves. Such channeling can be dangerous, though; while it is difficult to get too much Rea it is possible certain spells can consume all of the Rea from the caster and the surrounding area. Such spells can lead to disastrous results.

Training in one of the schools of magic allows for more controlled and focused use of Rea. Without this training using Rea becomes a guided intention rather than a direct command. With careful study, focus, and desire one can learn to harness Rea to conform to their will. As the Herevans learned how to control Rea they quickly realized this power could cause havoc if not properly taught, and formed the schools of magic to properly teach the various ways of harnessing Rea. They perfected their craft via trial and error, and compiled that information into spellbooks and potion recipes. Some schools became more secretive about their methods for controlling Rea, while others remained open to all students (mostly from necessity, but some because of the charters of their founding members). Most of the people of Hereva know a little bit of the schools of magic they were exposed to (for instance the farmers of Hereva know a diluted form of Hippiah magic passed on from one generation to the next) but there is a huge difference between mimicking the magic of a school and knowing all of their secret knowledge.

Magmah, Aquah, and Hippiah are especially tuned to channel Rea into the physical world. Ah and Zombiah focus their Rea outside of the physical world. Chaosah is attuned to both the physical and non-material world.

Rea must be replenished from time to time. While Rea can be replenished in various ways (meditation, careful focus on a project, dedication to a project, etc.) these take time and effort to achieve. Certain potions and rare ingredients can be purchased to replenish Rea, but these can be quite costly and are used by a limited number of practitioners. Beware anyone who claims to have invented a way to regenerate Rea without effort; such "perpetual Rea machines" are usually little more than "get-Ko-quick" schemes.

Only students and members of the magical school can effectively cast spells from that school. Those who are untrained in a particular school can still cast Poor (-1) spells regardless of the approach they take. Shichimi is an exception to this, having learned a little bit of each magical tradition in her journeys. She is able to cast Mediocre (+0) spells once per session (see Shichimi's stunts for more details). Pepper is also an exception, having learned some Hippiah Magic. She can cast Hippiah Spells at Mediocre (+0).

## Casting spells

Casting a spell is the same as as any other action in Fate. You can use Create Advantage, Overcome, Attack, or Defend with the approach that matches what you're attempting to do. You may also use aspects or stunts to mofidy your action. See TAKING ACTION for more details.

Spells are cast against the difficulty of the spell and any opposition that may interfere with the casting of the spell. If there is no opposition you either automatically succeed or test agsinst the difficulty of casting the spell. If there is opposition you cast the spell against the difficulty of that opposition. (e.g.: If you are a Magmah witch creating a torch in a dark room this is trivial for you, and no roll is required. If, however, you are in a hurricane trying to make a torch via a spell you'll roll against Great difficulty. If you're a Chaosah witch creating a small Chaosah black hole to remove potions with no opposition then you create it and no roll is required. If, however, you're creating a Chaosah Black Hole in order to prevent a giant asteroid from hitting Hereva while riding the back of a Dragon Cow then you'll roll against a difficulty of Fantastic or greater. Hope you studied!

Casting a spell using a school you haven't studied defaults to Poor (-1). Some witches may have experience with multiple schools (Pepper with Hippiah Magic, Shichimi with multiple schools). Witches that have some experience with another school may cast spells from that school at Mediocre (+0).

## Example of Casting a spell

Saffron is in a dark room. She wants to create some light in the room, so she creates a floating ball of fire next to her. This is a trivial spell for her so she creates the floating ball of fire next to her. The aspect "floating ball of fire providing light" exists for the remainder of the scene. 

Camomile is running away from the guardian of an abandoned castle. She darts into the woods, and casts a spell to create some vines to trip the guardian. She chooses to Create an Advantage "thick vines on the ground" and rolls using her Quick approach at the default Good level (she's in a hurry). The GM decides this test is at Great difficulty, and Camomile rolls a +1 with the dice.  She is successful, so the aspect "thick vines on the ground" exists for the remainder of the scene. Hopefully the guardian won't be able to cut through the vines!

## Magical Focus for each school

### Chaosah
* Relates to Physics and our concept of General Relativity.
* Works with dimensions, space, and time.
* Can disintegrate objects and reconstruct them with enough witches.
* Focuses on small changes having large results.

### Magmah
* Relates to metallurgy and materials science, fire, heat, lifestyle, and cooking
* Works with poshness, metal purification, food preparation
* Focuses more on flashy spells and spells of comfort (food, poshness, riches)

### Hippiah
* Relates to Plants and Plant Growth
* Works with healing and nourishment
* Focuses on helping folks, even if reluctantly

### Aquah
* Relates to water, storms, and weather
* Works with motion of the tides, weather systems, and aquatic life
* Focuses on powerful spells that aren't flashy

### Zombiah
* Relates to reanimation and maker culture
* Works with building mechanical structures and comples machinery
* Focuses on animation and reanimation

### Ah
* Relates to spirits and the spirit world
* Works with listening to spirits and using spirit-based magic for information gathering and direction.
* Focuses on knowledge and order, but can be used offensively in certain cases. 
* (Depending on the campaign sprits may either be able or unable to interact with the physical world)

## Example spells for each school

### Chaosah
* Chaosah Black Hole: Creates a black hole, which opens a portal to another dimension
* Disintegrate / Reintegration spells
* Micro-dimension
* Small changes having large impact

### Magmah
* Fireball (as a weapon or as a torch for light)
* Transformation (potion of poshness, transform one material into another material)
* Cooking / Purifying to make something edible and tasty
* Tempering metal

### Hippiah
* Rapid growth for plants and other materials
* Healing / health restoration
* Communicate with land-based animals (telepathy or otherwise).

### Aquah
* Weather / Storms
* Communicate with sea creatures via telepathy
* Water control (water spouts, water blast)

### Zombiah
* Reanimation spells (resurrection)
* Understand and repair machinery
* Bring constructs and machinery to life

### Ah
* Communicate with spirits
* Spirit blast
* Direct spirits to help find or retrieve objects or move obstacles

## Duration of a spell

The default duration for aspects created with a spell is the length of the scene. Some spell effects might last longer. (A spell to carve a Chaosah symbol into the rocks lasts for as long as the rocks exist and haven't been eroded.) Some spell effects might be immediate with temporary consequences. (A spell to create a flash of light lasts for an instant, but the aspect "Temporarily Can't See" will continue for those who couldn't avert their eyes). Making a spell last longer than a scene increases the difficulty of the spell. Creating a fireball that lasts for a scene might be trivial for Saffron (no roll required), but making a torch that burns for several years unattended would be of Great Difficulty for her. Pepper may have Good Difficulty creating a micro-dimension lasting for a few hours, but creating a permanent micro-dimension laboratory that doesn't collapse is closer to Legendary difficulty (or more) for even the most advanced Chaosah witches.

# Potions

## Creating potions

Potions are spells that are created, carried, and used later. Potions may be created by using either the Quick or Careful approaches. They may also require other ingredients that the characters may have or may need to locate. Once the characters have found the required ingredients they combine those ingredients to create the potion. Use the Quick or Careful approach to Create an Advantage to combine the ingredients correctly and create the a potion aspect. The potion then contains the aspect that you wish to create. The level of your approach is then recorded as the potions Effectiveness. Write down the aspect and the effectiveness of the potion.

Camomile is working on a potion of rapid growth. She uses her Quick approach to create the potion and rolls a Poor (-1) result. Her player then writes "Potion of Rapid Growth (Good)" on an index card.

Potions created at Poor level or lower are unstable and unusable. Depending on the scene the GM may choose to have the potion do nothing, have the opposite effect, or become a potion of something unexpected. If the goal was for Camomile to create a potion of rapid growth and she creates a Poor potion then the potion may have no effect, have the opposite effect (it becomes a potion of rapid shrinking), or does something unexpected (it becomes a potion of itchiness, a potion of rapid hair growth, or something equally unpleasant.)

## Using potions

Using a potion is the same as casting a spell. Use the effectiveness of the potion as the starting level for the spell and roll to find if the potion succeeds. On a successful roll the aspect from the potion is added to the character, object, or scene.

## Example of creating and using potions

Pepper is creating a Potion of Genius. She uses her Quick (Good) approach to create the potion. She doesn't add any other bonuses to her roll, and rolls a Mediocre result (+0), which doesn't add anything to her Quick (Good) approach. Her potion now has the aspect "Potion of Genius" at the level of Good. The GM determines that making a cat into a genius is Good difficulty. She rolls again and gets an Average (+1) result, which is more than the difficulty of Good. When she gives it to Carrot he takes the aspect "Temporary Genius" for the duration of the scene. If she were to attempting to make inanimate rock a temporary genius the GM may set the difficulty at Fantastic or higher (Making an inanimate rock a genius is difficult, even by Heveva standards), so even the Great level of the potion would have no effect. 


## Optional rule: potions from other disciplines

If you are creating a potion that is based off of the magic from one of the other schools you create at the same level as your ability with that school. Even if your Quick or Careful is higher than your skill with the other school's magic you still create potions and spells at your level of familiarity with that particular school's magic. 

Example: If Shichimi is creating a growth potion that uses Hippiah magic, so her potion is created at Mediocre. Good thing Camomile didn't participate or the canary might have been much larger!

## Optional rule: quality of ingredients

Certain ingredients may be of better quality than others. If the GM wishes they may keep track of the quality of the ingredients (based on the Fate ladder, from Terrible to Good) for the potion. Add the ingredients together and use that for a potion creation bonus.

Shichimi is creating a potion for rapid growth (a Hippiah-based potion). Her rating in Hippiah spells is Mediocre (+0). She has sourced ingredients for the spell (Poor, Mediocre, Good, Good, Great). She adds them together to get +6, which is capped off at Good (+2). She then rolls and gets a Poor (-1) result. This means that her Potion of Growth has the aspect Potion of Growth and an effectiveness of Fair. 

## Using potions

Potions are one-time use items. Once a potion is used you cross it off or remove the card from play. 

## Optional Rule: Too many potions

Potion bottles are brittle things. They can leak, break, or slip out of one's hands at inopprtune times.  If a player has more than three potions on their person at one time they may fumble them and cause them to break. The GM may require a test against a character's Careful Approach to ensure that one or more of the potion bottles doesn't break during a combat or other strenuous task. Breaking one potion bottle means that potion is now affecting the player who broke it. The GM determines which potion bottle broke and the potion's aspect now applies to the player character. For instance, if Pepper has three potions and manages to break one the GM selects from her list of potions. She is carrying two potions of invisibility and one potion for slowing down time. The GM decides that one of the potions of invisibility shattered while climbing a rock face, so now she's invisible against the rock face, which adds a level of difficulty to her rock climbing since she can't see her hands. She'd better be extra careful!

Be creative with the results of combining too many potions together. Combining a potion of resurrection with a potion of poshness and a potion of rapid growth could mean having a well-dressed canary stomping through Komona. Who knows what might have happened had Pepper brought an actual potion with her? Perhaps a potion of invisibility might have made an invisible resurrected giant posh canary, or it could have interacted badly with the other potions to make light-reflecting resurrected giant posh canary. Let your imagination and the imagination of the table be your guide.

## Rea Exhaustion

If you run out of Fate Points while casting a spell or making a potion you may take the consequence "Rea Exhaustion" to gain a Fate Point. Rea Exhaustion is not something to be taken lightly. Rea Exhaustion affects your refresh level and adds an additional consequence to your character. Each level of Rea Exhaustion subtracts one from your current Refresh. If a character has three refresh and takes the moderate consequence of "Rea Exhaustion" their Refresh drops from 3 to 2. If they take a second consequence of "Rea Exhaustion" at the Severe level and their refresh drops from 2 to 1.

The GM awards one Fate Point for taking the Rea Exhaustion consequence. For parties that prefer a more challenging game the Fate Point may be taken from one of the other players. See the optional rule below.

Rea Exhaustion: If a witch runs out of Rea (fate points) for a spell you may take the consequence "Rea Exhaustion". This grants one fate point for a moderate consequence or two fate points for a severe consequence. Remember that the rules for being "taken out" still apply. 

Until the Rea Exhaustion is removed then your refresh is depleted by one for moderate consequence or two for severe consequences.

The consequence "Rea Exhaustion" can only be removed via rest or meditation or concentrated study. Each period of deep rest, meditation, or study reduces the "Rea Exhaustion" consequence by one point.


## Optional Rule: The Balance of Rea

Groups that prefer a more challenging game or more consequences for Rea Exhaustion may opt to use The Balance of Rea rule in their game. The default Rea Exhaustion consequence gives one Fate Point for Rea Exhaustion. This Fate Point comes from the GM's pool of points. The Balance of Rea rule changes where that Fate Point comes from. When a player character takes the Rea Exhaustion consequence they may select another player character that is closest to the player character, or the player character with the most Fate Points. That player then gives the player taking Rea Exhaustion one of their Fate Points. If this reduces the giving player's character to zero Fate Points they must then take a moderate "Rea Exhaustion" consequence and receive a Fate Point from one of the other players that has Fate Points. If that player is then reduced to zero then the Fate Points are distributed and subsequent consequences are taken. If all players have the consequence "Rea Exhaustion" and the players have run out of Fate Points to give then the GM may compel the group to accept a world aspect that grants each player one Fate Point. This aspect should reflect a massive Rea Shift in the world of Hereva. At the end of the Great War each of the Chaosah Witches were exhausted of their Rea which caused the Great Tree of Komona to break, which damaged the magic in all of Hereva. GMs, the decision of how severe the exhaustion of Rea would be for your party of witches is up to you.

 Some examples:

* A dimensional rift appears, where a curious inter-dimensional being lays in wait
* The spike in the flow Rea draws the attention of some of the other schools, who come to investigate what happened (This could draw unwanted attention to the characters if they're investigating something secret)
* The depletion of Rea makes it more difficult to to regain Rea in this area, so Refresh might not be possible (Burnout)
* Reality becomes distorted from the power drain, so subtle and not-so-subtle changes in the world may have occurred (the human shopkeeper in the Komona Square is now part-rat, Star Fruit now have seven arms instead of five, Phandas are now twice as big as before, etc.)

These are just some examples. Let your imagination and your players be your guide. Rea Exhaustion is not something to be taken lightly by any witch, but it could be a way to introduce other fun elements into the campaign.

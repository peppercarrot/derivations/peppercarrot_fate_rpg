Pepper&Carrot RPG

* Overview: What is this?
* World Fiction Sections
  * Brief history of Hereva
  * Magic Schools and Systems
    * Rea
    * The Impossible Triangle
    * Chaosah
    * Magmah
    * Ah
    * Hippiah
    * Zombiah
    * Aquah
  * Key areas of note
    * Forests of Squirrels End
    * Komona
    * Hippiah
    * Castle of Zombiah
    * Temples of Ah
    * (Others?)
* Player Characters
  * Character Creation
    * Player Rules
    * Equipment
  * Sample Characters
  * Magic Schools
* Magic in Hereva
  * Conjuring
  * Potions
  * Managing Rea
  * Sample spells
* Game Moderator's Section
  * How to run a game of P&C
* P. D. Fiddlehead's Guide to Hereva (excerpts from the upcoming fourth edition)
  * (Bits of the wiki, adventure seeds)
  * Notable people of Hereva
  * Creatures of Hereva
* Afterword

all: peppercarrot.tex peppercarrot.pdf

peppercarrot.tex : peppercarrot.md metadata.yaml
	pandoc -s --template=Pandoc/templates/cs-6x9-pdf.latex -o $@ $^

peppercarrot.pdf : peppercarrot.md metadata.yaml
	pandoc -s  --template=Pandoc/templates/cs-6x9-pdf.latex --pdf-engine=xelatex -o $@ $^

clean:
	rm peppercarrot.pdf peppercarrot.tex

# Pepper&Carrot Fate-based TTRPG

Written by Craig Maloney

Based on the [Pepper&Carrot](http://peppercarrot.com) Webcomic and the world of Hereva by David Revoy et. al. 

## Getting the PDF

To get the Pepper&Carrot Fate-based TTRPG PDF, you need to run the
following steps on a linux operating system:

+ install xelatex and Pandoc if it is not installed yet
    + `sudo apt install texlive-xetex pandoc`.
+ get these sources
    + `git clone https://framagit.org/peppercarrot/derivations/peppercarrot_fate_rpg`
+ run `make`

Once the `make` command stops, the PDF should be in `peppercarrot_fate_rpg/out`.

## Code of Conduct

All contributions to this project and all contributors are expected to abide by the [Citizens of Hereva Code of Conduct](https://www.peppercarrot.com/en/documentation/409_Code_of_Conduct.html).

## License

Authors of all editions or contributions to this project accept to release their work under the license: [Creative Commons Attribution 4.0 International , CC-BY](https://creativecommons.org/licenses/by/4.0/) . All resources here (images/sounds/videos) are under the same license.

This work is based on Fate Core System and Fate Accelerated Edition (found at http://www.faterpg.com/), products of Evil Hat Productions, LLC, developed, authored, and edited by Leonard Balsera, Brian Engard, Jeremy Keller, Ryan Macklin, Mike Olson, Clark Valentine, Amanda Valentine, Fred Hicks, and Rob Donoghue, and licensed for our use under the Creative Commons Attribution 3.0 Unported license (http://creativecommons.org/licenses/by/3.0/).
